<?php

class val {

    public function __construct() {
        
    }

    public function minlength($data, $arg) {
        if (strlen(preg_replace("#(^(&nbsp;|\s)+|(&nbsp;|\s)+$)#", "",strip_tags($data))) < $arg) {
            return 'Too Short, Minimum length is ' . $arg;
        }
    }

    public function maxlength($data, $arg) {
        if (strlen($data) > $arg) {
            return 'Too Long, Maximum length is ' . $arg;
        }
    }

    public function digit($data) {

        if (ctype_digit($data) == false) {
            return 'Invalid Type of data...';
        }
    }

    public function filter($data, $filterMethod) {
        if (!filter_var($data, $filterMethod)) {
            return 'Not Valid...';
        }
    }

    public function match($data, $data2) {
        if ($data != $data2) {
            return 'Not Match...';
        }
    }

    public function compareValues($values) {
        if (in_array($val, $values)) {
            return 'Question can not have two same tag';
        }
    }

    public function file($data) {
       
        if ($data['error'] != UPLOAD_ERR_NO_FILE ) {
            if($data['error'] == UPLOAD_ERR_INI_SIZE || $data["size"] > 10000000){
                return 'Invalid file size, Maximum 1MB';
            }
            $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
            $detectedType = exif_imagetype($data['tmp_name']);
            
            if(!in_array($detectedType, $allowedTypes)){
                return 'Invalid file type, allow only gif,jpeg,png';
            }
            
        }
    }

}
