<?php

class Mailer {

    function __construct() {
        
    }
    
    public function confirmMail($data) {
        $to = $data['to'];
        $subject = "Confirm Email for QDrains Registration";
        
        $message = '<html>
<head>
</head>
<body>
<div id="main" style="width:700px;height:400px;background-color:#CCC">
<div id="logo"style="width:190px;height:40px;background-repeat:no-repeat;background-size:cover;display:inline-block;margin-top:10px;margin-left:45px;">
<img src="http://www.qdrains.com/public/images/logo.png" style="margin-top:-1px;" height="45" width="250">
</div>


<div id="midle" style="width:600px;height:auto; background-color: #FFF;margin-left: 50px;margin-top:-20px;">
    <div id="topbar" style="width: auto;height: 50px; background-color:#FC0;"><h2 style=" padding-left:25px;line-height:50px;">Please confirm your email address</h2></div>
<div id="toptwo" style="width:570px;height:auto;color:#666;padding-left:25px;overflow:hidden; word-wrap:break-word;">

<h2 style="font-size:19px;">'.$data['f_name'].', confrming your e-mail address will give you full access to QDrains</h2>
</div>

    <div id="midlethree" style="width:700px;height:50px;margin-top:5px;">
    <a href="'.$data['link'].'" style="text-decoration:none;background: #3498db;background-image: -webkit-linear-gradient(top, #3498db, #3ba8eb); background-image: -moz-linear-gradient(top, #3498db, #3ba8eb); background-image: -ms-linear-gradient(top, #3498db, #3ba8eb);background-image: -o-linear-gradient(top, #3498db, #3ba8eb);background-image: linear-gradient(to bottom, #3498db, #3ba8eb);-webkit-border-radius: 5;-moz-border-radius: 5;
border-radius: 5px;font-family: Arial;color: #ffffff;font-size: 20px;padding: 10px 20px 10px 20px;text-decoration: none;width:260px;margin-left:160px;">Create QDrains Account
</a>

</div>

<div id="midletwo" style="width:700px;height:50px;margin-top:5px;padding-left:20px;color:#666;">

If the button dosen\'t work. You can click <a href="'.$data['link'].'">here</a>
</div>
     <div id="topbar" style="width: auto;height:30px; background-color:#FC0;"></div>
</div>


<div id="midlefour" style="width:600px;height:50px;margin-top:10px;margin-left:30px;font-style:italic;font-size:13px;padding-left:20px;color:#666;">
This email was intended for '.$data['f_name'].' '.$data['l_name'].'<br>
If you need assistance or have questions, please contact <a href="http://www.qdrains.com/help/contactus">QDrains Developer Team</a>
</div>

 </div>
</body>
</html>';
        
        $this->sendMail($to, $subject, $message);
    }
    
    public function initeMail($data){
        
    }

    private function sendMail($to, $subject, $message, $headers = '') {
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: QDrains <info@qdrains.com>' . "\r\n";
        mail($to, $subject, $message, $headers);
    }

}
