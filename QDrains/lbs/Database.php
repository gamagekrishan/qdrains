<?php

class Database extends PDO {

    public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS) {
        parent::__construct($DB_TYPE.':host='.$DB_HOST.';dbname='.$DB_NAME, $DB_USER, $DB_PASS);
    }
    
    /**

     * SELECT
     * @param string $sql as SQL string
     * @param array $array Parameters to bind
     * @param constent $fetchMode A PDO fetch mode 
     * @return mixed
     */
    public function select($sql,$array = array() ,$fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        
        foreach ($array as $key => $value) {
            $sth->bindValue("$key", $value);
        }
        $sth->execute();
        return $sth->fetchAll($fetchMode);
    }


    /**
     * Insert
     * @param string $table a name of table to insert into
     * @param string $data as associative array
     */
    public function insert ($table, $data)
    {
        //sort the data by key of associative array
        ksort($data);
       
        $fieldname = implode(',', array_keys($data));
        $fieldvalues = ':'.implode(',:', array_keys($data));

        $sth = $this->prepare("INSERT INTO $table ($fieldname) VALUES ($fieldvalues)");
        
        //just fot the bind the values with prepare statement
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
      
        $sth->execute();
    }
    
    /**
     * update
     * @param string $table a name of table to insert into
     * @param string $data as associative array
     * @param string $where the WHERE query part
     */
    public function update ($table, $data, $where)
    {
        ksort($data);
        $fieldDetails = null;
        foreach ($data as $key => $value) {
            $fieldDetails .= "$key = :$key,";
        }
       $fieldDetails = rtrim($fieldDetails,",");
       
        // upadate user_acc SET user_name = :user_name ,user_pass = 'v2' ,role ='v3'
        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
        
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
      
        $sth->execute();
    }
    /**
     * 
     * @param strin $sql as SQL string
     * @param array $array Parameters to bind
     */
    public function delete ($sql,$array = array())
    {
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue("$key", $value);
        }
        $sth->execute();
    }

}
