<?php

class error extends Controller {

    function __construct() {
        parent::__construct();
        $this->authUserlogin();
        $this->view->title = '404 Error';
    }

    function index() {
        //page variables
        $this->view->css = array('error');
        
        //Render pages
        $this->view->render('header');
        $this->view->render('contentstart');
        $this->view->render('error/index');
        $this->view->render('footer');
    }

    public function unsuccess() {
        if (isset($_SESSION['error'])) {
            $this->view->title = 'unsuccess';
            $this->view->error = $_SESSION['error'];
            //unset($_SESSION['error']);
            $this->view->css = array('unsuccess');
            $this->pages = array('header','contentstart','error/unsuccess','footer');
            $this->pageRender();
        } else {
            $this->flag = false;
            $this->pageRender();
        }
    }

}
