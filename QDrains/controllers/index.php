<?php

class index extends Controller {

    function __construct() {
        parent::__construct();
        //auto login if is set keep me login
        $this->authUserlogin();
        $this->view->css = array('index');
    }

    function index() {
        $tab = isset($_GET['tab']) ? filter_input(INPUT_GET, 'tab', FILTER_SANITIZE_STRING) : 'hot';

        //call model to get date
        switch ($tab) {
            case 'hot':
                $this->view->data = $this->model->index();
                $this->view->smenu = 'hot';
                break;
            case 'week':
                $this->view->data = $this->model->indexWeek();
                $this->view->smenu = 'week';
                break;
            case 'month':
                $this->view->data = $this->model->indexMonth();
                $this->view->smenu = 'month';
                break;
            default :
                $this->view->data = $this->model->index();
                $this->view->smenu = 'hot';
                break;
        }
        $this->view->sidebarData = $this->model->getPageData();
        //render pages
        $this->pages = array('header','sidebarallquestions','sidebarallusers','sidebarunansquestions','sidebartopusers', 'contentstart', 'index/index', 'footer');
        $this->pageRender();
    }

}
