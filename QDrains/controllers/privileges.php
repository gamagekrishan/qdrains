<?php

class privileges extends Controller{

    function __construct() {
        parent::__construct();
        $this->authUserlogin();
        $this->view->title = 'Privileges';
    }
    
    public function index(){
        /*here first get the user id from session then display paricular person privileges*/
        $this->view->title = 'Privileges - Q Drains';
        $this->view->css = array('privileges');
        
        //Render pages
        $this->view->render('header');
        $this->view->render('contentstart');
        $this->view->render('privileges/index');
        $this->view->render('footer');
    }

}