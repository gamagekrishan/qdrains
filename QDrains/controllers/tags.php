<?php

class tags extends Controller {

    function __construct() {
        parent::__construct();
        $this->view->menu = "tags";
        $this->view->title = "Tags";
        $this->authUserlogin();
    }
    
    public function index(){
        $this->view->css = array('tags');
        //$this->view->js = array('tags/js/refreshDiv');
        $this->pages = array('header','sidebarallquestions','sidebarallusers','sidebarunansquestions','sidebartopusers', 'contentstart', 'tags/tagscontentstart');
        if (isset($_GET['tab']) && $_GET['tab'] == 'create') {
            $this->view->submenu = 'create';
            $this->view->js = array('tags/js/createtag');
            array_push($this->pages, 'tags/createtag');
        }else{
            $tagsData = $this->model->getTagsData();
            if($tagsData != false){
//                echo '<pre>';
//                print_r($tagsData);
//                echo '<pre>';
//                die;
            $this->view->tagsData = $tagsData;
            $this->view->submenu = 'popular';
            $this->view->js = array('tags/js/refreshDiv');
            array_push($this->pages, 'tags/index');
            }else{
                $this->flag = false;
            }
        }
        $this->view->sidebarData = $this->model->getPageData();
        array_push($this->pages, 'tags/tagscontentend', 'footer');
        $this->pageRender();
    }
    
    public function search() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $this->model->search();
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function create() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && isset($_POST['title'])) {
            if (Session::get('loggedIn') == true) {
                if (Session::get('rep') > 500) {
                    $this->model->create();
                } else {
                    echo json_encode(array('status' => 0, 'error' => 'Reputation should be more than 500 to create tags'));
                }
            } else {
                echo json_encode(array('status' => 0, 'error' =>'You must be logged in to create tags on QDrains' ));
            }
        }else{
            $this->flag = false;
            $this->pageRender();
        }
    }
}