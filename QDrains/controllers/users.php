<?php

class users extends Controller {

    function __construct() {
        parent::__construct();
        $this->view->menu = "users";
        $this->authUserlogin();
    }

    public function index() {
        if(isset($_GET['tab']) && $_GET['tab'] == 'new'){
            $this->view->submenu = "new";
            $data = $this->model->getUsersData('join_date');
            if($data != false){
            $this->view->usersData = $data ;
            }else{
                $this->flag = false;
            }
        }else{
            $this->view->submenu = "rep";
            $data = $this->model->getUsersData('reputation');
            if($data != false){
            $this->view->usersData = $data ;
            }else{
                $this->flag = false;
            }
        }
        $this->view->sidebarData = $this->model->getPageData();
        $this->view->css = array('users');
        $this->view->js = array('users/js/refreshContent');
        $this->pages = array('header','sidebarallquestions','sidebarallusers','sidebarunansquestions','sidebartopusers', 'contentstart', 'users/users', 'footer');
        $this->pageRender();
    }

    /**
     * To render login page
     */
    function login() {

        if (isset($_SESSION['loggedIn'])) {
            if ($_SESSION['loggedIn'] == true) {
                //to make sure to not visit login page whene loggedin
                header("location:javascript://history.go(-1)");
                exit();
            }
        }
        //views data
        $this->view->submenu = "login";
        $this->view->css = array('login');
        $this->view->js = array('users/js/login');

        //pages loading
        $this->pages = array('header', 'contentstart', 'users/logheader', 'users/login', 'users/logfooter', 'footer');
        $this->pageRender();
    }

    /**
     * log out user
     */
    public function logout() {

        if (Session::get('loggedIn') == true) {
            Session::set('loggedIn', FALSE);

            //destroy session and cookies
            Session::destroy();
            $this->model->destroyCookie();

            //stay the current page after logout
            if (isset($_GET['u'])) {
                header("Location: " . filter_input(INPUT_GET, 'u', FILTER_SANITIZE_URL));
                exit();
            } else {
                header("Location: " . URL);
                exit();
            }
        }
    }

    /**
     * Main login function check the condition and log user
     */
    public function loginRun() {
        //sanitize post data
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && isset($_POST['email'])){
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $pass = Hash::create('sha256', filter_input(INPUT_POST, 'pass', FILTER_SANITIZE_STRING), HASH_PASSWORD_KEY);
        $this->model->loginRun($email, $pass);
        }else{
            $this->flag = false;
            $this->pageRender();
        }
    }

    public function loginconfirm($link = FALSE) {
        if ($link == FALSE) {
            $this->flag = false;
            $this->pageRender();
        } else {
            $res = $this->model->loginconfirm($link);
            if ($res == 'false') {
                $this->flag = false;
                $this->pageRender();
            }
            if ($res == 'true') {
                Session::set('requestfrom', 'loginconfirm');
                header('Location: ' . URL . 'help/confirmation');
                exit();
            }else{
                Session::set('error', 'This link is expired or already used');
                header('Location: ' . URL . 'error/unsuccess');
                exit();
            }
        }
    }
    public function loguser() {
        if (isset($_SESSION['uid'])) {
            $this->model->loguser();
        } else {
            $this->flag = false;
            $this->pageRender();
        }
    }

    public function googlelogin() {
        if(Session::get('loggedIn') != 'true'){
            
        $res = $this->model->googlelogin();

        if ($res == 'new') {
            $this->view->data = Session::get('guser');
            $this->view->css = array('accdetails');

            $this->pages = array('header', 'contentstart', 'users/accdetails', 'footer');
            $this->pageRender();
        }else if($res == '2'){
                Session::set('error', 'This invitation already used or expired');
                header('Location: ' . URL . 'error/unsuccess');
                exit();
            }else if($res == '0'){
                Session::set('error', 'You do not have permission to register with qdrains');
                header('Location: ' . URL . 'error/unsuccess');
                exit();
            }
            
        }else{
            Session::set('error','You are already logged in, if you want to login again please logout first');
            header('Location: ' . URL . 'error/unsuccess');
                exit();
        }
    }

    public function signupRun() {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && isset($_POST['email']) ){
            
            $res = $this->model->signupRun();
            if($res == '0'){
                Session::set('error', 'You do not have permission to register with qdrains');
                echo json_encode(array('status' => 3));
            }
            if($res == '2'){
                Session::set('error', 'This invitation already used or expired');
                echo json_encode(array('status' => 3));
            }
        }else{
            
            $this->flag = false;
            $this->pageRender();
        }
    }

    public function registerGoogleUser() {
        if(isset($_SESSION['guser'])){
        $this->model->registerGoogleUser();
        }else{
             $this->flag = false;
            $this->pageRender();
        }
    }

    public function signUpRunFacebook() {
        
    }

    /**
     * Render signup page 
     */
    function signup() {

        //if user already loggedin then redirect
        if (isset($_SESSION['loggedIn'])) {
            if ($_SESSION['loggedIn'] == true) {
                header("location:javascript://history.go(-1)");
                exit();
            }
        }
        if(isset($_GET['invauthtoken'])){
            $inv = filter_input(INPUT_GET, 'invauthtoken',FILTER_SANITIZE_STRING);
            Session::set('inv', $inv);
        }
        //views data
        $this->view->submenu = "signup";
        $this->view->css = array('login');
        $this->view->js = array('users/js/signup');

        //pages loading
        $this->pages = array('header', 'contentstart', 'users/logheader', 'users/signup', 'users/logfooter', 'footer');
        $this->pageRender();
    }

    /**
     * function which run when particuler user
     * @param int $uId to get particular user data from db
     * @param strin $title to get conten if set
     */
    function single($uId, $title = FALSE) {

        $this->view->css = array('profile');
        $this->view->js = array('users/profile/js/chart');
        $this->view->sidebarData = $this->model->getPageData();
        $this->pages = array('header','sidebarallquestions','sidebarallusers','sidebarunansquestions','sidebartopusers', 'contentstart', 'users/profile/profilecontainerstart');
        //get the user bio data
        $userBioData = $this->model->getUserDetails($uId);

        if ($userBioData != false) {
            $this->view->bioData = $userBioData;
            //calculate user reputaion details
            $this->view->repData = $this->model->getReputaionData($uId);
            /*  echo '<pre>';
              print_r($repDetails);
              echo '<pre>';die; */
            //render sub pages
            if (isset($_GET['tab'])) {
                $tab = $_GET['tab'];
                switch ($tab) {
                    case "summary":
                        $summaryData = array();
                        $summaryData['ans'] = $this->model->getAnsSummaryTabData($uId);
                        $summaryData['ques'] = $this->model->getQuesSummaryTabData($uId);
                        $this->view->summaryTabData = $summaryData;
                        /* echo '<pre>';
                          print_r($summaryData);
                          echo '<pre>';
                          die; */
                        $this->view->submenu = "summary";
                        array_push($this->pages, 'users/profile/index');
                        break;
                    case "answers":
                        $this->view->ansTabData = $this->model->getAnsTabData($uId);
                        $this->view->submenu = "answers";
                        array_push($this->pages, 'users/profile/profanswers');
                        break;
                    case "questions":
                        $this->view->quesTabData = $this->model->getQuesTabData($uId);
                        $this->view->submenu = "questions";
                        array_push($this->pages, 'users/profile/profquestions');
                        break;
                    case "tags":
                        $this->view->TagTabData = $this->model->getTagTabData($uId);
                        $this->view->submenu = "tags";
                        array_push($this->pages, 'users/profile/proftags');
                        break;
                    case "reputation":
                        $this->view->RepTabData = $this->model->getRepTabData($uId);
                        $this->view->submenu = "reputation";
                        array_push($this->pages, 'users/profile/profreputations');
                        break;
                    default :
                        $this->flag = false;
                }
            } else {
                $summaryData = array();
                $summaryData['ans'] = $this->model->getAnsSummaryTabData($uId);
                $summaryData['ques'] = $this->model->getQuesSummaryTabData($uId);
                $this->view->summaryTabData = $summaryData;
                $this->view->submenu = "summary";
                array_push($this->pages, 'users/profile/index');
            }
        } else {
            $this->flag = false;
        }
        array_push($this->pages, 'users/profile/profilecontainerend', 'footer');
        $this->pageRender();
    }

    public function edit() {
        if(Session::get('loggedIn') == true){
        $this->view->title = "User - ".Session::get('uName');
        $this->view->css = array('edit');
        $this->view->js = array('users/profile/js/edit');
        $this->view->userData = $this->model->edit();
        $this->pages = array('header','contentstart', 'users/edit', 'footer');
        
        }else{
            $this->flag = false;
        }
        $this->pageRender();
    }
    public function editsave(){
        if(isset($_POST['saveprofile'])){
            $this->model->saveprofile();
        }else{
            $this->flage = false;
            $this->pageRender();
        }
    }

    public function search(){     
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $this->model->search();
        }else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }
    
    public function authfbuser(){
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        echo json_encode(array(
           'status' => 0,
            'er' => 'cant reach your request at this moment, authentication fail.Error 17897'
        ));
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }
}
