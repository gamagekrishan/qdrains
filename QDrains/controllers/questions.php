<?php

class questions extends Controller {

    function __construct() {
        parent::__construct();
        $this->view->menu = "questions";

        $this->authUserlogin();
    }

    /**
     * render default page of questions
     */
    public function index() {
        $this->view->css = array('questions');
        $this->view->title = "Questions";
        if (isset($_GET['tab']) && $_GET['tab'] == 'unanswered') {
            $this->view->submenu = "unanswered";
            $unAnsQData = $this->model->getUnanswerdQuestionsData();
            if ($unAnsQData != false) {
                $this->view->quesData = $unAnsQData;
            } else {
                $this->flag = false;
            }
        } else {
            $this->view->submenu = "newest";
            $newQData = $this->model->getNewstQuestionsData('newest');
            if ($newQData != false) {
                $this->view->quesData = $newQData;
            } else {
                $this->flag = false;
            }
        }
        $this->view->sidebarData = $this->model->getPageData();
        $this->pages = array('header', 'sidebarallquestions', 'sidebarallusers', 'sidebarunansquestions', 'sidebartopusers', 'contentstart', 'questions/index', 'footer');
        $this->pageRender();
    }

    /**
     * render a one question with question id
     * @param int $qId get question id
     * @param string  $qTitle get the question title
     */
    public function single($qId, $qTitle = FALSE) {
        $data = $this->model->getQuestionsById($qId);
        if ($data == false) {
            $this->flag = false;
        } else {
            $this->flag = true;
        }
        $this->view->css = array('questionanswers');
        $this->view->data = $data;
        $this->view->js = array('questions/questionanswers/js/questionanswers');
        $this->view->sidebarData = $this->model->getPageData();
        //Render pages
        $this->pages = array('header', 'sidebarallquestions', 'sidebarallusers', 'sidebarunansquestions', 'sidebartopusers', 'contentstart', 'questions/questionanswers/index', 'footer');
        $this->pageRender();
    }

    /**
     * render question ask page
     */
    public function ask() {
        $this->view->title = "Ask Questions";
        $this->view->menu = "ask";
        $this->view->css = array('ask');
        $this->view->js = array('questions/ask/js/ask');

        $this->view->sidebarData = $this->model->getPageData();
        //Render pages
        $this->pages = array('header', 'sidebarallquestions', 'sidebarallusers', 'sidebarunansquestions', 'contentstart', 'questions/ask/index', 'footer');
        $this->pageRender();
    }

    /**
     * get the available tags for set to qeustion
     */
    public function getTags() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $this->model->getTags();
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function postQuestions() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (isset($_SESSION['uId'])) {
                $this->model->postQuestions();
            } else {
                echo json_encode(array('status' => 2));
            }
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function postcomment($type) {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if ($type == 'ans') {
                if (isset($_SESSION['uId'])) {
                    $this->model->postAnsComment();
                } else {
                    echo json_encode(array('status' => 0, 'notlog' => 'true'));
                }
            }
            if ($type == 'ques') {
                if (isset($_SESSION['uId'])) {
                    $this->model->postQuesComment();
                } else {
                    echo json_encode(array('status' => 0, 'notlog' => 'true'));
                }
            }
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function postAnswer() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (isset($_SESSION['uId'])) {
                $this->model->postAnswer();
            } else {
                echo json_encode(array('status' => 0, 'notlog' => 'true'));
            }
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function quesvote() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (isset($_SESSION['uId'])) {
                    $this->model->quesvote();
            } else {
                echo json_encode(array('status' => 0, 'msg' => 'You must be logged in to vote'));
            }
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function ansvote() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (isset($_SESSION['uId'])) {
                    $this->model->ansvote();
            } else {
                echo json_encode(array('status' => 0, 'msg' => 'You must be logged in to vote'));
            }
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function statusChangeRequest() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if ($_POST['type'] == 'ans') {
                $table = 'answerchangestatus';
                $column = 'ans_Id';
                $des = 'This Answer clsed by the community as not useful answer';
                $rType = 'close';
                $this->model->statusChangeRequest($table, $column, $des, $rType);
            }
            if ($_POST['type'] == 'ques') {
                $table = 'questionschangestatus';
                $column = 'q_Id';
                $des = 'This question clsed by the community as not useful question';
                $rType = 'close';
                $this->model->statusChangeRequest($table, $column, $des, $rType);
            }
            if ($_POST['type'] == 'accept') {
                $table = 'answerchangestatus';
                $column = 'ans_Id';
                $des = 'Answer is accept';
                $rType = 'accept';
                $this->model->statusChangeRequest($table, $column, $des, $rType);
            }
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function closeAnswers() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $this->model->closeAnswers();
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    public function acceptRequest() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $this->model->acceptRequest();
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

}
