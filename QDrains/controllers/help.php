<?php

class help extends Controller {

    function __construct() {
        parent::__construct();
        $this->authUserlogin();
        $this->view->title = 'Help';
    }

    /**
     * 
     */
    function index() {
        $this->view->title = 'Help';
        $this->view->css = array('help');

        $this->pages = array('header', 'contentstart', 'help/index', 'footer');
        $this->pageRender();
    }

    /**
     * render help info pages
     * @param string $data use to get particular page name
     */
    function info($data = false) {
        if ($data == false) {
            $this->flag = FALSE;
            $this->pageRender();
        }
        $file = 'views/help/' . $data . '.php';
        if (file_exists($file)) {
            $this->flag = true;
            $this->view->css = array('help');
            $this->pages = array('header', 'sidebarhelp', 'contentstart', 'help/' . $data, 'footer');
            $this->pageRender();
        } else {
            $this->flag = FALSE;
            $this->pageRender();
        }
    }

    function contactus() {
        $this->view->title = 'Contact Us';
        $this->view->js = array('help/contactus/js/cqdrainsteam');
        $this->view->css = array('contactus');
        $this->pages = array('header', 'contentstart', 'help/contactus/index', 'footer');
        $this->pageRender();
    }

    function registersuccess() {
        if (Session::get('requestfrom') == 'registersuccess') {
            unset($_SESSION['requestfrom']);
            $this->view->title = 'Help';
            $this->view->css = array('success');

            $this->pages = array('header', 'contentstart', 'help/registersuccess/index', 'footer');
            $this->pageRender();
        } else {
            $this->flag = false;
            $this->pageRender();
        }
    }
    function mergesuccess() {
        if (Session::get('requestfrom') == 'mergesuccess') {
            
            $this->view->title = 'Help';
            $this->view->css = array('success');

            $this->pages = array('header', 'contentstart', 'help/registersuccess/mergesuccess', 'footer');
            unset($_SESSION['requestfrom']);
            $this->pageRender();
        } else {
            $this->flag = false;
            $this->pageRender();
        }
    }

    function confirmation() {
        if (Session::get('requestfrom') == 'loginconfirm') {
            unset($_SESSION['requestfrom']);
            $this->view->title = 'Help';
            $this->view->css = array('confirm');
            $this->pages = array('header', 'contentstart', 'help/confirmation/index', 'footer');
            $this->pageRender();
        } else {
            $this->flag = false;
            $this->pageRender();
        }
    }

    function recoverpassword() {
        if (Session::get('requestfrom') == 'loginconfirm') {
            $this->view->title = 'Help';
            
            $this->view->css = array('recover');
            $this->pages = array('header', 'contentstart', 'help/forgetpassword/index', 'footer');
            $this->pageRender();
        } else {
            $this->flag = false;
            $this->pageRender();
        }
    }
    public function cusubmit(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && isset($_POST['email'])){
            $this->model->cuSubmit();
        }else {
            $this->flag = false;
            $this->pageRender();
        }
    }

}
