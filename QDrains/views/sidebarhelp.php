<div id="news"> 
    <div id="help">
        <div id="help_head">Help</div>
        <ul style="margin-left: -25px;">
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/on-topic">What topic can i ask Here?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/question-bans">Why are questions no longer being accepted from my account? </a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/how-to-ask">How do I ask a good question?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/accepted-answer">What does it mean when an answer is "accepted"?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/how-to-answer">How do I write a good answer?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/answer-bans">Why are answers no longer being accepted from my account?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/reset-password">I lost my password; how do I reset it?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/why-register">How do I create an account?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/why-vote">Why is voting important?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/whats-reputation">What is reputation? How do I earn (and lose) it?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/site-moderators">Who are the site moderators, and what is their role here?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/be-nice">Be nice.</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/interesting-topics">How do I find topics I'm interested in?</a></div></li>
        <li><div id="help_content"><a href="<?php echo URL; ?>help/info/behavior">What kind of behavior is expected of users?</a></div></li>
    </ul>
    </div>
</div>