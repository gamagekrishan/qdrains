<div id="news">   
    <div id="topusers">
        <div id="topusers_head">Top Users</div>
        <div id="topusers_content">
            <?php foreach($this->sidebarData['users'] as $user){ ?>
            <div id="topusers_content_oneuser">
                <a href="<?php echo URL.'users/'.$user['user_Id'].'/'.$user['l_name']; ?>"><div id="topusers_content_oneuser_img" style="background-image: url(<?php echo $user['profile_image']; ?>)"></div></a>
                <div id="topusers_content_oneuser_name"><a href="<?php echo URL.'users/'.$user['user_Id'].'/'.$user['l_name']; ?>"><?php echo $user['l_name']; ?></a></div>
            </div>
            <?php }?>
        </div>
    </div>
</div>