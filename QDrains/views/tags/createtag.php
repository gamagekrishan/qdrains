
<div id="createtag_head">
    <div id="subconmenu">
        <ul>
            <li class='active'><span id="font">Create Tag</span></li>
        </ul>
    </div>

</div>
<form action="" method="post" name="createtagform" id="createtagform" >
    <div id="createtag_title"><span id="font" style="margin-left:40px;">Tag Title :</span>
        <input type="text" id="textboxstyle" name="title"/>
    </div>
    <div id="createtag_description">
        <span id="font" style="margin-left:40px;">Description</span>
        <hr style="width:670px;">
        <div id="createtag_description_textarea">
            <textarea name="content"></textarea>
        </div>
    </div>
    <div id="btncon">
        <input type="submit" id="btn" value="Submit" />
    </div>

</form>
<div id="dialog"></div>
<div id="about_tags">
    <pre style="font-size:14px;"><span style="color: red;font-weight: bold">What is creating tags?</span>

A tag is a label that categorizes your question with other.
 
When you choose to create tag, you're asking a question in a topic that nobody 
before you.

<span style="color: red;font-weight: bold">How do I create a new tag?</span>

Simply enter tag title and description here and submit. 
When creating your new tag,

    must be shorter than 25 characters
    must use the character set a-z 0-9 + # - .</pre>

</div>