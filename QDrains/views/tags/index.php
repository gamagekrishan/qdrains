
<div id="dp_wrapper_maincon_body_type">
    <?php $tagsData = $this->tagsData['data'];
    $pageSetupData = $this->tagsData['pagesetupdata']; ?>
    <div id="dp_wrapper_maincon_body_type_text">
        <span id="font">Type to find tag &nbsp&nbsp</span>
    </div>
    <div id="dp_wrapper_maincon_body_type_textcon">
        <input type="text" id="textboxstyle"/>
        
    </div>
    <div id="dp_wrapper_maincon_body_wait" style="float: right; padding-top: 5px;">
    <div style="display: none" id="waitdiv"><img src="<?php echo URL; ?>public/images/wait.gif"></div>
</div>
</div>

<!----Tags---->

<div id="tagCon">
    <?php foreach ($tagsData as $tag) { ?>
        <div id="dp_wrapper_maincon_body_tags_con">
            <a href="#" id="dp_wrapper_maincon_body_tags_block_inherit">
                <div id="dp_wrapper_maincon_body_tags_block">
                    <div id="dp_wrapper_maincon_body_tags_block_tagname">
                        <span id="font"><?php echo $tag['title']; ?></span>
                    </div>
                    <div id="dp_wrapper_maincon_body_tags_block_discription">
                        <span id="font"><?php echo substr($tag['content'], 0, 120) . '...'; ?></span>
                    </div>
                    <div id="dp_wrapper_maincon_body_tags_block_useageandrank">
                        <div id="dp_wrapper_maincon_body_tags_block_useage">
                            <div id="dp_wrapper_maincon_body_tags_block_useage_name">
                                <span id="font">Followers:</span>
                            </div>
                            <div id="dp_wrapper_maincon_body_tags_block_useage_count">
                                0
                            </div>
                        </div>
                        <div id="dp_wrapper_maincon_body_tags_block_rank">
                            <div id="dp_wrapper_maincon_body_tags_block_rank_name">
                                <span id="font">Questions:</span>
                            </div>
                            <div id="dp_wrapper_maincon_body_tags_block_rank_count">
                                <?php echo $tag['questions_usage']; ?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </a>
<?php } ?>

</div>
<!----End of tags--->

