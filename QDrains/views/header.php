<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php if(isset($this->title)){ echo $this->title.' - QDrains';}else{echo 'QDrains';} ?></title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script language="Javascript" src="<?php echo URL; ?>public/text_editor/text_editor/tinymce.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/Default.css" />
        <?php
        if (isset($this->css)) {
            foreach ($this->css as $css) {
                echo '<link rel="stylesheet" href="' . URL . 'public/css/' . $css . '.css" />';
            }
        }
        if (isset($this->js)) {
            foreach ($this->js as $js) {
                echo '<script type= "text/javascript" src="' . URL . 'views/' . $js . '.js"></script>';
            }
        }
        ?>
        <script>
            tinymce.init({selector: 'textarea'});
        </script>
    </head>
    <body>
        <div id="wrapper" >
            <input type="hidden" id="phpVar" value="<?php echo URL; ?>">
            <!--main menu bar code-->
            <div id="mainmenubar">
                <div id="mainmenubarcontent"style="float: right">
                    <div id="logo" style="background-image: url(<?php echo URL; ?>public/images/logo.png)">
                    </div>
                    <div id="mainmenubarbtn" class="fontfamily">
                        <a <?php
                        if (isset($this->menu) && $this->menu == "questions") {
                            echo 'style="background-color: #e9e9e9;"';
                        }
                        ?>
                            href="<?php echo URL ?>questions">Questions</a>
                        <a <?php
                        if (isset($this->menu) && $this->menu == "tags") {
                            echo 'style="background-color: #e9e9e9;"';
                        }
                        ?>
                            href="<?php echo URL ?>tags">Tags</a>
                        <a <?php
                        if (isset($this->menu) && $this->menu == "users") {
                            echo 'style="background-color: #e9e9e9;"';
                        }
                        ?>
                            href="<?php echo URL ?>users" >Users</a>
                        <a <?php
                        if (isset($this->menu) && $this->menu == "ask") {
                            echo 'style="background-color: #e9e9e9;"';
                        }
                        ?>
                            href="<?php echo URL ?>questions/ask" >Ask Questions</a>
                    </div>
                    <div id="searchbar" style="float: right">
                        <div>
                        <form action="<?php echo URL ?>index/getData" method="post">
                            <input style="padding-left:5px;" type="text" name="text" id="txtsearch" onkeyup="" placeholder="Search"/>
                        </form>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div style=" margin-top: 50px;">
                <div id="lsidebar"><?php if (Session::get('loggedIn') == true) { ?>
                        <div id="loginstatus">
                            <div id="lsidebarpoto" style="display: inline-block;  background-repeat: no-repeat; background-size: cover; background-image: url(<?php echo $_SESSION['uImage'];?>);">
                                <a href="<?php echo URL; ?>users/edit" id="profeditbtn">Edit</a>
                            </div>
                            <div id="logindetails">
                                <span style="color: blue;">Reputation <?php echo Session::get('rep'); ?></span>
                            </div>
                            <div id="logindetails1" style="margin:10px;">
                                <span><?php echo $_SESSION['uName']; ?></span>
                            </div>
                            <div class="sidemenu">
                                <ul>
                                    <li 
                                    <?php
                                    if (isset($this->lmenu) && $this->lmenu == "profile") {
                                        echo 'style="background-color: white;"';
                                    }
                                    ?>
                                        ><a href='<?php echo URL.'users/'.  Session::get('uId').'/'.str_replace(' ', '+', Session::get('uName')); ?>'><span>Profile</span></a></li>
                                    <li><a href='
                                       <?php
                                        
                                       $url = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL);
                                        $url = urlencode($url);
                                        echo URL.'users/logout?u='.$url;?>'><span>Log out</span></a></li>
                                </ul>
                            </div>

                        </div>
                        <?php
                    }

                    if (Session::get('loggedIn') == FALSE) {
                        ?>
                        <div class="sidemenu">
                            <ul>
                                <li><a href='<?php echo URL; ?>users/login'><span>Login</span></a></li>
                                <li><a href='<?php echo URL; ?>users/signup'><span>Sign Up</span></a></li>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="sidemenu">
                        <ul>
                            <li><a href='<?php echo URL; ?>help'><span>Help</span></a></li>
                            <li><a href='<?php echo URL; ?>help/contactus'><span>Contact Us</span></a></li>
                        </ul>
                    </div>
