
<div id="dp_wrapper"><?php if( !isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false){ ?>
    <div id="dp_wrapper_head">
        <div id="dp_wrapper_head_exit">
            <a href="#">X</a>
        </div>
        <span class="fontfamily">Qdrain is a question and answer site for professional and enthusiast programmers. It's 100% free, no registration required. </span>
</div><?php }?>
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Total Questions</span></li>
                    <!--<li <?php if(isset($this->smenu) && $this->smenu == "interesting"){
                        echo 'style="background-color: white;"';} ?>
                        ><a href='#'><span>Interesting</span></a></li>-->
                    <li <?php if(isset($this->smenu) && $this->smenu == "hot"){
                        echo 'style="background-color: white;"';} ?>
                        ><a href='<?php echo URL; ?>?tab=hot'><span>Hot</span></a></li>
                    <li <?php if(isset($this->smenu) && $this->smenu == "week"){
                        echo 'style="background-color: white;"';} ?>
                        ><a href='<?php echo URL; ?>?tab=week'><span>Week</span></a></li>
                    <li <?php if(isset($this->smenu) && $this->smenu == "month"){
                        echo 'style="background-color: white;"';} ?>
                        ><a href='<?php echo URL; ?>?tab=month'><span>Month</span></a></li>
                </ul>
            </div>
        </div>
        
        <?php  foreach ($this->data['question'] as $ques){ ?>
    <div id="dp_wrapper_maincon_body">
        <div id="dp_wrapper_maincon_body_status_con" class="fontfamily">
                <div id="dp_wrapper_maincon_body_status">
                    <div id="dp_wrapper_maincon_body_status_count_vote">
                        <span id="font1"><?php echo $this->data['votes'][$ques['qId']]; ?> </span>
                    </div>
                    <span id="font1">Vote</span>
                </div>
                <div id="dp_wrapper_maincon_body_status">
                    <div id="dp_wrapper_maincon_body_status_count_answers">
                        <span id="font1"><?php echo $this->data['ans'][$ques['qId']]; ?></span>
                    </div>
                    <span id="font1">Answers</span>
                </div>
                <div id="dp_wrapper_maincon_body_status">
                    <div id="dp_wrapper_maincon_body_status_count_views">
                        <span id="font1"><?php echo $ques['views']; ?></span>
                    </div>
                    <span id="font1">Views</span>
                </div>
            </div>
            <div id="dp_wrapper_maincon_body_details">
                <div id="dp_wrapper_maincon_body_titleofq">
                    <span id="font"><a href="<?php echo URL.'questions/'.$ques['qId'].'/'.str_replace(' ', '-', $ques['title']); ?>"><?php echo $ques['title']; ?></a></span>
                </div>
                <div id="dp_wrapper_maincon_body_content" class="fontfamily">
                    <span><?php echo substr(strip_tags(html_entity_decode($ques['content'])),0,400); ?>
                    </span>
                </div>
                <div id="dp_wrapper_maincon_body_tags_tab"><?php  foreach ($this->data['tags'][$ques['qId']] as $tag){?>
                    <a href="#" id="dp_wrapper_maincon_body_tags_tab_tag"><?php echo $tag['title']; ?></a>
                <?php } ?>
                </div>
                <div id="dp_wrapper_maincon_body_status_con_two">
                    <div id="dp_wrapper_maincon_body_status_two">
                        <div id="dp_wrapper_maincon_body_status_asked">
                            <div id="font1">Asked</div>
                        </div>
                        <div id="dp_wrapper_maincon_body_status_time">
                            <div id="font1"><?php echo $ques['ask_date']; ?></div>
                        </div>
                    </div>
                    <div id="dp_wrapper_maincon_body_status_three">
                        <div id="dp_wrapper_maincon_body_status_three_image">
                            <a href="<?php echo URL.'users/'.$ques['user_Id'].'/'.$ques['l_name'];?>"><img src="<?php echo $ques['profile_image']; ?>" id="dp_wrapper_maincon_body_status_three_image"/></a>
                        </div>
                        <div id="dp_wrapper_maincon_body_status_three_username">
                            &nbsp;&nbsp;<?php echo $ques['f_name'].' '.$ques['l_name']; ?>
                        </div>
                        <div id="dp_wrapper_maincon_body_status_three_reputation">
                            <div id="font1">&nbsp;&nbsp;<?php echo $ques['reputation']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
       <!---------------------------------------------------------------------------------------------------> 
        
        
        
      <!--------------------------------------------------------------------------------------------------->  
    </div>
    
</div>