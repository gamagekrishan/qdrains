
<div id="dp_wrapper">
  <div id="dp_wrapper_maincon">
    <div id="dp_wrapper_maincon_head">
      <div id="mainconmenu">
        <ul>
          <li class='active'><span style="font-size:16px; font-weight:bold;">Unexpected Error</span></li>
        </ul>
      </div>
    </div>
    <div id="dp_wrapper_maincon_body">
     
      <br>
      <div id="dp_wrapper_maincon_body_div2" style="background-image:url(<?php echo URL; ?>public/images/sorry.png);"> </div>
      
      <div id="dp_wrapper_maincon_body_div1"> <span id="font"><?php echo Session::get('error'); ?></span> </div>
      
      <div id="dp_wrapper_maincon_body_div3"> <span style="font-size:25px; font-weight:bold;"> If you feel something is wrong here <br> Please <a style="color: blue; text-decoration: none;" href="<?php  echo URL; ?>help/contactus">contact us</a>.</span> </div>
    </div>
  </div>
</div>