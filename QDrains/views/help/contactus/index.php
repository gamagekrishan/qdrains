<div id="dp_wrapper">
  <div id="dp_wrapper_maincon">
    <div id="dp_wrapper_maincon_head">
      <div id="mainconmenu">
        <ul>
            <li class='active'><span style="font: sans-serif; font-size: 16px; font-weight: bold;">Contact Us</span></li>
        </ul>
      </div>
    </div>
    
    <div id="dp_wrapper_maincon_body"> 
        <form action="#" method="post" id="cform">
    <div id="dp_wrapper_maincon_body_contentpack">
    <div id="dp_wrapper_maincon_body_subjecttitle"><span id="font">Subject</span></div>
    <div id="dp_wrapper_maincon_body_subjecttextbox">
        <input type="text" name="subject" placeholder="Enter Subject" id="textboxstyle"/>
        <div class="notification" id="csubject" style="margin-left:10px;"></div>
    </div>
    <div id="dp_wrapper_maincon_body_emailtitle"><span id="font">Your E-Mail</span></div>
    <div id="dp_wrapper_maincon_body_emailtextbox">
        <input type="text" name="email" placeholder="Enter E-mail" id="textboxstyle"/>
        <div class="notification" id="cemail" style="margin-left:10px;"></div>
    </div>
    <div id="dp_wrapper_maincon_body_problemtitle"><span id="font">Please describe Your Problem</span></div>
    <div id="dp_wrapper_maincon_body_problemtextbox">
        <textarea rows="10" name="content"></textarea>
        <div id="ccontent" class="notification"></div>
    </div>
    <button id="btn">Submit</button>
    </div><div id="dialog"></div>
        </form>
    </div>
  </div>
  </div>