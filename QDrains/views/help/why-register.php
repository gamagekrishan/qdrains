<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Why should I create an account?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
               <!--- <span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               If you have lost your password,</span><br><br>--->



Registration is not required to participate on QDrains; you can read, answer, and suggest edits as an anonymous user, much like on Wikipedia. 
<br><br>
There are some things you won’t be able to do on the site without registering, however, such as vote or ask new questions. Registering is easy, and once logged in, you can gain other key privileges by earning reputation.
<br><br>
You will receive a +1 point reputation bonus when you first log in.


         <br><br><br>   
            </div>
        </div>
    </div>
</div>