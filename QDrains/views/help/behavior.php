<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">What kind of behavior is expected of users?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
               <!---<span style="font-style:inherit; font-weight:bold; font-size:16px;">
                    
               Browse by tag</span><br><br>--->


We’re excited to have you here, but we do ask that you follow a few guidelines when participating on our network.
<br><br>

<span style="font-style:inherit; font-weight:bold; font-size:16px;">
                    
               Be honest.</span>

<br><br>
Above all, be honest. If you see misinformation, vote it down. Add comments indicating what, specifically, is wrong. Provide better answers of your own. Last but not least, edit and improve the existing questions and answers! By doing these things, you are helping keep QDrains a great place to share knowledge of our craft.
<br><br>
While you’re doing all of those things, we also require that you...

<br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16px;">
                    
               Be nice.</span>

<br><br>
Whether you've come to ask questions, or to generously share what you know, remember that we’re all here to learn, together. Be welcoming and patient, especially with those who may not know everything you do. Oh, and bring your sense of humor. Just in case.
<br><br>
For specific guidelines, see: Be nice - principles and practice.

<br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16px;">
                    
               Do not use signature, taglines, or greetings.</span>

<br><br>
Every post you make is already “signed” with your standard user card, which links directly back to your user page. If you use an additional signature or tagline, it will be removed to reduce noise in the questions and answers.
<br><br>
Your user page belongs to you — fill it with information about your interests, links to stuff you’ve worked on, or whatever else you like!
<br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16px;">
                    
               Avoid overt self-promotion.</span>

<br><br>
The community tends to vote down overt self-promotion and flag it as spam. Post good, relevant answers, and if some (but not all) happen to be about your product or website, that’s okay. However, you must disclose your affiliation in your answers.
<br><br>
If a large percentage of your posts include a mention of your product or website, you're probably here for the wrong reasons. Our advertising rates are quite reasonable; contact our ad sales team for details. We also offer free community promotion ads for open source projects and non-profit organizations.


         <br><br><br>   
            </div>
        </div>
    </div>
</div>