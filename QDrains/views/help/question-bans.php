<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Why are questions no longer being accepted?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
                <span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
                Why are questions no longer being accepted from my account?</span><br><br>
                

QDrains has automatic filters in place to ban questions from accounts that have contributed many low-quality questions in the past. These filters help keep the quality of our sites high. The exact formula for the bans is not disclosed, but users are only banned if they have a significant number of heavily down-voted, zero-voted, or deleted posts. One or two bad posts will not cause you to be blocked from using the site.
<br><br><br>
Users who are banned from asking questions see the following error message when trying to post a new question:
<br><br><br>
    We are no longer accepting questions from this account. See the Help Center to learn more. 
<br><br><br>
Question bans do not affect other privileges, such as commenting or voting, and there is no indication to the rest of the community that a particular user has been banned.
How can I get out of a question ban?
<br><br><br>
The ban will be lifted automatically by the system when it determines that your positive contributions outweigh those questions which were poorly received. 
            </div>
        </div>
    </div>
</div>