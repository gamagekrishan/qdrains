<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">How do I write a good answer?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
                <span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Pay it forward</span><br><br>
                

Saying “thanks” is appreciated, but it doesn’t answer the question. Instead, vote up the answers that helped you the most! If these answers were helpful to you, please consider saying thank you in a more constructive way – by contributing your own answers to questions your peers have asked here.

<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Have the same problem?</span><br><br>


Still no answer to the question, and you have the same problem? Help us find a solution by researching the problem, then contribute the results of your research and anything additional you’ve tried as a partial answer. That way, even if we can’t figure it out, the next person has more to go on. You can also vote up the question or set a bounty on it so the question gets more attention.
<br><br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Answer the question</span><br><br>


Read the question carefully. What, specifically, is the question asking for? Make sure your answer provides that – or a viable alternative. The answer can be “don’t do that”, but it should also include “try this instead”. Any answer that gets the asker going in the right direction is helpful, but do try to mention any limitations, assumptions or simplifications in your answer. Brevity is acceptable, but fuller explanations are better.
<br><br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
              Provide context for links</span><br><br>


Links to external resources are encouraged, but please add context around the link so your fellow users will have some idea what it is and why it’s there. Always quote the most relevant part of an important link, in case the target site is unreachable or goes permanently offline.
<br><br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
              Write to the best of your ability</span><br><br>


We don't expect every answer to be perfect, but answers with correct spelling, punctuation, and grammar are easier to read. They also tend to get upvoted more frequently. Remember, you can always go back at any time and edit your answer to improve it.
<br><br>
            </div>
        </div>
    </div>
</div>