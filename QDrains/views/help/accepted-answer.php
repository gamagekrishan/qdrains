<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">What does it mean when an answer is "accepted"?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
                <!---<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Write a title that summarizes the specific problem</span><br><br>--->
                

                
When a user receives a good answer to his or her question, that user has the option to "accept" an answer. Acceptance is indicated by a colored checkmark next to the answer that has been accepted by the original author of the question.
<br><br>
Accepting an answer is not meant to be a definitive and final statement indicating that the question has now been answered perfectly. It simply means that the author received an answer that worked for him or her personally, but not every user comes back to accept an answer, and of those who do, they may not change the accepted answer if a newer, better answer comes along later.
<br><br>
Often, accepting an answer corresponds to reputation gains.ed an obvious piece of information, be ready to respond by editing your question to include it. If someone posts an answer, be ready to try it out and provide feedback!
<br><br>
            </div>
        </div>
    </div>
</div>