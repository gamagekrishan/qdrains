<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">How do I ask a good question?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
                <span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Write a title that summarizes the specific problem</span><br><br>
                


The title is the first thing potential answerers will see, and if your title isn't interesting, they won't read the rest. So make it count:
<br><br><br>
    Pretend you're talking to a busy colleague and have to sum up your entire question in one sentence: what details can you include that will help someone identify and solve your problem? Include any error messages, key APIs, or unusual circumstances that make your question different from similar questions already on the site.
<br><br><br>
    Spelling, grammar and punctuation are important! Remember, this is the first part of your question others will see - you want to make a good impression. If you're not comfortable writing in English, ask a friend to proof-read it for you.
<br><br><br>
    If you're having trouble summarizing the problem, write the title last - sometimes writing the rest of the question first can make it easier to describe the problem.
<br><br><br>

<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               include all relevant tags</span><br><br>


Try to include a tag for the language, library, and specific API your question relates to. If you start typing in the tags field, the system will suggest tags that match what you've typed - be sure and read the descriptions given for them to make sure they're relevant to the question you're asking! See also: What are tags, and how should I use them?
<br><br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Proof-read before posting!</span><br><br>


Now that you're ready to ask your question, take a deep breath and read through it from start to finish. Pretend you're seeing it for the first time: does it make sense? Try reproducing the problem yourself, in a fresh environment and make sure you can do so using only the information included in your question. Add any details you missed and read through it again. Now is a good time to make sure that your title still describes the problem!
<br><br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Post the question and respond to feedback</span><br><br>


After you post, leave the question open in your browser for a bit, and see if anyone comments. If you missed an obvious piece of information, be ready to respond by editing your question to include it. If someone posts an answer, be ready to try it out and provide feedback!
            </div>
        </div>
    </div>
</div>