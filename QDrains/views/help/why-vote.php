<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Why is voting important?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
               <!---<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Why is voting important?</span><br><br>--->






Voting is central to our model of providing quality questions and answers; it is how …
<br><br><br>
    ...good content rises to the top<br>
    ...incorrect content falls to the bottom<br>
    ...users who consistently provide useful content accrue reputation and are granted more privileges on the site
<br><br><br>
It’s only through voting that a class of editors, closers, and moderators can emerge to help run and govern the site. Voting is how site leadership forms. That’s why the reputation leagues show a breakdown of top users by reputation for the week, month, quarter, year, or all time.
<br><br><br>
Our sites are all intended to be a sort of representative democracy. Moderator elections are an important part of that plan, but voting on questions and answers is the primary mechanism through which the community governs the site on a day to day basis. Every user with sufficient reputation can exercise their right to vote, every day that they visit the site.
<br><br><br>
Voting is so important that there is a variety of badges associated with different aspects of voting – like casting your first up- or down vote, using up all of your allotted votes in a day, or casting upvotes on other people's answers to a question that you have answered yourself.
<br><br><br>
Voting up a question or answer signals to the rest of the community that a post is interesting, well-researched, and useful, while voting down a post signals the opposite: that the post contains wrong information, is poorly researched, or fails to communicate information. The more that people vote on a post, the more certain future visitors can be of the quality of information contained within that post – not to mention that upvotes are a great way to thank the author of a good post for the time and effort put into writing it!



         <br><br><br>   
            </div>
        </div>
    </div>
</div>