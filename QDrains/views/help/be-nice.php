<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Be nice. </span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
               <!----<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               what is their role here?</span><br><br>--->


Whether you've come to ask questions, or to generously share what you know, remember that we’re all here to learn, together. Be welcoming and patient, especially with those who may not know everything you do. Oh, and bring your sense of humor. Just in case.
<br><br>
That basically covers it. But these three guidelines may help:
<br><br>
    Rudeness and belittling language are not okay. Your tone should match the way you'd talk in person with someone you respect and whom you want to respect you. If you don't have time to say something politely, just leave it for someone who does.
<br><br>
    Be welcoming, be patient, and assume good intentions. Don't expect new users to know all the rules — they don't. And be patient while they learn. If you're here for help, make it as easy as possible for others to help you. Everyone here is volunteering, and no one responds well to demands for help.

         <br><br><br>   
            </div>
        </div>
    </div>
</div>