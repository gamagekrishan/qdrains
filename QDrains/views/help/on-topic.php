<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">What Topic can i ask here ?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
                <span style="font-style:inherit; font-weight:bold; font-size:16;">Some questions are still off-topic, even if they fit into one of the categories listed above:</span><br><br>

                Questions seeking debugging help ("why isn't this code working?") must include the desired behavior, a specific problem or error and the shortest code necessary to reproduce it in the question itself. Questions without a clear problem statement are not useful to other readers. See: How to create a Minimal, Complete, and Verifiable example.<br><br><br>

                Questions about a problem that can no longer be reproduced or that was caused by a simple typographical error. While similar questions may be on-topic here, these are often resolved in a manner unlikely to help future readers. This can often be avoided by identifying and closely inspecting the shortest program necessary to reproduce the problem before posting.<br><br><br>

                Questions asking for homework help must include a summary of the work you've done so far to solve the problem, and a description of the difficulty you are having solving it.<br><br><br>

                Questions asking us to recommend or find a book, tool, software library, tutorial or other off-site resource are off-topic for Stack Overflow as they tend to attract opinionated answers and spam. Instead, describe the problem and what has been done so far to solve it.<br><br><br>

                Questions about general computing hardware and software are off-topic for Stack Overflow unless they directly involve tools used primarily for programming.<br><br><br>

                Questions on professional server, networking, or related infrastructure administration are off-topic for Stack Overflow unless they directly involve programming or programming tools.<br><br><br>

            </div>
        </div>
    </div>
</div>