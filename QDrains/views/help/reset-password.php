<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">I lost my password; how do I reset it?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
                <span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               If you have lost your password,</span><br><br>



 please visit our account recovery page and enter the email address that you signed up with. We will email you a list of your account credentials and a link to reset your password.
<br><br>
If you used an external openID provider, you will need to contact that provider.

         <br><br><br>   
            </div>
        </div>
    </div>
</div>