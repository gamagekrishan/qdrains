<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Find Out More About QDrains</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_body">

            <!-------------------------------------->
            <div class="dp_wrapper_maincon_body_divset">
                <div class="dp_wrapper_maincon_body_divset_title">
                    Asking Questions
                </div>
                <div class="dp_wrapper_maincon_body_divset_content"> 
                    <ul class="a">
                        <li class="b"><a href="<?php echo URL; ?>help/info/on-topic" style="text-decoration: none;">What topic can i ask Here?</a></li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/question-bans" style="text-decoration: none;">Why are questions no longer being accepted from my account? </a></li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/how-to-ask" style="text-decoration: none;">How do I ask a good question?</a> </li>
                    </ul>  
                </div>
            </div>
            <!--------------------------------------->

            <!-------------------------------------->
            <div class="dp_wrapper_maincon_body_divset">
                <div class="dp_wrapper_maincon_body_divset_title">
                    Answring For Questions
                </div>
                <div class="dp_wrapper_maincon_body_divset_content"> 
                    <ul class="a">
                        <li class="b"><a href="<?php echo URL; ?>help/info/accepted-answer" style="text-decoration: none;">What does it mean when an answer is "accepted"?</a> </li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/how-to-answer" style="text-decoration: none;">How do I write a good answer? </a></li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/answer-bans" style="text-decoration: none;">Why are answers no longer being accepted from my account?</a> </li>
                        
                    </ul>
                </div>
            </div>
            <!--------------------------------------->

            <!-------------------------------------->
            <div class="dp_wrapper_maincon_body_divset">
                <div class="dp_wrapper_maincon_body_divset_title">
                    Profile
                </div>
                <div class="dp_wrapper_maincon_body_divset_content"> 
                    <ul class="a">
                        <li class="b"><a href="<?php echo URL; ?>help/info/reset-password" style="text-decoration: none;">I lost my password; how do I reset it? </a></li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/why-register" style="text-decoration: none;">How do I create an account?</a> </li>
                    </ul> 
                </div>
            </div>
            <!--------------------------------------->

            <!-------------------------------------->
            <div class="dp_wrapper_maincon_body_divset">
                <div class="dp_wrapper_maincon_body_divset_title">
                    Reputation
                </div>
                <div class="dp_wrapper_maincon_body_divset_content"> 
                    <ul class="a">
                        <li class="b"><a href="<?php echo URL; ?>help/info/why-vote" style="text-decoration: none;">Why is voting important?</a> </li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/whats-reputation" style="text-decoration: none;">What is reputation? How do I earn (and lose) it? </a></li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/site-moderators" style="text-decoration: none;">Who are the site moderators, and what is their role here?</a> </li>
                    </ul>  
                </div>
            </div>
            <!--------------------------------------->

            <!-------------------------------------->
            <div class="dp_wrapper_maincon_body_divset">
                <div class="dp_wrapper_maincon_body_divset_title">
                    Our System
                </div>
                <div class="dp_wrapper_maincon_body_divset_content"> 
                    <ul class="a">
                        <li class="b"><a href="<?php echo URL; ?>help/info/be-nice" style="text-decoration: none;">Be nice.</a> </li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/interesting-topics" style="text-decoration: none;">How do I find topics I'm interested in? </a></li>
                        <li class="b"><a href="<?php echo URL; ?>help/info/behavior" style="text-decoration: none;">What kind of behavior is expected of users?</a> </li>
                    </ul>
                </div>
            </div>
            <!--------------------------------------->


        </div>
    </div>
</div>