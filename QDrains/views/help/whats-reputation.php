<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">What is reputation? How do I earn (and lose) it?</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
               <!---<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               Why is voting important?</span><br><br>--->

Reputation is a rough measurement of how much the community trusts you; it is earned by convincing your peers that you know what you’re talking about. Basic use of the site, including asking questions, answering, and suggesting edits, does not require any reputation at all. But the more reputation you earn, the more privileges you gain.
<br><br>
The primary way to gain reputation is by posting good questions and useful answers.
<br><br>

<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               You gain reputation when:</span><br><br>



    question is voted up: +15<br>
    answer is voted up: +15<br>
    answer is marked “accepted”: +20 (+2 to acceptor)<br>
    

<br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               You lose reputation when:</span><br><br>


    your question is voted down: −5<br>
    your answer is voted down: −5<br>
    one of your posts receives 6 spam or offensive flags: −100
<br><br>
All users start with one reputation point, and reputation can never drop below 1. Accepting your own answer does not gain you any reputation. If a user reverses a vote, the corresponding reputation loss or gain will be reversed as well. Vote reversal as a result of voting fraud will also return lost or gained reputation.
<br><br>
At the high end of this reputation spectrum there is little difference between users with high reputation and ♦ moderators. That is intentional. We don’t run this site. The community does.


         <br><br><br>   
            </div>
        </div>
    </div>
</div>