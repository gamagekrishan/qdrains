<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Who are the site moderators</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_subpage_body">
            <div id="dp_wrapper_maincon_subpage_body_text">
               <span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               what is their role here?</span><br><br>


At QDrains, we believe moderation starts with the community itself, so in addition to all users gaining privileges through reputation earned, each site has moderators elected through popular vote. We hold regular elections to determine who these community moderators will be (except in the case of new beta sites, which have moderators pro tempore, who are appointed by Stack Exchange staff). Moderators are elected for life, though they may resign (or, in very rare cases, be removed).
<br><br><br>
<span style="font-style:inherit; font-weight:bold; font-size:16;">
                    
               We generally expect that moderators:</span>

<br><br>
    are patient and fair<br>
    lead by example<br>
    show respect for their fellow community members in their actions and words<br>
    are open to some light but firm moderation to keep the community on track and resolve (hopefully) uncommon disputes and exceptions



         <br><br><br>   
            </div>
        </div>
    </div>
</div>