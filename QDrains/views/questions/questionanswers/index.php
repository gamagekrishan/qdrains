<div id="dp_wrapper">
    <div id="dp_wrapper_maincon_head">
        <div id="mainconmenu">
            <ul>
                <li class='active'><span style="margin-left:5px; font-size:16px; font-weight:bold;"><?php echo $this->data[0]['title']; ?></span></li>
                <li class='activetwo'><span style="margin-left:5px;"><?php echo $this->data[0]['views']; ?></span></li>
            </ul>
        </div>
    </div>


    <div id="dp_wrapper_maincon"> 
        <!------questions----->
        <div id="dp_wrapper_maincon_question">
            <div id="dp_wrapper_maincon_body_status_con">
                <div id="dp_wrapper_maincon_body_status">
                    <a href="" id="up" class="qvote" rel="<?php echo $this->data[0]['qId']; ?>"><div class="dp_wrapper_maincon_body_status_count_vote"  style="background-image:url(<?php echo URL; ?>public/images/up.png);"></div></a>
                </div>
                <div id="dp_wrapper_maincon_body_status">
                    <div id="dp_wrapper_maincon_body_status_count_answers"> <span id="<?php echo $this->data[0]['qId']; ?>q"><?php echo $this->data[0]['votes']; ?></span> </div>
                </div>
                <div id="dp_wrapper_maincon_body_status">
                    <a href="" id="down" class="qvote" rel="<?php echo $this->data[0]['qId']; ?>"><div class="dp_wrapper_maincon_body_status_count_views"  style=" background-image:url(<?php echo URL; ?>public/images/down.png);"></div></a>
                </div>
                <div id="dp_wrapper_maincon_body_status" style="margin-top:-20px;">
                    <div id="dp_wrapper_maincon_body_status_count_star" style="background-image:url(<?php echo URL; ?>public/images/star.png);"></div>
                </div>
                <div id="dp_wrapper_maincon_body_status">
                    <div id="dp_wrapper_maincon_body_status_count_star_count"> <span id="font">0</span> </div>
                </div>
                <div id="dp_wrapper_maincon_body_details">
                    <div id="dp_wrapper_maincon_body_content"> <span><?php echo html_entity_decode($this->data[0]['content']); ?></span> </div>
                    <div id="dp_wrapper_maincon_body_tags_tab"> 
                        <?php foreach ($this->data[0]['tags'] as $tag) { ?>
                            <a id="dp_wrapper_maincon_body_tags_tab_tag"><?php echo $tag; ?></a> 
                        <?php } ?>

                    </div>
                    <br>

                    <div id="dp_wrapper_maincon_body_tags_close">
                        <?php if (Session::get('rep') > 500) { ?>
                            <a href="" rel="<?php echo $this->data[0]['qId']; ?>" id="quesclose"><span id="dp_wrapper_maincon_body_tags_tab_tag" style="color:#F00;">Close</span><br></a>
                        <?php } ?>
                    </div><br>

                    <br>
                    <div id="dp_wrapper_maincon_body_status_con_two">
                        <div id="dp_wrapper_maincon_body_status_two">
                            <div id="dp_wrapper_maincon_body_status_asked">
                                <div id="font">Asked</div>
                            </div>
                            <div id="dp_wrapper_maincon_body_status_time">
                                <div id="font"><?php echo $this->data[0]['ask_date']; ?></div>
                            </div>
                        </div>
                        <div id="dp_wrapper_maincon_body_status_three">
                            <div id="dp_wrapper_maincon_body_status_three_image"> <a href="<?php echo URL . 'users/' . $this->data[0]['user_Id'] . '/' . $this->data[0]['user'][0]['l_name'] ?>"><img src="<?php echo $this->data[0]['user'][0]['profile_image']; ?>" id="dp_wrapper_maincon_body_status_three_image"></a> </div>
                            <div id="dp_wrapper_maincon_body_status_three_username"> &nbsp;&nbsp;<?php echo $this->data[0]['user'][0]['l_name']; ?> </div>
                            <div id="dp_wrapper_maincon_body_status_three_reputation">
                                <div id="font">&nbsp;&nbsp;<?php echo $this->data[0]['user'][0]['reputation']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---------------------------------------->
        <?php foreach ($this->data[0]['comments'] as $comment) { ?>
            <div id="add_comment_con">
                <div id="add_comment_borderdiv">
                    <div id="add_comment"> 
                        <span><?php echo $comment['content']; ?></span>
                        <a style=" text-decoration: none;" href="<?php echo URL . 'users/' . $comment['user_id'] . '/' . $comment['l_name'] ?>">
                            <span id="dp_wrapper_maincon_answers_tags_tab_tag2" style="color:#F00; font-size:13px;margin-left: 10px;"><?php echo $comment['l_name']; ?></span></a>
                        <span id="dp_wrapper_maincon_answers_tags_tab_tag" style="color:#00F; font-size:13px; background-color: #999; margin-left: 5px;"><?php echo $comment['post_date']; ?></span> 
                    </div>
                </div>
            </div>
        <?php } ?>
        <!---------------------------------------> 
        <div id="add_comment_textdiv">
            <a href="" class="com" id="quescom" style="margin-left:65px; color: #999; text-decoration: none;">Add Comment</a>
        </div> 
        <form action="" method="post" name="comform" id="formques">
            <div id="quescomdiv" class="quescom add_comment_postdev">
                <input type="hidden"  name="quesid" value="<?php echo $this->data[0]['qId']; ?>">
                <textarea name="quescomcontent"></textarea>
                <div id="asdf" style="margin-top: 8px;">
                    <span style=" margin-left: 10px; color: red; " id="quescomstatus"></span>
                </div>
                <div id="save_btncon">
                    <button class="btn2" id="">Post</button>
                </div>
            </div>
        </form>
        <!-----------end of questions---------> 
        <!----answers---->
        <div id="dp_wrapper_maincon_answers">
            <div id="subconmenu">
                <ul>
                    <li class='active'><span id="font">Answers</span></li>
                </ul>
            </div>
            <?php foreach ($this->data[0]['ans'] as $ans) { ?>
                <div id="dp_wrapper_maincon_answers_con_border">
                    <div id="dp_wrapper_maincon_answers_con">
                        <div id="dp_wrapper_maincon_answers_status_con">
                            <div id="dp_wrapper_maincon_answers_status">
                                <a href="" id="up" class="avote" rel="<?php echo $ans['ans_Id']; ?>"> <div class="dp_wrapper_maincon_answers_status_count_vote" style="background-image:url(<?php echo URL; ?>public/images/up.png);"></div></a>
                            </div>
                            <div id="dp_wrapper_maincon_answers_status">
                                <div id="dp_wrapper_maincon_answers_status_count_answers"> <span id="<?php echo $ans['ans_Id']; ?>a"><?php echo $ans['votes']; ?></span> </div>
                            </div>
                            <div id="dp_wrapper_maincon_answers_status">
                                <a href="" id="down" class="avote" rel="<?php echo $ans['ans_Id']; ?>"><div class="dp_wrapper_maincon_answers_status_count_views" style="background-image:url(<?php echo URL; ?>public/images/down.png);"></div></a>
                            </div>
                            <div id="dp_wrapper_maincon_answers_status" style="margin-top:-20px;">
                                <?php if ($ans['acceptance'] == 'yes') { ?>
                                    <div id="dp_wrapper_maincon_answers_status_count_star" style="background-image:url(<?php echo URL; ?>public/images/right.png);"></div>
                                <?php } ?>
                            </div>

                            <div id="dp_wrapper_maincon_answers_details">
                                <div id="dp_wrapper_maincon_answers_content"> 
                                    <span><?php echo html_entity_decode($ans['content']); ?></span> 
                                </div>
                                <div id="dp_wrapper_maincon_answers_close">
                                    <?php if (Session::get('rep') > 500) { ?>
                                        <a href="" rel="<?php echo $ans['ans_Id']; ?>" class="ansclose"><span id="dp_wrapper_maincon_body_tags_tab_tag" style="color:#F00;">Close</span></a>
                                    <?php
                                    }
                                    if ($this->data[0]['user_Id'] == Session::get('uId')) {
                                        ?>
                                        <?php if ($ans['acceptance'] == 'no') { ?>
                                            <a href="" rel="<?php echo $ans['ans_Id']; ?>" class="ansaccept"><span id="dp_wrapper_maincon_body_tags_tab_tag" style="color:#F00;">Accept</span></a>
        <?php }
    } ?>
                                </div><br>
                                <br>
                                <div id="dp_wrapper_maincon_answers_status_con_two">
                                    <div id="dp_wrapper_maincon_answers_status_two">
                                        <div id="dp_wrapper_maincon_answers_status_asked">
                                            <div id="font">Asked</div>
                                        </div>
                                        <div id="dp_wrapper_maincon_answers_status_time">
                                            <div id="font"><?php echo $ans['post_date']; ?></div>
                                        </div>
                                    </div>
                                    <div id="dp_wrapper_maincon_answers_status_three">
                                        <div id="dp_wrapper_maincon_answers_status_three_image"> 
                                            <a href="<?php echo URL . 'users/' . $ans['user_Id'] . '/' . $ans['l_name']; ?>">
                                                <img src="<?php echo $ans['profile_image']; ?>" id="dp_wrapper_maincon_answers_status_three_image"/>
                                            </a> 
                                        </div>
                                        <div id="dp_wrapper_maincon_answers_status_three_username"> &nbsp;&nbsp;<?php echo $ans['l_name']; ?> </div>
                                        <div id="dp_wrapper_maincon_answers_status_three_reputation">
                                            <div id="font">&nbsp;&nbsp;<?php echo $ans['reputation']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------------------------------------->
                    <div id="<?php echo $ans['ans_Id']; ?>">
    <?php foreach ($ans['comments'] as $anscomment) { ?>
                            <div id="answers_add_comment_con">
                                <div id="answers_add_comment_borderdiv">
                                    <div id="answers_add_comment"> 
                                        <span><?php echo $anscomment['content']; ?></span>
                                        <span id="dp_wrapper_maincon_answers_tags_tab_tag2" style="color:#F00; font-size:13px; margin-left: 10px;">
                                            <a style="text-decoration:none;" href="<?php echo URL . 'users/' . $anscomment['user_id'] . '/' . $anscomment['l_name']; ?>"> <?php echo $anscomment['l_name']; ?></a>
                                        </span>
                                        <span id="dp_wrapper_maincon_answers_tags_tab_tag" style="color:#00F; font-size:13px; margin-left: 5px; background-color: #999;"><?php echo $anscomment['post_date']; ?></span> 
                                    </div>
                                </div>
                            </div>
    <?php } ?>
                    </div>
                    <!---------------------------------------> 

                    <div class="add_answers_comment_textdiv">
                        <a href="" class="com" id="<?php echo $ans['ans_Id']; ?>" style="margin-left:65px; color: #999; text-decoration: none;">Add Comment</a>
                    </div>
                    <!--------------------\------------------------------->
                    <form action="" method="post" name="comform" class="formans">
                        <div id="<?php echo 'ans' . $ans['ans_Id']; ?>" class="anscom add_answers_comment_postdev">
                            <input type="hidden" id="phpVar" name="ansid" value="<?php echo $ans['ans_Id']; ?>">
                            <textarea name="commentcontent"></textarea>

                            <div id="asdf" style="margin-top: 8px;">
                                <span style=" margin-left: 10px; color: red; " id="content<?php echo $ans['ans_Id']; ?>"></span>
                            </div>
                            <div id="save_btncon">
                                <input type="submit" value="Post" class="btn2" />
                            </div>
                        </div>
                    </form>
                </div>
<?php } ?>
        </div>
        <!-----------end of answers---------> 
        <!------youer Answer---->
        <div id="dp_wrapper_maincon_postanswer">
            <div id="subconmenu">
                <ul>
                    <li class='active'><span id="font">Post Your Answers</span></li>
                </ul>
            </div>
            <form action="#" method="post" id="answerpostform">
                <div id="dp_wrapper_maincon_postanswer_textarea">
                    <input type="hidden"  name="quesid" value="<?php echo $this->data[0]['qId']; ?>">
                    <textarea name="answercontent"></textarea>

                    <div id="asdf" style="margin-top: 8px;">
                        <span style=" margin-left: 10px; color: red; " id="anspoststatus"></span>
                    </div>
                    <div id="save_btncon">
                        <input type="submit" value="Post" id="btn" />
                    </div>
                </div>
            </form>
        </div>
        <!------ end of youer Answer----> 
    </div>
    <div id="dialog"></div>
</div>

