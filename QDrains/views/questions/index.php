<div id="dp_wrapper">
    <?php
    $quesData = $this->quesData['data'];
    $pageSetupData = $this->quesData['pagesetupdata'];
    ?>
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style=" margin-left:5px;font-size:16px; font-weight:bold;">All Questions</span></li>
                    <li <?php
                    if (isset($this->submenu) && $this->submenu == "newest") {
                        echo 'style="background-color: white;"';
                    }
                    ?>
                        ><a href='<?php echo URL; ?>questions?tab=newest'><span>Newest</span></a></li>
                    <li <?php
                    if (isset($this->submenu) && $this->submenu == "unanswered") {
                        echo 'style="background-color: white;"';
                    }
                    ?>
                        ><a href='<?php echo URL; ?>questions?tab=unanswered'><span>Unanswered</span></a></li>
                </ul>
            </div>
        </div>

        <?php foreach ($quesData as $ques) { ?>
            <div id="dp_wrapper_maincon_body">
                <div id="dp_wrapper_maincon_body_status_con">
                    <div id="dp_wrapper_maincon_body_status">
                        <div id="dp_wrapper_maincon_body_status_count_vote"> <span id="font"><?php echo $ques['votes'] ?></span> </div>
                        <span id="font">Vote</span> </div>
                    <div id="dp_wrapper_maincon_body_status">
                        <div id="dp_wrapper_maincon_body_status_count_answers"> <span id="font"><?php echo $ques['ans'] ?></span> </div>
                        <span id="font">Answers</span> </div>
                    <div id="dp_wrapper_maincon_body_status">
                        <div id="dp_wrapper_maincon_body_status_count_views"> <span id="font"><?php echo $ques['views'] ?></span> </div>
                        <span id="font">Views</span> </div>
                </div>
                <div id="dp_wrapper_maincon_body_details">
                    <div id="dp_wrapper_maincon_body_titleofq"> <span id="font"><a href="<?php echo URL . 'questions/' . $ques['qId'] . '/' . str_replace(' ', '-', $ques['title']); ?>"><?php echo $ques['title'] ?></a></span> </div>
                    <div id="dp_wrapper_maincon_body_content"> <span><?php echo substr(strip_tags(html_entity_decode($ques['content'])), 0, 400) ?></span> </div>
                    <div id="dp_wrapper_maincon_body_tags_tab"> 
                        <?php foreach ($ques['tags'] as $tag) { ?>
                            <a href="#" id="dp_wrapper_maincon_body_tags_tab_tag"><?php echo $tag; ?></a>
                        <?php } ?>
                    </div>
                    <div id="dp_wrapper_maincon_body_status_con_two">
                        <div id="dp_wrapper_maincon_body_status_two">
                            <div id="dp_wrapper_maincon_body_status_asked">
                                <div id="font">Asked</div>
                            </div>
                            <div id="dp_wrapper_maincon_body_status_time">
                                <div id="font" style="color: #666"><?php echo $ques['ask_date'] ?></div>
                            </div>
                        </div>
                        <div id="dp_wrapper_maincon_body_status_three">
                            <div id="dp_wrapper_maincon_body_status_three_image"> <a href="<?php echo URL . 'users/' . $ques['user_Id'] . '/' . $ques['user'][0]['l_name']; ?>"><img src="<?php echo $ques['user'][0]['profile_image']; ?>" id="dp_wrapper_maincon_body_status_three_image"></a> </div>
                            <div id="dp_wrapper_maincon_body_status_three_username"> &nbsp;&nbsp;<a href="<?php echo URL . 'users/' . $ques['user_Id'] . '/' . $ques['user'][0]['l_name']; ?>" style="text-decoration: none;"><?php echo $ques['user'][0]['l_name']; ?></a> </div>
                            <div id="dp_wrapper_maincon_body_status_three_reputation">
                                <div id="font">&nbsp;&nbsp;<?php echo $ques['user'][0]['reputation'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>





    </div>
    <div id="dp_wrapper_foot">
        <div id="dp_wrapper_foot_left">
            <?php
            //$url = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL);
            //$url = urlencode($url);
            $tab = isset($this->submenu) ? 'tab=' . $this->submenu : '';
            $url = URL . 'questions?' . $tab;
            if ($pageSetupData['cpage'] != 1) {
                ?>
                <div id="dp_wrapper_foot_left_prve"> <a href="<?php $nPage = $pageSetupData['cpage'] - 1;
            echo $url . '&page=' . $nPage;
                ?>" style="text-decoration:none;">Prev</a> </div>
                                                        <?php
                                                    }
                                                    if ($pageSetupData['cpage'] < 5) {
                                                        $i = 0;
                                                        for ($i = 1; $i < $pageSetupData['allpages']; $i++) {
                                                            if ($i > 5) {
                                                                break;
                                                            }
                                                            ?>
                    <a href="<?php echo $url . '&page=' . $i; ?>" style="text-decoration:none;">
                        <div id="dp_wrapper_foot_left_number" <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo $i ?> </div>
                        </a>
                <?php } ?>
                <?php if (($pageSetupData['allpages'] - $i) >= 1) { ?>
                    <div id="dp_wrapper_foot_left_spece"> ... </div>
    <?php } ?>



            <?php
            }
            if ($pageSetupData['cpage'] >= 5) {
                $i = 0;
                ?>

                    <a href="<?php echo $url . '&page=1'; ?>" style="text-decoration:none;">
                    <div id="dp_wrapper_foot_left_number" <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo 1; ?> </div>
                    </a>
                <div id="dp_wrapper_foot_left_spece"> ... </div>
                <?php
                for ($i = $pageSetupData['cpage'] - 2; $i < $pageSetupData['cpage'] + 3; $i++) {
                    if ($i >= $pageSetupData['allpages']) {
                        break;
                    }
                    ?>
                <a href="<?php echo $url . '&page=' . $i; ?>" style="text-decoration:none;">
                <div id="dp_wrapper_foot_left_number" <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo $i ?> </div>
                </a>
                    <?php
                }
                if (($pageSetupData['allpages'] - $i) >= 1) {
                    ?>
                    <div id="dp_wrapper_foot_left_spece"> ... </div>



    <?php }
}
?>
            <a href="<?php echo $url . '&page=' . $pageSetupData['allpages']; ?>" style="text-decoration:none;">
                <div id="dp_wrapper_foot_left_count"  <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo $pageSetupData['allpages']; ?> </div>
            </a>
                                                    <?php if ($pageSetupData['cpage'] != $pageSetupData['allpages']) { ?>
                <div id="dp_wrapper_foot_left_next"> <a href="<?php $nPage = $pageSetupData['cpage'] + 1;
                                                    echo $url . '&page=' . $nPage;
                                                    ?>" style="text-decoration:none;">Next</a> </div>
<?php } ?>
        </div>
        <div id="dp_wrapper_foot_right">
           <a href="<?php echo $url . '&pagesize=5'; ?>" style="text-decoration:none;"> <div id="dp_wrapper_foot_right_counts_one" <?php if (Session::get('pageSize') == 5) { ?>style="background-color: #CCC" <?php } ?>> 5 </div></a>
            <a href="<?php echo $url . '&pagesize=10'; ?>" style="text-decoration:none;"><div id="dp_wrapper_foot_right_counts_two" <?php if (Session::get('pageSize') == 10) { ?>style="background-color: #CCC" <?php } ?>> 10 </div></a>
            <a href="<?php echo $url . '&pagesize=20'; ?>" style="text-decoration:none;"><div id="dp_wrapper_foot_right_counts_three" <?php if (Session::get('pageSize') == 20) { ?>style="background-color: #CCC" <?php } ?>> 20 </div></a>
            <div id="dp_wrapper_foot_left_perpage"> per page</div>
        </div>
    </div>
</div>