<div id="dp_wrapper">
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="font-size:16px; font-weight:bold;">Create Your New Account</span></li>
                </ul>
            </div>
        </div>
        <div id="dp_wrapper_maincon_body">
            <div id="dp_wrapper_maincon_body_div1">Confirm to continue to QDrains</div>
            <div id="dp_wrapper_maincon_body_div2">
                <div id="dp_wrapper_maincon_body_div2_con1" style="background-image:url(<?php echo $this->data['picture'];?>); background-repeat:no-repeat; background-size:cover;"></div>
                <div id="dp_wrapper_maincon_body_div2_con2">
                    <span><?php echo $this->data['name']; ?></span>
                </div>
                <div id="dp_wrapper_maincon_body_div2_con3">
                    <span><?php echo $this->data['email']; ?></span>
                </div>
                <input type="button" value="Confirm" onclick="location.href='<?php echo URL; ?>users/registerGoogleUser'" id="btn" /><br>
                <div id="dp_wrapper_maincon_body_div2_con4">
                    <a href="<?php echo URL; ?>users/login" style="text-decoration:none;">Cancel</a>
                </div>
            </div>
            <div id="dp_wrapper_maincon_body_div3">This Informations are provide by Google. After successfully registered you can change these details</div>
        </div>
    </div>
</div>