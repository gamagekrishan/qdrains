<!---answers--->
        <div id="dp_wrapper_maincontwo_answers">
            <?php $summaryAns = $this->summaryTabData['ans']['ans'];
             $summaryQues = $this->summaryTabData['ques']['ques'];
            ?>
            <div id="dp_wrapper_maincontwo_answers_head">
                <div id="subconmenu">
                    <ul>
                        <li class='active'><span id="font">Answers</span></li>
                        <li class="activetwo"><span id="font"> <?php echo $this->summaryTabData['ans']['allans']; ?> </span></li>
                    </ul>
                </div>
            </div>
            <?php foreach ($summaryAns as $ans){ ?>
            <div id="dp_wrapper_maincontwo_answers_content">
                <div id="dp_wrapper_maincontwo_answers_content_status">
                    <div id="dp_wrapper_maincontwo_answers_content_status_count">
                        <?php echo $ans['vote']; ?>
                    </div>
                    <span id="font">Votes</span>
                </div>
                <div id="dp_wrapper_maincontwo_answers_content_details">
                    <?php echo substr(strip_tags(html_entity_decode($ans['content'])),0,400); ?>
                </div>
            </div>
            <?php } ?>
        </div>
        <!---end of answers--->
        <!----questions--->
        <div id="dp_wrapper_maincontwo_questions">
            <div id="dp_wrapper_maincontwo_questions_head">
                <div id="subconmenu">
                    <ul>
                        <li class='active'><span id="font">Questions</span></li>
                        <li class="activetwo"><span id="font"><?php echo $this->summaryTabData['ques']['allques']; ?></span></li>
                    </ul>
                </div>
            </div>
            <?php foreach ($summaryQues as $ques){ ?>
            <div id="dp_wrapper_maincontwo_questions_content">
                <div id="dp_wrapper_maincontwo_questions_content_status">
                    <div id="dp_wrapper_maincontwo_questions_content_status_count">
                        <?php echo $ques['vote']; ?>
                    </div>
                    <span id="font">Votes</span>
                </div>
                <div id="dp_wrapper_maincontwo_questions_content_details">
                    <?php echo $ques['title']; ?>
                </div>
            </div>
            <?php } ?>
        </div>
        <!---end of questions--->