
            <div id="dp_wrapper_maincontwo_tagspage">
                <?php $tags = $this->TagTabData['tags']; ?>
                <div id="dp_wrapper_maincontwo_tagspage_head">
                    <div id="dp_wrapper_maincontwo_tagspage_head_con1"><span id="font"><?php echo $this->TagTabData['alltags']; ?></span></div>
                    <div id="dp_wrapper_maincontwo_tagspage_head_con2"><span id="font">&nbsp;Tags</span></div>
                    
                    
                    <?php foreach ($tags as $tag) { ?>
                    <div id="dp_wrapper_maincontwo_tagspage_body">
                        <div id="dp_wrapper_maincontwo_tagspage_content_borderdiv">
                            <div id="dp_wrapper_maincontwo_tagspage_content"> 
                                <!-------------------------------------->
                                <div id="dp_wrapper_maincontwo_tagspage_status_con">
                                    <div id="dp_wrapper_maincontwo_tagspage_status">
                                        <div id="dp_wrapper_maincontwo_tagspage_status_count_vote"> <span id="font"><?php echo rand(5, 50); ?></span> </div>
                                        <span id="font">Follows</span> </div>
                                    <div id="dp_wrapper_maincontwo_tagspage_status">
                                        <div id="dp_wrapper_maincontwo_tagspage_status_count_answers"> <span id="font"><?php echo $tag['usage']; ?></span> </div>
                                        <span id="font">Useage</span> </div>
                                    <div id="dp_wrapper_maincontwo_tagspage_content_title"><?php echo $tag['title']; ?></div>
                                    <div id="dp_wrapper_maincontwo_tagspage_content_content"><?php echo $tag['content']; ?></div>
                                </div>
                                <!----------------------------------------->
                                <?php if($tag['status'] == 'closed') { ?>
                                <div id="dp_wrapper_maincontwo_tagspage_content_closereq">
                                    <div id="close_request">
                                        <div id="close_request_tag" style="margin-left:5px; margin-top:5px; border-radius:3px;"><span style="margin-left:5px;">Close Request</span></div>
                                        <div id="close_request_content"><span id="font">This tag is closed by community as not useful</span></div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div id="dp_wrapper_foot">
                     <!--page setup here -->
                </div>
            </div>

        </div>