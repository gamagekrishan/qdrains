<div id="dp_wrapper">
    <!---maincon--->
    <?php $bioData = $this->bioData[0];
    $repData = $this->repData;
    ?>
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="margin-left: 5px; font-size: 16px; font-weight: bold;"><?php echo $bioData['f_name'].' '.$bioData['l_name']; ?></span></li>
                    <?php if(isset($_SESSION['uId']) && $_SESSION['uId'] == $bioData['user_Id']) { ?><li><a href='<?php echo URL; ?>users/edit'><span>Edit</span></a></li> 
                    <li><a href='<?php echo URL; ?>privileges'><span>Privileges</span></a></li>
                    <?php }?>
                </ul>
            </div>
        </div>
        <!---middle content---->
        <div id="dp_wrapper_maincon_body">
            <div id="dp_wrapper_maincon_body_content">
                <!---profile details--->
                <div id="foot_con_main">
                    <div id="foot_con_pic" style="background-image:url( <?php echo $bioData['profile_image']; ?> );">
                    </div>
                    <div id="foot_con_pack">
                        <div id="foot_con_name">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Name</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php echo $bioData['f_name'].' '.$bioData['l_name']; ?>
                            </div>
                        </div>
                        <div id="foot_con_address">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Location</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php echo $bioData['address']; ?>
                            </div>
                        </div>
                        <div id="foot_con_reputation">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Reputation</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php echo $bioData['reputation']; ?>
                            </div>
                        </div>
                        <div id="foot_con_gender">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Gender</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php if($bioData['gender'] == 'm'){echo "Male";}if($bioData['gender'] == 'f'){echo "Female";} ?>
                            </div>
                        </div>
                    </div>
                    <div id="foot_con_bestfor">
                        <div id="foot_con_email">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Email</span>
                            </div>
                            <div class="foot_con_name_name_email">
                                <span style="margin-left: 5px;">
                                    <?php echo $bioData['email']; ?>
                                </span>
                            </div>
                        </div>
                        <div id="foot_con_joind">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Joined</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php echo $bioData['join_date']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!----about me--->
                <div id="foot_con_aboutme">
                    <div id="foot_con_aboutme_top">
                        <span id="font">About Me</span>
                    </div>
                    <div style="margin-top: 5px; margin-left: 5px; word-wrap: break-word; overflow: auto; height: 155px;">
                        <?php echo html_entity_decode($bioData['about']); ?>
                    </div>
                </div>
                <!---end of about me--->
                <!--- end of profile details--->
            </div>
            <!---reputation---->
            <div id="dp_wrapper_maincon_reputation">
                <div id="dp_wrapper_maincon_reputation_head">
                    <div id="subconmenu">
                        <ul>
                            <li class='active'><span id="font">Reputation</span></li>
                            <li class="activetwo"><span id="font"><?php echo $bioData['reputation']; ?></span></li>
                        </ul>
                    </div>
                </div>
                <div id="dp_wrapper_maincon_body_chart">
                    <div style="width:60%; margin:0 auto 0px; margin: 10px;">
                        <div>
                            <canvas id="canvas" height="250" width="550"></canvas>
                        </div>
                    </div>

                    <script>
                        var lineChartData = {
                            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                            datasets: [
                                
                                {
                                    label: "My Second dataset",
                                    fillColor: "rgba(151,187,205,0.2)",
                                    strokeColor: "rgba(151,187,205,1)",
                                    pointColor: "rgba(151,187,205,1)",
                                    pointStrokeColor: "#fff",
                                    pointHighlightFill: "#fff",
                                    pointHighlightStroke: "rgba(151,187,205,1)",
                                    data: [ <?php echo $repData[1]; ?>, <?php echo $repData[2]; ?>, <?php echo $repData[3]; ?>, <?php echo $repData[4]; ?>, <?php echo $repData[5]; ?>, <?php echo $repData[6]; ?>, <?php echo $repData[7]; ?>, <?php echo $repData[8]; ?>, <?php echo $repData[9]; ?>, <?php echo $repData[10]; ?>, <?php echo $repData[11]; ?>, <?php echo $repData[12]; ?>]
                                }
                            ]

                        };

                        window.onload = function() {
                            var ctx = document.getElementById("canvas").getContext("2d");
                            window.myLine = new Chart(ctx).Line(lineChartData, {
                                responsive: false
                            });
                            
                            //header script start------
                           
                            //header script end
                        };


                    </script>
                </div>
                <div id="dp_wrapper_maincon_reputation_detailscon">
                    <span style="color: #999;">Reputation chart that user earn for each month of this year</span>
                </div>
            </div>
            <!---end of reputation--->
        </div>
        <!---end of middle content (body)--->
        <!---end of maincon--->
    </div>
    <!--maincon two--->
    <div id="dp_wrapper_maincontwo">
        <div id="dp_wrapper_maincontwo_head">
            <div id="mainconmenu">
                <ul>
                    <li><a <?php
                        if (isset($this->submenu) && $this->submenu == "summary") {
                            echo 'style="background-color: white;"';
                        }
                        ?> href='<?php echo URL.'users/'.$bioData['user_Id'].'/'.$bioData['l_name'].'+'.$bioData['f_name'].'?tab=summary'; ?>'><span>Summary</span></a></li>
                    <li><a <?php
                        if (isset($this->submenu) && $this->submenu == "answers") {
                            echo 'style="background-color: white;"';
                        }
                        ?> href='<?php echo URL.'users/'.$bioData['user_Id'].'/'.$bioData['l_name'].'+'.$bioData['f_name'].'?tab=answers'; ?>'><span>Answers</span></a></li>
                    <li><a <?php
                        if (isset($this->submenu) && $this->submenu == "questions") {
                            echo 'style="background-color: white;"';
                        }
                        ?> href='<?php echo URL.'users/'.$bioData['user_Id'].'/'.$bioData['l_name'].'+'.$bioData['f_name'].'?tab=questions'; ?>'><span>Questions</span></a></li>
                    <li><a <?php
                        if (isset($this->submenu) && $this->submenu == "tags") {
                            echo 'style="background-color: white;"';
                        }
                        ?> href='<?php echo URL.'users/'.$bioData['user_Id'].'/'.$bioData['l_name'].'+'.$bioData['f_name'].'?tab=tags'; ?>'><span>Tags</span></a></li>
                    <li><a <?php
                        if (isset($this->submenu) && $this->submenu == "reputation") {
                            echo 'style="background-color: white;"';
                        }
                        ?>href='<?php echo URL.'users/'.$bioData['user_Id'].'/'.$bioData['l_name'].'+'.$bioData['f_name'].'?tab=reputation'; ?>'><span>Reputation</span></a></li>
                </ul>
            </div>
        </div>
        <!---------------------------------------------------------->
        <div id="dp_wrapper_maincontwo_body">