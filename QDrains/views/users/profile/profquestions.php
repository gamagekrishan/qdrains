
            <div id="dp_wrapper_maincontwo_questionspage">
                <?php $quesTabData = $this->quesTabData['ques']; ?>
                <div id="dp_wrapper_maincontwo_questionspage_head">
                    <div id="dp_wrapper_maincontwo_questionspage_head_con1"><span id="font"><?php echo $this->quesTabData['allques']; ?></span></div>
                    <div id="dp_wrapper_maincontwo_questionspage_head_con2"><span id="font">&nbsp;Questions</span></div>
<!--                    <div id="dp_wrapper_maincontwo_questionspage_head_con3"><a href="" id="dp_wrapper_maincon_body_tags_tab_tag">Closed</a> <a href="" id="dp_wrapper_maincon_body_tags_tab_tag">Vote</a> <a href="" id="dp_wrapper_maincon_body_tags_tab_tag">Active</a> <a href="" id="dp_wrapper_maincon_body_tags_tab_tag">Newest</a></div>-->
                </div>
                
                <?php foreach($quesTabData as $ques) { ?>
                <div id="dp_wrapper_maincontwo_questionspage_body">
                    <div id="dp_wrapper_maincontwo_questionspage_content_borderdiv">
                        <div id="dp_wrapper_maincontwo_questionspage_content"> 
                            <!--------------------------------------->
                            <div id="dp_wrapper_maincontwo_questionspage_content_statuscon">
                                <div id="dp_wrapper_maincontwo_questionspage_content_status">
                                    <div id="dp_wrapper_maincontwo_questionspage_content_status_count_image" style="background-image:url(<?php echo URL;?>public/images/star.png); background-repeat:no-repeat; background-size:cover;"></div>
                                    <span id="font">0</span> </div>
                                <div id="dp_wrapper_maincontwo_questionspage_content_status">
                                    <div id="dp_wrapper_maincontwo_questionspage_content_status_count"><span id="font"><?php echo $ques['vote']; ?></span></div>
                                    <span id="font">Votes</span> </div>
                                <div id="dp_wrapper_maincontwo_questionspage_content_status">
                                    <div id="dp_wrapper_maincontwo_questionspage_content_status_count"><span id="font"><?php echo $ques['answers']; ?></span></div>
                                    <span id="font">Answers</span> </div>
                                <div id="dp_wrapper_maincontwo_questionspage_content_status">
                                    <div id="dp_wrapper_maincontwo_questionspage_content_status_count"><span id="font"><?php echo $ques['views']; ?></span></div>
                                    <span id="font">Views</span> </div>
                            </div>
                            <!---------------------------------------> 
                            <span id="dp_wrapper_maincon_body_tags_tab_tag" style="float:right; margin-right:10px; margin-top:5px;">Jan 16 2014</span>
                            <div id="dp_wrapper_maincontwo_questionspage_content_details">
                                <?php echo substr(strip_tags(html_entity_decode($ques['content'])),0,400); ?>
                            </div>
                            <div id="dp_wrapper_maincon_body_tags_tab"> 
                               <?php foreach ($ques['tags'] as $tag) { ?>
                                <a href="" id="dp_wrapper_maincon_body_tags_tab_tag"><?php echo $tag['title']; ?></a> 
                               <?php }?>
                            </div>
                            <?php if ($ques['status'] == 'closed') { ?>
                            <div id="dp_wrapper_maincontwo_questionspage_content_closereq">
                                <div id="close_request">
                                    <div id="close_request_tag"><span style="margin-left: 5px;">Close Request</span></div>
                                    <div id="close_request_content"><span id="font">This question closed by the community as not useful question</span></div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
                
            </div>
            <div id="dp_wrapper_foot">
                 <!--page setup here -->
            </div>