<div id="dp_wrapper">
    <?php $usersData = $this->usersData['data']; 
    $pageSetupData = $this->usersData['pagesetupdata'];
    ?>
    
    <div id="dp_wrapper_maincon">
        <div id="dp_wrapper_maincon_head">
            <div id="mainconmenu">
                <ul>
                    <li class='active'><span style="margin-left: 5px; font-weight: bold; font-size: 18px;">Users</span></li>
                    
                    <li <?php if(isset($this->submenu) && $this->submenu == "rep"){
                        echo 'style="background-color: white; "';} ?> 
                        ><a href='<?php echo URL; ?>users?tab=reputation'><span>Reputation</span></a></li>
                    <li <?php if(isset($this->submenu) && $this->submenu == "new"){
                        echo 'style="background-color: white;"';} ?> 
                        ><a href='<?php echo URL; ?>users?tab=new'><span>new Users</span></a></li>
                </ul>
            </div>
        </div>
        <!----body--->
        <div id="dp_wrapper_maincon_content">
        <div id="dp_wrapper_maincon_body">
            <!---type div--->
            <div id="dp_wrapper_maincon_body_type">
                <div id="dp_wrapper_maincon_body_type_text">
                    <span id="font">Type to find users</span>
                </div>
                <div id="dp_wrapper_maincon_body_type_textcon">
                    <input type="text" id="textboxstyle"/>
                    <div id="dp_wrapper_maincon_body_wait" style="float: right; padding-top: 5px;">
                     <div style="display: none" id="waitdiv"><img src="<?php echo URL; ?>public/images/wait.gif"></div>
                </div>
                </div>
                <!--- letter box--->
                <div id="dp_wrapper_maincon_body_letters">
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">A</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">B</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">C</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">D</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">E</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">F</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">G</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">H</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">I</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">J</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">K</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">L</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">M</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">N</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">O</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">P</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">Q</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">R</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">S</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">T</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">U</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">V</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">W</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">X</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">Y</a>
                    </div>
                    <div id="dp_wrapper_maincon_body_letters_numbers">
                        <a href="#" style="text-decoration:none;">Z</a>
                    </div>
                    <span id="font" style="margin-left: 40px; color:#999; ">Sort By Alphabatically</span>
                </div>
                <!---end of letter box--->
            </div>
            <!---end of type div--->
        </div>
        <!--footer--->
        <div id="wrapper_foot">
            
            <?php foreach($usersData as $userData) { ?>
            <div id="foot_con_main" style="display:inline-block;">
                <a href="<?php echo URL.'users/'.$userData['user_Id'].'/'.$userData['f_name'].'+'.$userData['l_name']; ?>"><div id="foot_con_pic" style=" background-image:url(<?php echo $userData['profile_image']; ?>);">
                    </div></a>
                <div id="foot_con_name">
                    <div class="foot_con_name_tag">
                        <span style="color:#999;">Name</span>
                    </div>
                    <div class="foot_con_name_name">
                        <a href="<?php echo URL.'users/'.$userData['user_Id'].'/'.$userData['f_name'].'+'.$userData['l_name']; ?>" style=" text-decoration: none;"><?php echo $userData['f_name'].' '.$userData['l_name']; ?></a>
                    </div>
                </div>
                <div id="foot_con_address">
                    <div class="foot_con_name_tag">
                        <span style="color:#999;">Join Date</span>
                    </div>
                    <div class="foot_con_name_name">
                        <?php echo $userData['join_date']; ?>
                    </div>
                </div>
                <div id="foot_con_reputation">
                    <div class="foot_con_name_tag">
                        <span style="color:#999;">Reputation</span>
                    </div>
                    <div class="foot_con_name_name">
                        <?php echo $userData['reputation']; ?>
                    </div>
                </div>
                <div id="foot_con_bestfor">
                    <div id="foot_con_bestfor_name">
                        <span style="color:#999;">Best For</span>
                    </div>
                    <div id="foot_con_bestfor_details">
                        <?php foreach($userData['tags'] as $tag) { ?>
                        <a href="#" id="foot_con_bestfor_details_tag"><?php echo $tag['title']; ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            
             
        </div>
        </div>
        <!---end of footer---->
        <!---end of body--->
    </div>
    
    
    
    
    
    
    
    
    
    
    <div id="dp_wrapper_foot">
        <div id="dp_wrapper_foot_left">
            <?php
            //$url = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL);
            //$url = urlencode($url);
            $tab = isset($this->submenu) ? 'tab=' . $this->submenu : '';
            $url = URL . 'users?' . $tab;
            
            if ($pageSetupData['cpage'] != 1) {
                ?>
                <div id="dp_wrapper_foot_left_prve"> <a href="<?php $nPage = $pageSetupData['cpage'] - 1;
            echo $url . '&page=' . $nPage;
                ?>" style="text-decoration:none;">Prev</a> </div>
                                                        <?php
                                                    }
                                                    if ($pageSetupData['cpage'] < 5) {
                                                        $i = 0;
                                                        for ($i = 1; $i < $pageSetupData['allpages']; $i++) {
                                                            if ($i > 5) {
                                                                break;
                                                            }
                                                            ?>
                    <a href="<?php echo $url . '&page=' . $i; ?>" style="text-decoration:none;">
                        <div id="dp_wrapper_foot_left_number" <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo $i ?> </div>
                        </a>
                <?php } ?>
                <?php if (($pageSetupData['allpages'] - $i) >= 1) { ?>
                    <div id="dp_wrapper_foot_left_spece"> ... </div>
    <?php } ?>



            <?php
            }
            if ($pageSetupData['cpage'] >= 5) {
                $i = 0;
                ?>

                    <a href="<?php echo $url . '&page=1'; ?>" style="text-decoration:none;">
                    <div id="dp_wrapper_foot_left_number" <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo 1; ?> </div>
                    </a>
                <div id="dp_wrapper_foot_left_spece"> ... </div>
                <?php
                for ($i = $pageSetupData['cpage'] - 2; $i < $pageSetupData['cpage'] + 3; $i++) {
                    if ($i >= $pageSetupData['allpages']) {
                        break;
                    }
                    ?>
                <a href="<?php echo $url . '&page=' . $i; ?>" style="text-decoration:none;">
                <div id="dp_wrapper_foot_left_number" <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo $i ?> </div>
                </a>
                    <?php
                }
                if (($pageSetupData['allpages'] - $i) >= 1) {
                    ?>
                    <div id="dp_wrapper_foot_left_spece"> ... </div>



    <?php }
}
?>
            <a href="<?php echo $url . '&page=' . $pageSetupData['allpages']; ?>" style="text-decoration:none;">
                <div id="dp_wrapper_foot_left_count"  <?php if ($pageSetupData['cpage'] == $i) { ?>style="background-color: #CCC" <?php } ?>> <?php echo $pageSetupData['allpages']; ?> </div>
            </a>
                                                    <?php if ($pageSetupData['cpage'] != $pageSetupData['allpages']) { ?>
                <div id="dp_wrapper_foot_left_next"> <a href="<?php $nPage = $pageSetupData['cpage'] + 1;
                                                    echo $url . '&page=' . $nPage;
                                                    ?>" style="text-decoration:none;">Next</a> </div>
<?php } ?>
        </div>
        <div id="dp_wrapper_foot_right">
           <a href="<?php echo $url . '&pagesize=5'; ?>" style="text-decoration:none;"> <div id="dp_wrapper_foot_right_counts_one" <?php if (Session::get('pageSize') == 5) { ?>style="background-color: #CCC" <?php } ?>> 5 </div></a>
            <a href="<?php echo $url . '&pagesize=10'; ?>" style="text-decoration:none;"><div id="dp_wrapper_foot_right_counts_two" <?php if (Session::get('pageSize') == 10) { ?>style="background-color: #CCC" <?php } ?>> 10 </div></a>
            <a href="<?php echo $url . '&pagesize=20'; ?>" style="text-decoration:none;"><div id="dp_wrapper_foot_right_counts_three" <?php if (Session::get('pageSize') == 20) { ?>style="background-color: #CCC" <?php } ?>> 20 </div></a>
            <div id="dp_wrapper_foot_left_perpage"> per page</div>
        </div>
    </div>
    
    
    
    
    
    
    
    
</div>