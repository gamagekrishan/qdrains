
<?php $userData = $this->userData[0];?>
<div id="dp_wrapper">
  <div id="dp_wrapper_maincon">
    <div id="dp_wrapper_maincon_head">
      <div id="mainconmenu">
        <ul>
          <li class='active'><span><?php echo $userData['f_name']." ".$userData['l_name']; ?></span></li>
        </ul>
      </div>
    </div>
    <div id="dp_wrapper_maincon_body">
        <form action="" id="uploadimage" method="post" enctype="multipart/form-data">
      <div id="prof_pic_con">
          <div id="prof_pic"><img id="previewing" style="width: 150px;" src="<?php echo $userData['profile_image']; ?>" /> </div>
        <div id="selectImage">
		<label>Select Your Image</label><br/>
                <input type="file" name="profile_image" id="file" />
                <div id="message" style="color: red; margin-top: 5px;">
                
                </div>
            </div>    
      </div>
      <div id="dp_wrapper_maincon_body_form">
          
        <div id="dp_username">
          <div class="dp_text_tag"> <span id="font">First Name</span> </div>
          <div class="dp_text_details">
              <input class="textbox" required="required" name="f_name" type="text" value="<?php echo $userData['f_name']; ?>" id="dp_textboxstyle">
              <div class="error" id="f_name"></div>
          </div>
        </div>
        <div id="dp_email">
          <div class="dp_text_tag"> <span id="font">Last Name</span> </div>
          <div class="dp_text_details">
              <input class="textbox" required="required" name="l_name" type="text" value="<?php echo $userData['l_name']; ?>" id="dp_textboxstyle">
              <div class="error" id="l_name"></div>
          </div>
        </div>
        
        <div id="dp_website">
          <div class="dp_text_tag"> <span id="font">Age</span> </div>
          <div class="dp_text_details">
              <input class="textbox" name="age" type="text" value="<?php echo $userData['age']; ?>" id="dp_textboxstyle">
              <div class="error" id="age"></div>
          </div>
        </div>
        <div id="dp_location">
          <div class="dp_text_tag"> <span id="font">Location</span> </div>
          <div class="dp_text_details">
              <input class="textbox" name="address" type="text" value="<?php echo $userData['address']; ?>" id="dp_textboxstyle">
              <div class="error" id="address"></div>
          </div>
        </div>
        <div id="dp_dob">
          <div class="dp_text_tag"> <span id="font">Gender</span> </div>
          <div class="dp_text_details">
              <select name="gender">
               <option <?php if($userData['gender'] == '') echo 'selected' ?> value=""></option>
           <option <?php if($userData['gender'] == 'm') echo 'selected' ?> value="m">Male</option>
           <option <?php if($userData['gender'] == 'f') echo 'selected' ?> value="f">Female</option>
           </select>
              <div class="error"></div>
          </div>
        </div>
      </div>
      
      <!--- rich text box--->
      <div id="richtextbox">
      <h2>About Me</h2>
      <hr style="border-top: 1px solid #000; margin-top:-20px;">
      <textarea style="margin-top:10px;" name="about"><?php echo html_entity_decode($userData['about']); ?></textarea>
      <div class="error" id="about"></div>
      </div>
      <!---end of rich text box--->
      <div id="save_btncon">
          <input id="btn" type="submit" name="saveprofile" value="Save Profile">
          <a href="<?php echo URL.'users/'.  Session::get('uId').'/'.str_replace(' ', '+', Session::get('uName')); ?>">Cancel</a> </div>
    </form>
    </div>
  </div>
</div>