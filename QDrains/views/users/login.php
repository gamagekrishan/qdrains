<div id="login_wrapper_maincon">
    <div id="login_wrapper_maincon_body">
        <div id="login_wrapper_maincon_body_loginform">
            <div id="login_email">
                <form action="" method="post" id="loginform">
                    <div class="login_text_tag"> <span id="font">Email</span> </div>
                    <div class="login_text_details">
                        <input class="textbox textboxstyle"type="text" id="email" name="email" required="required">
                        <div id="signup_textbol_lable">
                            <span id="emailstatus"></span>
                        </div>
                    </div>
            </div>
            <div id="login_password">
                <div class="login_text_tag"> <span id="font">Password</span> </div>
                <div class="login_text_details">
                    <input class="textbox textboxstyle" type="password" id="pass" name="pass" required="required">
                    <div class="signup_textbol_lable">
                        <span id="loginvalidation"></span>
                    </div>

                </div>
            </div>

            <div id="login_keepme">
                <input type="checkbox" name="keepmelogincheck"/>
                Keep Me Login
            </div>
            <div id="login_forgetpassword">
                <a href="#">Forget Password ?</a>

            </div>
            <div id="login_btncon">
                <input type="submit" id="btn" value="Login"/>

            </div>
            <div id="signup_btncon1" style="margin-top: 40px; margin-right: 160px">
                <div id="signup_btncon_pic1" style="background-image: url(<?php echo URL; ?>public/images/google-plus.png);"> </div>
                <a href="#"><div id="signup_btncon_btn1"> <span>Signin With Google </span> </div></a>
            </div>
            <div id="signup_btncon2" style=" margin-top: 10px; margin-right: 160px;">
                <div id="signup_btncon_pic2" style="background-image: url(<?php echo URL; ?>public/images/facebook-icon.png);"> </div>
                <a href="#"><div id="signup_btncon_btn2"> <span>Signin With Facebook </span> </div></a>
            </div>
            </form>
                <div id="dialog"></div>
        </div>
    </div>
</div>