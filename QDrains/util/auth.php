<?php

class auth
{
    public static function handelLogin()
    {
        @session_start();
        $logged = $_SESSION['loggedIn'];
        if($logged == FALSE )
        {
            session_destroy();
            header('location: '.URL);
            exit;
        }
    }
}
