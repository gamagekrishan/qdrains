-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2015 at 02:15 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `qdrains`
--

-- --------------------------------------------------------

--
-- Table structure for table `answerchangestatus`
--

CREATE TABLE IF NOT EXISTS `answerchangestatus` (
  `ans_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `request_type` enum('close','accept') NOT NULL,
  PRIMARY KEY (`ans_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answerchangestatus`
--

INSERT INTO `answerchangestatus` (`ans_Id`, `user_Id`, `description`, `date`, `request_type`) VALUES
(10003, 100007, 'Answer is accept', '2014-12-14', 'accept'),
(10006, 100007, 'This Answer clsed by the community as not useful answer', '2014-12-14', 'close'),
(10007, 100007, 'This Answer clsed by the community as not useful answer', '2014-12-14', 'close'),
(10008, 100007, 'This Answer clsed by the community as not useful answer', '2014-12-14', 'close'),
(10009, 100007, 'Answer is accept', '2014-12-14', 'accept'),
(10010, 100007, 'Answer is accept', '2014-12-15', 'accept'),
(10012, 100007, 'Answer is accept', '2014-12-21', 'accept');

--
-- Triggers `answerchangestatus`
--
DROP TRIGGER IF EXISTS `after_insert_answerchangestatus`;
DELIMITER //
CREATE TRIGGER `after_insert_answerchangestatus` AFTER INSERT ON `answerchangestatus`
 FOR EACH ROW if(new.request_type = 'close') then
	update answers set status = 'closed' where ans_Id = NEW.ans_Id;
elseif (new.request_type = 'accept') then
	update answers set acceptance = 'yes' where ans_Id = NEW.ans_Id;
end if
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `ans_Id` int(10) NOT NULL AUTO_INCREMENT,
  `content` varchar(1000) NOT NULL,
  `post_date` date NOT NULL,
  `status` enum('active','closed') NOT NULL DEFAULT 'active',
  `user_Id` int(6) NOT NULL,
  `q_Id` int(10) NOT NULL,
  `acceptance` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`ans_Id`),
  KEY `user_Id` (`user_Id`),
  KEY `q_Id` (`q_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10016 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`ans_Id`, `content`, `post_date`, `status`, `user_Id`, `q_Id`, `acceptance`) VALUES
(10001, 'There is no option to disable the ''X'' button. You would need to add css to display none/hide() the element with the class ''ui-icon-closethick'' when it is loaded and opened.', '2014-11-25', 'active', 100007, 38, 'no'),
(10002, 'This code snippet also shows how to set the title and text of the dialog box -- I am using it as a modal notification window and closing it when my AJAX call completes.', '2014-12-03', 'active', 100009, 38, 'no'),
(10003, 'simply you can do this with jquery api', '2014-12-10', 'active', 100007, 39, 'yes'),
(10004, 'you can lasjdf asldjfmkl ajskldjfka lsdnflka lksdjkl fjlaskdnfncsdkjch hakjsdjkl jasdfkn ajksdjkljf cklasjdlcna kldkjkl; asdfl asdklncklaj sdklcnaskldjckasndc;k jaskldckl flsdkl jjklasdfklj aklsdjfkl jakdsjfklaj klsdjfklja sdkljfklj askdjfkl sadjdfkljasdkldjfjaskdjfjaskldjfj\r\n&nbsp;\r\nasdiofj oasdjfkoj', '2014-12-10', 'active', 100007, 40, 'no'),
(10005, 'sdfjajsdkfj asdkjfklja sdkfak jsdklfj aklsjdfklj sdkjflkajdfklj alsdfjlasjdklfj askldjfklaj sdklfj lasdkjflka jlsdkjfklajsdfkljasdklfjaklsdjfl kasdkljfklasdf', '2014-12-10', 'active', 100007, 40, 'no'),
(10006, 'lkjasdkljflkasdkljf klasjdflkjasdf lakdjsfkljasdlkdjf lasjdfkljasdf sdflkasdjfnsdkf sdnflksdnfmkln asdkjfn', '2014-12-13', 'closed', 100007, 39, 'no'),
(10007, 'asdf klasjdklfj klasdlkfjkl jasdkjfl aklsdjfl jaskldjflk jaklsdjfk ljlsdkj', '2014-12-14', 'closed', 100007, 42, 'no'),
(10008, 'asdklflkas djfklj asdkljfl aklsjdljf asdkljfasddklfklsdjflkj asdklfklja lksjdfjasd;kfj alsdjfl jaksdjf&nbsp;', '2014-12-14', 'closed', 100007, 43, 'no'),
(10009, 'asd fkj asdf askldfh asdhfjkhasdkfhkjahsdkjhf kadjfhhkasjdhfjkh asjkhdfjkh asjkhdfkhsjdhfhjkdjjhjhjhjhjhjjd&nbsp;', '2014-12-14', 'active', 100007, 43, 'yes'),
(10010, 'asdjflka jsdkljf askldjfkl k;asdjklfj klasjskldjfkl jasdklfj klasdjklfj ksd', '2014-12-15', 'active', 100007, 46, 'yes'),
(10011, 'ok i gotta answer for this if any one have defferent please post', '2014-12-15', 'active', 100007, 47, 'no'),
(10012, 'asdfi alsdkljf klaklsdjfkl jaklsdjklfj klasdjfkl jaksljdkljf klajsdklfjlk ajkljdskf', '2014-12-21', 'active', 100007, 45, 'yes'),
(10013, 'asdfklasdflk aslkdjfl ajlsdfjl aldsjf asldjfl asldfj lasdfjla jsdljfl kajlsdfj', '2014-12-21', 'active', 100007, 48, 'no'),
(10014, 'asdfa sdf asdf asd', '2014-12-01', 'active', 100013, 49, 'no'),
(10015, 'asdf asd fashdfjh kasdkjfh akshdfkjh akshdkjfh kajsdhjkfhkja shjkdhfkjh asjkdhf', '2014-12-21', 'active', 100007, 48, 'no');

--
-- Triggers `answers`
--
DROP TRIGGER IF EXISTS `after_insert_answers`;
DELIMITER //
CREATE TRIGGER `after_insert_answers` BEFORE INSERT ON `answers`
 FOR EACH ROW begin
set @qStatus = (select answered from questions where qId = new.q_Id);
if @qStatus = 'no' then
	update questions set answered = 'yes' where qId = new.q_Id;
end if;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ans_comments`
--

CREATE TABLE IF NOT EXISTS `ans_comments` (
  `com_Id` int(10) NOT NULL,
  `ans_Id` int(10) NOT NULL,
  PRIMARY KEY (`com_Id`,`ans_Id`),
  KEY `ans_Id` (`ans_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ans_comments`
--

INSERT INTO `ans_comments` (`com_Id`, `ans_Id`) VALUES
(10004, 10001),
(10005, 10001),
(10008, 10001),
(10009, 10001),
(10010, 10001),
(10011, 10001),
(10014, 10001),
(10006, 10002),
(10007, 10002),
(10012, 10002),
(10013, 10002),
(10015, 10002),
(10016, 10002),
(10017, 10002),
(10018, 10002),
(10022, 10003),
(10029, 10003),
(10027, 10004),
(10023, 10005),
(10025, 10005),
(10033, 10009);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `com_Id` int(10) NOT NULL AUTO_INCREMENT,
  `post_date` date NOT NULL,
  `content` varchar(500) NOT NULL,
  `user_id` int(6) NOT NULL,
  PRIMARY KEY (`com_Id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10036 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`com_Id`, `post_date`, `content`, `user_id`) VALUES
(10001, '2014-10-13', 'asdf asdkfh ajksdhfk asdhf jaksdhf jkahsdkf hasdf ', 100007),
(10002, '2014-12-08', 'asdjfh sdjklhf asdjkhfjk ahsdhf kasdjfhasdjkfh asdf asdjkfhjk asdhjkfhk adljsfh askdhf khasdkfhjkashdfjh asdkhf kahsdkfjh ', 100003),
(10004, '2014-10-06', 'asdf adsf kljasdklf jasdfklh asdkhfkl asdkfasddhfjkl hasdjkfh aksdhfk jahsdkdjfh ajksdhfjh asdjfasd', 100012),
(10005, '2014-12-01', 'asd fasdkjfh k;asjdhfkha jsdhfjkhasjk hfjasdjkfhajk sdhfjkhasdkhf ajsdhfkjh akjsdjhfjk ahsdfasf', 100012),
(10006, '2014-10-13', 'asd fjasdhfk hasjkdhf kjahsdfhajkhsruiyawekl bsxfjghdjk fhajksdjkh fasdhfjk ', 100020),
(10007, '2014-10-07', 'asdfn aklsdf asdhf asdjkhfkl sdhf jihasduiyfahsddfjiyhasd gsdf', 100007),
(10008, '2014-12-10', 'asdfasdf asdf asdf asdf adsf asd', 100007),
(10009, '2014-12-10', 'asdfasdf asdf asdf asdf adsf asd', 100007),
(10010, '2014-12-10', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 100007),
(10011, '2014-12-10', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 100007),
(10012, '2014-12-10', 'asdfas dfa sdf asdf sa&nbsp;', 100007),
(10013, '2014-12-10', 'asdfas dfa sdf asdf sa&nbsp;', 100007),
(10014, '2014-12-10', 'asdfa sdf asdf asdf asdf asd&nbsp;', 100007),
(10015, '2014-12-10', 'asdf asdf asdf asdf asdf asdf asdf aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 100007),
(10016, '2014-12-10', 'asdfasdfasdfasdfasd as', 100007),
(10017, '2014-12-10', 'asdfasdfasdfasdfasd as', 100007),
(10018, '2014-12-10', 'asddf asdf asdf asdf asdf asdaaaaaaaaaaaaaaaaaaaaaaaaaaa', 100007),
(10019, '2014-12-10', 'asdfasdfasdfasdf asdfas df&nbsp;', 100007),
(10020, '2014-12-10', 'can you put more code?', 100007),
(10021, '2014-12-10', 'this question is not good', 100007),
(10022, '2014-12-10', 'when prived answer please care about question', 100007),
(10023, '2014-12-10', 'asdf asdf kajsdkf klajsdkfj aksdjfsdf', 100007),
(10024, '2014-12-13', 'kjefwkefwef rkf eufbiwbf frubfur fu rbfr furubeubw', 100007),
(10025, '2014-12-13', 'r44thy5hygyfyyyyyyyyyehtyr', 100007),
(10026, '2014-12-14', 'asdlfj lajskldjfkl jlasdjklf jklsdjfl jaskldjfklajsdkldfjkl ajlksd', 100007),
(10027, '2014-12-14', 'asdklfkljaskldjkf jklajsdklf klajsdkldfj kladskldfj klasd', 100007),
(10028, '2014-12-14', 'sfasdfasdfa adsf asdf asdf asdf asdf', 100007),
(10029, '2014-12-14', 'asdf asd saddf asdf', 100007),
(10030, '2014-12-14', 'asdf asdf s', 100007),
(10031, '2014-12-14', 'sdf adsf asdf asdf sd', 100007),
(10032, '2014-12-14', 'asdf asdf asdf asdf asddf asdf sd', 100007),
(10033, '2014-12-14', 'asdfasdf asdf asdf', 100007),
(10034, '2014-12-15', 'asdfk asdkljf lasdkljfkl asdf&nbsp;\r\n&nbsp;', 100007),
(10035, '2014-12-15', 'please any one can help me............', 100007);

-- --------------------------------------------------------

--
-- Table structure for table `facebookusers`
--

CREATE TABLE IF NOT EXISTS `facebookusers` (
  `user_Id` int(6) NOT NULL,
  `f_Id` varchar(21) NOT NULL,
  PRIMARY KEY (`user_Id`,`f_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `favouritetags`
--

CREATE TABLE IF NOT EXISTS `favouritetags` (
  `tag_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `select_date` date NOT NULL,
  PRIMARY KEY (`tag_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `googleusers`
--

CREATE TABLE IF NOT EXISTS `googleusers` (
  `user_Id` int(6) NOT NULL,
  `g_Id` varchar(30) NOT NULL,
  PRIMARY KEY (`user_Id`,`g_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `linkidentify`
--

CREATE TABLE IF NOT EXISTS `linkidentify` (
  `user_Id` int(6) NOT NULL,
  `link` varchar(64) NOT NULL,
  `validity` enum('true','false') NOT NULL DEFAULT 'true',
  `createdate` datetime NOT NULL,
  PRIMARY KEY (`link`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `linkidentify`
--

INSERT INTO `linkidentify` (`user_Id`, `link`, `validity`, `createdate`) VALUES
(100050, '0273744a916b54493813f9b5e979f5c25b580a2a75a811db279a3861e39e93ee', 'false', '2014-12-08 05:35:19'),
(100049, '0432f8b4759500c564d1b45b89c3e10fa60d10322e96071ba9611a8c7a5e39c0', 'false', '2014-12-08 05:33:06'),
(100053, '2909fb3e39241d5e5662a0edee6983cd9423b7e3778c199c9424351b4da5a6cb', 'false', '2014-12-08 06:44:20'),
(100051, '38e02f421ab19f4874bed44d2a79ea3ca5f52e1ddd23c74f5dbefe2d0e2ae6e8', 'false', '2014-12-08 06:14:29'),
(100056, '5b3e9830de75ba25225aff64e17945b0ec90be72daea4fb9f52e99e94f91d72a', 'true', '2014-12-13 10:27:15'),
(100057, '9fcd021b8378c453210f0341fa60c46037b10c634c774b7b43c1383a8cc28c97', 'true', '2014-12-22 05:26:30'),
(100052, 'b40fb3797e9893831cc0b83b4acd7a5566a8d810832bf12cfec313a4ba307964', 'false', '2014-12-08 06:37:43'),
(100054, 'c8b46983a94f8ee0e3d1b407d47118416686ef23c9a77c293dcb1656ed565387', 'false', '2014-12-08 06:48:08'),
(100055, 'd71cf2437f325cbe6e1f73c9a589c830a17628fa44033d478bc30153423dc09f', 'false', '2014-12-08 07:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `localusers`
--

CREATE TABLE IF NOT EXISTS `localusers` (
  `user_Id` int(6) NOT NULL,
  `password` varchar(64) NOT NULL,
  `verification` enum('true','false') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`user_Id`),
  KEY `user_Id` (`user_Id`,`password`),
  KEY `user_Id_2` (`user_Id`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `localusers`
--

INSERT INTO `localusers` (`user_Id`, `password`, `verification`) VALUES
(100001, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100002, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100003, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100006, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100007, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100008, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100009, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100010, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100011, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100012, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100013, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100014, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100015, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100016, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100017, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100018, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100019, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100020, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100021, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100022, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100023, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100024, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100025, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100026, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100027, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100028, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100029, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100030, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100031, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false'),
(100032, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100033, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100034, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100035, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100036, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100037, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100038, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100039, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100040, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100041, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100042, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100043, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100044, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100045, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100046, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100047, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100048, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100049, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100050, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100051, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100052, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100053, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100054, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100055, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100056, '9840784c2a904c9eca479322a63f072773686c40ddb028ffc8af90a5a8203468', 'false'),
(100057, 'aafb8761e6b9a6a40059eeace43eb777aaa6af0564de9a8c7a1840eb5002d25d', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `loggeddetails`
--

CREATE TABLE IF NOT EXISTS `loggeddetails` (
  `user_Id` int(6) NOT NULL,
  `token` varchar(64) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`user_Id`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loggeddetails`
--

INSERT INTO `loggeddetails` (`user_Id`, `token`, `date`) VALUES
(100007, 'c9e782cd907ea89b97d61e0df3f77d9cec0c03f2ba0d692b088f340783c5b612', '2014-12-13');

-- --------------------------------------------------------

--
-- Table structure for table `questioncomments`
--

CREATE TABLE IF NOT EXISTS `questioncomments` (
  `com_Id` int(10) NOT NULL,
  `q_Id` int(10) NOT NULL,
  PRIMARY KEY (`com_Id`,`q_Id`),
  KEY `q_Id` (`q_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questioncomments`
--

INSERT INTO `questioncomments` (`com_Id`, `q_Id`) VALUES
(10001, 38),
(10002, 38),
(10019, 38),
(10021, 39),
(10030, 39),
(10031, 39),
(10020, 40),
(10024, 40),
(10026, 43),
(10032, 43),
(10028, 45),
(10034, 46),
(10035, 47);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `qId` int(10) NOT NULL AUTO_INCREMENT,
  `ask_date` date NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` varchar(5000) NOT NULL,
  `status` enum('active','closed') NOT NULL DEFAULT 'active',
  `views` int(10) NOT NULL DEFAULT '0',
  `user_Id` int(6) NOT NULL,
  `answered` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`qId`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`qId`, `ask_date`, `title`, `content`, `status`, `views`, `user_Id`, `answered`) VALUES
(38, '2014-12-04', 'How to make jquery-ui-dialog not have a close button', 'I didn&#39;t find this&nbsp;in the documentation.\nShould I just make the close button&nbsp;display:none&nbsp;with css, or is there a clean way in the API to make a dialog without the X button (top-right)?', 'active', 5, 100007, 'yes'),
(39, '2014-08-12', 'Dynamically create text in dialog box', 'How can I dynamically set the text of the dialog box that I&#39;m opening? I&#39;ve tried a few different things but they all respond with an empty dialog box.\nHere is my current try:\n$(&#39;#dialog&#39;).text(&#39;Click on the link to download the file:&#39;.data);\n$(&#39;#dialog&#39;).dialog(&#34;open&#34;);', 'active', 58, 100007, 'yes'),
(40, '2014-12-06', 'Accessing Google Drive from a Firefox extension', 'I&#39;m trying to access (CRUD) Google Drive from a Firefox extension. Extensions are coded in Javascript, but neither of the two existing javascript SDKs seem to fit; the client-side SDK expects &#34;window&#34; to be available, which isn&#39;t the case in extensions, and the server-side SDK seems to rely on Node-specific facilities, as a script that works in node no longer does when I load it in chrome after running it through browserify. Am I stuck using raw REST calls? The Node script that works looks like this:\r\nvar google =require(&#39;googleapis&#39;);var readlineSync =require(&#39;readline-sync&#39;);var CLIENT_ID =&#39;....&#39;,\r\n    CLIENT_SECRET =&#39;....&#39;,\r\n    REDIRECT_URL =&#39;urn:ietf:wg:oauth:2.0:oob&#39;,\r\n    SCOPE =&#39;https://www.googleapis.com/auth/drive.file&#39;;var oauth2Client =new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);var url = oauth2Client.generateAuthUrl({\r\n  access_type:&#39;offline&#39;,// &#39;online&#39; (default) or &#39;offline&#39; (gets refresh_token)\r\n  scope: SCOPE // If you only need one scope you can pass it as string});var code = readlineSync.question(&#39;Auth code? :&#39;);\r\n\r\noauth2Client.getToken(code,function(err, tokens){\r\n  console.log(&#39;authenticated?&#39;);// Now tokens contains an access_token and an optional refresh_token. Save them.if(!err){\r\n    console.log(&#39;authenticated&#39;);\r\n    oauth2Client.setCredentials(tokens);}else{\r\n    console.log(&#39;not authenticated&#39;);}});\r\nI wrap the node GDrive SDK using browserify on this script:\r\nvarGoogle=newfunction(){this.api =require(&#39;googleapis&#39;);this.clientID =&#39;....&#39;;this.clientSecret =&#39;....&#39;;this.redirectURL =&#39;urn:ietf:wg:oauth:2.0:oob&#39;;this.scope =&#39;https://www.googleapis.com/auth/drive.file&#39;;this.client =newthis.api.auth.OAuth2(this.clientID,this.clientSecret,this.redirectURL);}}', 'active', 20, 100022, 'yes'),
(41, '2014-12-13', 'kasjfkl jasdkljfkl asdklfj aksjdl;fkj asdklf ', 'a sdf;jklh alksdflkj asdjfklasdklf klasdklf asdklfkljasdkljf asdklfjklasdj flkjasdlkfj&nbsp;', 'closed', 3, 100007, 'no'),
(42, '2014-12-13', 'aslkdfj laksdklfj asdkljfkl jasdf ', 'asd fasdjklhf asdklfj asdhf ajksdhfjklh askjdfh', 'closed', 3, 100007, 'yes'),
(43, '2014-12-13', 'aslkdfj lakasdfssdklfj asdkljfkl jasdf ', 'asd fasdjklhf asasdfasdfasdddklfj asdhf ajksdhfjklh askjdfh', 'active', 11, 100007, 'yes'),
(44, '2014-12-13', 'alsdjflkjasdkljf asdlfjk laksdjflk jasdkljf ', '&nbsp;asdkfj lkasjdlfj lasdjklf jalsjdklfj laksdjfklj alksjdklfj lasdjklfj klasdjkf asd', 'closed', 27, 100007, 'no'),
(45, '2014-12-13', 'kkkkkkk kkkkkkk llllllll lll', 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', 'active', 13, 100007, 'no'),
(46, '2014-12-15', 'asdf asdkl kjkljklj kldjklfja sdkjdfaf dfasdd', 'fa sdf kaksdjkfh asdjkhfjk hajksdhjk fhksjdf sdf asddf', 'active', 8, 100007, 'yes'),
(47, '2014-12-15', 'asd fasdfj kasjdklfj klaskldjf jasdhjkf hkjasdjkfh jkashjkfh asd', '&nbsp;asdf asdkfhjkasdklfj asjkdhfjkahsddjkfhkasjdhdfkjahs kjdfh asjkdhfjk hasjkhdfjk ahksdjfh kajsdhfjkhasjkdfhjk ahsdjf\r\najjhsd fjkhasdjklfh asjkdhf&nbsp;\r\nad sfjhajksdhfkj jhasd\r\nfasd fasdf&nbsp;', 'active', 6, 100007, 'yes'),
(48, '2014-12-19', 'asdfjjkash dfjhajk hsdjkhfa sdjkhfk ahsdjkhfkjah kjsdhfk hasd', 'a sdfajk hjkdsfjk akdhsfjk hakjsdhfjk hakjdhsfjkh asdjkhf jhadjkshfkja sdjkfhja s', 'active', 9, 100007, 'yes'),
(49, '2014-12-21', 'ggggggggggggggggggggggggggggggggggggggggggggggggg', 'ggggggggggggggggggggggggggggggggggggggggggggggggg', 'active', 9, 100007, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `questionscategories`
--

CREATE TABLE IF NOT EXISTS `questionscategories` (
  `q_Id` int(10) NOT NULL,
  `tag_Id` int(10) NOT NULL,
  PRIMARY KEY (`q_Id`,`tag_Id`),
  KEY `tag_Id` (`tag_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionscategories`
--

INSERT INTO `questionscategories` (`q_Id`, `tag_Id`) VALUES
(41, 10001),
(45, 10001),
(48, 10002),
(38, 10003),
(40, 10003),
(43, 10003),
(44, 10003),
(40, 10004),
(46, 10005),
(47, 10006),
(38, 10007),
(39, 10007),
(40, 10007),
(47, 10010),
(38, 10015),
(39, 10015),
(47, 10020),
(40, 10022),
(42, 10024),
(43, 10024),
(49, 10024),
(39, 10026),
(47, 10026);

--
-- Triggers `questionscategories`
--
DROP TRIGGER IF EXISTS `after_questionscategories_insert`;
DELIMITER //
CREATE TRIGGER `after_questionscategories_insert` AFTER INSERT ON `questionscategories`
 FOR EACH ROW update tags set questions_usage = questions_usage + 1 where tag_Id = NEW.tag_Id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `questionschangestatus`
--

CREATE TABLE IF NOT EXISTS `questionschangestatus` (
  `q_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `request_type` enum('close','hold') NOT NULL,
  PRIMARY KEY (`q_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionschangestatus`
--

INSERT INTO `questionschangestatus` (`q_Id`, `user_Id`, `description`, `date`, `request_type`) VALUES
(41, 100007, 'This question clsed by the community as not useful question', '2014-12-14', 'close'),
(42, 100007, 'This question clsed by the community as not useful question', '2014-12-14', 'close'),
(44, 100007, 'This question clsed by the community as not useful question', '2014-12-14', 'close'),
(45, 100007, 'This question clsed by the community as not useful question', '2014-12-14', 'close');

--
-- Triggers `questionschangestatus`
--
DROP TRIGGER IF EXISTS `after_insert_questionchangestatus`;
DELIMITER //
CREATE TRIGGER `after_insert_questionchangestatus` AFTER INSERT ON `questionschangestatus`
 FOR EACH ROW update questions set `status` = 'closed' where qId = new.q_Id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `reputation`
--

CREATE TABLE IF NOT EXISTS `reputation` (
  `rep_Id` int(10) NOT NULL AUTO_INCREMENT,
  `user_Id` int(6) NOT NULL,
  `date` datetime NOT NULL,
  `event` varchar(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `rep_amount` int(7) NOT NULL,
  PRIMARY KEY (`rep_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `reputation`
--

INSERT INTO `reputation` (`rep_Id`, `user_Id`, `date`, `event`, `description`, `rep_amount`) VALUES
(1, 100007, '2014-12-06 00:00:00', 'vote up', 'voting up a question', 15),
(3, 100007, '2014-12-12 00:00:00', 'vote down', 'voting down a questions', -5),
(4, 100007, '2014-12-12 00:00:00', 'vote down', 'voting down a answer', -5),
(5, 100007, '2014-06-09 00:00:00', 'asdfasd', 'asdfasdfas', 15),
(6, 100007, '2014-06-04 00:00:00', 'asdf', 'asdf', 45),
(8, 100007, '2014-12-13 00:00:00', 'vote down', 'voting down a answer', -5),
(9, 100007, '2014-12-13 00:00:00', 'vote down', 'voting down a questions', -5),
(10, 100007, '2014-12-14 00:00:00', 'vote down', 'voting down a questions', -5),
(11, 100007, '2014-12-14 00:00:00', 'vote down', 'voting down a answer', -5),
(12, 100007, '2014-12-15 13:37:32', 'vote up', 'voting up a question', 10),
(13, 100007, '2014-12-15 13:47:00', 'vote up', 'voting up a question', 15),
(14, 100007, '2015-01-16 00:00:00', 'Founder', 'earlier join with profix', 500);

--
-- Triggers `reputation`
--
DROP TRIGGER IF EXISTS `after_reputation_insert`;
DELIMITER //
CREATE TRIGGER `after_reputation_insert` AFTER INSERT ON `reputation`
 FOR EACH ROW UPDATE users SET reputation = reputation + NEW.rep_amount WHERE user_Id = NEW.user_Id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tagchangestatus`
--

CREATE TABLE IF NOT EXISTS `tagchangestatus` (
  `tag_Id` int(10) NOT NULL,
  `requester_Id` int(6) NOT NULL,
  `req_date` date NOT NULL,
  `description` int(11) NOT NULL,
  PRIMARY KEY (`tag_Id`,`requester_Id`),
  KEY `requester_Id` (`requester_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_Id` int(10) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(500) DEFAULT NULL,
  `questions_usage` int(10) NOT NULL DEFAULT '0',
  `followers` int(10) NOT NULL DEFAULT '0',
  `creater_Id` int(6) NOT NULL,
  `status` enum('active','closed') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`tag_Id`),
  KEY `creater_Id` (`creater_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10076 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_Id`, `create_date`, `title`, `content`, `questions_usage`, `followers`, `creater_Id`, `status`) VALUES
(10001, '2014-12-01', 'java', 'Java is a general-purpose programming language designed to be used in conjunction with the Java Virtual Machine (JVM). "Java platform" is the name for a computing system that has installed tools for developing and running Java programs. Use this tag for questions referring to Java programming language or Java platform tools.', 2, 0, 100007, 'active'),
(10002, '2014-11-11', 'c#', 'C# is a multi-paradigm programming language encompassing strong typing, imperative, declarative, functional, generic, object-oriented (class-based), and component-oriented programming disciplines.', 1, 0, 100007, 'active'),
(10003, '2014-10-06', 'javascript', 'JavaScript (not to be confused with Java) is a dynamically-typed language commonly used for client-side scripting. Use this tag for questions regarding ECMAScript and its various dialects/implementations (excluding ActionScript). Unless a tag for a framework/library is also included, a pure JavaScript answer is expected.', 3, 0, 100007, 'active'),
(10004, '2014-10-21', 'php', 'PHP is a general-purpose scripting language widely used in web development.', 1, 0, 100007, 'active'),
(10005, '2014-11-17', 'android', 'Android is Google''s OS for digital devices [Phone, Tablet, Auto, TV, Watch]. Please use Android-specific tags such as android-intent, not intent. For non-developer/programming questions, use http://android.stackexchange.com', 1, 0, 100007, 'active'),
(10006, '2014-09-15', 'python', 'Python is a dynamic and strongly typed programming language that is designed to emphasize usability. Two similar but incompatible versions of Python are in widespread use (2 and 3). Please consider mentioning the version and implementation that you are using when asking a question about Python.', 1, 0, 100007, 'active'),
(10007, '2014-08-11', 'jquery', 'jQuery is a popular cross-browser JavaScript library that facilitates DOM (HTML Structure) traversal, event handling, animation and AJAX interactions by minimizing the discrepancies across browsers and providing an easy-to-use API.', 1, 0, 100007, 'active'),
(10008, '2014-05-05', 'html', 'HTML (HyperText Markup Language) is the principal markup language used for structuring web pages and formatting content. The most recent revision to the HTML specification is HTML5.', 0, 0, 100007, 'active'),
(10009, '2014-08-11', 'c++', 'C++ is a general-purpose programming language based on C. Use this tag for questions about code compiled with a C++ compiler, regardless of whether the code could be valid in C, C#, Objective-C and other C based programming languages.', 0, 0, 100007, 'active'),
(10010, '2014-09-22', 'ios', 'iOS is Apple''s operating system for mobile devices, such as the iPhone, iPod touch, iPad and Apple TV (2nd generation and up). Much is shared with OS X, but iOS is optimized for touch-based interfaces.', 1, 0, 100001, 'active'),
(10011, '2014-04-14', 'mysql', 'MySQL is an open-source, relational database management system. If your issue relates to MySQLi, use the MySQLi tag instead.', 0, 0, 100007, 'active'),
(10012, '2014-11-16', 'css', 'CSS (Cascading Style Sheets) is a language used to control the visual presentation of HTML and XML documents including (but not limited to) colors, layout, and fonts.', 0, 0, 100007, 'active'),
(10013, '2014-10-13', 'sql', 'Structured Query Language (SQL) is a language for querying databases. Questions should include code examples and table structure, and include a tag for the DBMS implementation (e.g. MySQL, PostgreSQL, Oracle, MS SQL Server) being used. If your question relates to a specific DBMS (uses specific extensions/features), use that DBMS''s tag instead. Answers to questions tagged with SQL should use ANSI SQL as much as possible.', 0, 0, 100001, 'active'),
(10015, '2014-09-08', 'asp.net', 'ASP.NET is a Microsoft web application development framework that allows programmers to build dynamic web sites, web applications and web services.', 2, 0, 100001, 'active'),
(10016, '2014-10-06', 'objective-c', 'This tag should be used only on questions that are about Objective-C features or depend on code in the language. The tags "cocoa" and "cocoa-touch" should be used to ask about Apple''s frameworks or classes. Use the related tags [ios] and [osx] for issues specific to those platforms.', 0, 0, 100003, 'active'),
(10017, '2014-09-07', '.net', 'The .NET framework is a software framework designed mainly for the Microsoft Windows operating system. It includes an implementation of the Base Class Library, Common Language Runtime (commonly referred to as CLR) and Dynamic Language Runtime. It supports many programming languages, including C#, VB.NET, F# and C++/CLI.', 0, 0, 100008, 'active'),
(10018, '2014-09-14', 'iphone', 'DO NOT use this tag unless you are addressing Apple''s iPhone and/or iPod touch specifically. For questions not dependent on hardware, use the tag "iOS". Another tag to consider is "cocoa-touch" (but not "cocoa"). Please refrain from questions regarding the iTunes App Store or about iTunes Connect.', 0, 0, 100012, 'active'),
(10019, '2014-09-15', 'c', 'C is a general-purpose computer programming language used for operating systems, libraries, games and other high performance work and is clearly distinct from C++. It was developed in 1972 by Dennis Ritchie for use with the Unix operating system.', 0, 0, 100011, 'active'),
(10020, '2014-09-15', 'ruby', 'Ruby is a multi-platform open-source dynamic object-oriented interpreted language created by Yukihiro Matsumoto (Matz) in 1993.', 1, 0, 100007, 'active'),
(10021, '2014-09-22', 'array', 'An array is an ordered data structure consisting of a collection of elements (values or variables), each identified by one (single dimensional array, or vector) or multiple indexes.', 0, 0, 100003, 'active'),
(10022, '0000-00-00', 'ajax', 'AJAX (Asynchronous JavaScript and XML) is a technique for creating seamless interactive websites via asynchronous data exchange between client and server. AJAX facilitates communication with the server or partial page updates without a traditional page refresh.', 1, 0, 100006, 'active'),
(10024, '2014-10-13', 'regex', 'Regular expressions (often shortened to "regex") are expressions written in a declarative language used for matching patterns within strings. General reference: http://stackoverflow.com/q/22937618 Remember to include a tag specifying the programming language or tool you are using.', 3, 0, 100012, 'active'),
(10025, '2014-10-13', 'xml', 'Extensible Markup Language (XML) is a flexible structured document format that defines human- and machine-readable encoding rules.', 0, 0, 100007, 'active'),
(10026, '2014-04-22', 'dialog', 'A form (window, box, etc.) presented to a user, usually for the purpose of capturing input or to make a decision.', 1, 0, 100020, 'active'),
(10027, '2014-08-22', 'tinymce', 'TinyMCE is a platform independent web-based JavaScript HTML WYSIWYG editor control released as open source under LGPL by Moxiecode Systems AB. Use this tag for questions regarding the usage of TinyMCE and its integration into CMS and other web based applications.', 0, 0, 100012, 'active'),
(10028, '2014-07-14', 'ubuntu', 'Ubuntu is a free desktop and server operating system based on Debian GNU/Linux. Note that this is for programming questions specific to Ubuntu and http://askubuntu.com is dedicated to answering general Ubuntu questions.', 0, 0, 100011, 'active'),
(10029, '2014-04-07', 'query-builder', 'A query-builder is a set of classes and methods that is able to programmatically build queries.', 0, 0, 100003, 'active'),
(10030, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10031, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10032, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10033, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10034, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10035, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10036, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10037, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10038, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10039, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10040, '0000-00-00', 'krishan', 'aksdjfkla djfklajsldf aldkjf', 0, 0, 100007, 'active'),
(10041, '2015-01-17', 'gamage', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 0, 0, 100007, 'active'),
(10042, '2015-01-17', 'gamage', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 0, 0, 100007, 'active'),
(10043, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10044, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10045, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10046, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10047, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10048, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10049, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10050, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10051, '2015-01-17', 'hhhhhh', 'kkkkkkkkklklklklklklklklklklkl', 0, 0, 100007, 'active'),
(10052, '2015-01-17', 'kaksjdkfj', 'asjdfklalsjdfkjalks faskdfklasjdf', 0, 0, 100007, 'active'),
(10053, '2015-01-17', 'kaksjdkfj', 'asjdfklalsjdfkjalks faskdfklasjdf', 0, 0, 100007, 'active'),
(10054, '2015-01-17', 'nananan', 'llllllllllllllllllllllllllllllllllllllllllll', 0, 0, 100007, 'active'),
(10055, '2015-01-17', 'nananan', 'llllllllllllllllllllllllllllllllllllllllllll', 0, 0, 100007, 'active'),
(10056, '2015-01-17', 'nananan', 'llllllllllllllllllllllllllllllllllllllllllll', 0, 0, 100007, 'active'),
(10057, '2015-01-17', 'nananan', 'llllllllllllllllllllllllllllllllllllllllllll', 0, 0, 100007, 'active'),
(10058, '2015-01-17', 'nananan', 'llllllllllllllllllllllllllllllllllllllllllll', 0, 0, 100007, 'active'),
(10059, '2015-01-17', 'nananan', 'llllllllllllllllllllllllllllllllllllllllllll', 0, 0, 100007, 'active'),
(10060, '2015-01-17', 'nananan', 'llllllllllllllllllllllllllllllllllllllllllll', 0, 0, 100007, 'active'),
(10061, '2015-01-17', 'hhhhhhhhhhhhhhhhh', 'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', 0, 0, 100007, 'active'),
(10062, '2015-01-17', 'hhhhhhhhhhhhhhhhh', 'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', 0, 0, 100007, 'active'),
(10063, '2015-01-17', 'hhhhhhhhhhhhhhhhh', 'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', 0, 0, 100007, 'active'),
(10064, '2015-01-17', 'cccccccccccccc', 'cccccccccccccccc', 0, 0, 100007, 'active'),
(10065, '2015-01-17', 'cccccccccccccc', 'cccccccccccccccc', 0, 0, 100007, 'active'),
(10066, '2015-01-17', 'cccccccccccccc', 'cccccccccccccccc', 0, 0, 100007, 'active'),
(10067, '2015-01-17', 'cccccccccccccc', 'cccccccccccccccc', 0, 0, 100007, 'active'),
(10068, '2015-01-17', 'rrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrr', 0, 0, 100007, 'active'),
(10069, '2015-01-17', 'rrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrr', 0, 0, 100007, 'active'),
(10070, '2015-01-17', 'rrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrr', 0, 0, 100007, 'active'),
(10071, '2015-01-17', 'rrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrr', 0, 0, 100007, 'active'),
(10072, '2015-01-17', 'rrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrr', 0, 0, 100007, 'active'),
(10073, '2015-01-17', 'rrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrr', 0, 0, 100007, 'active'),
(10074, '2015-01-17', 'nimal', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 0, 0, 100007, 'active'),
(10075, '2015-01-17', 'blabla', 'vvvvvvvvvvvvvvvvvvvvvvvvvvvvv', 0, 0, 100007, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_Id` int(6) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(10) NOT NULL,
  `l_name` varchar(20) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `join_date` date NOT NULL,
  `about` varchar(1000) DEFAULT NULL,
  `profile_image` varchar(200) DEFAULT 'http://www.qdrains.com/public/images/unknown.jpg',
  `birth_date` date DEFAULT NULL,
  `reputation` int(10) NOT NULL DEFAULT '1',
  `active` enum('true','false') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`user_Id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100058 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_Id`, `f_name`, `l_name`, `email`, `join_date`, `about`, `profile_image`, `birth_date`, `reputation`, `active`) VALUES
(100001, 'anubhava', 'lachor', 'anubhava@gmail.com', '1985-09-19', 'My Linked-In Profile\r\nOn Twitter: Follow @anubhava\r\nWorks for AOL.\r\nMy Blog', 'http://localhost/QDrains/public/images/profile/dab08478b226280d4a30894c9a7ed719.jpeg', '2013-02-13', 0, 'false'),
(100002, 'VonC', NULL, 'vonc@ymail.com', '2013-11-19', 'tools support around java technologies, including eclipse.\r\ncode quality evaluation, including metrics definitions, and code static tools for different populations.\r\ncode management (Jira, FishEye/Cru', 'http://localhost/QDrains/public/images/profile/7aa22372b695ed2b26052c340f9097eb.jpeg', '1980-11-10', 0, 'false'),
(100003, 'Jon', 'Skeet', 'jon.skeet1@gmail.com', '2010-11-23', 'Author of C# in Depth.\r\nCurrently a software engineer at Google, London.\r\nUsually a Microsoft MVP (C#, 2003-2010, 2011-)', 'http://localhost/QDrains/public/images/profile/6d8ebb117e8d83d74ea95fbdd0f87e13.jpeg', '1984-11-14', 0, 'false'),
(100006, 'Greg', 'Hewgill', 'greghewgill@gmail.com', '2014-01-15', 'Software geek.', 'http://localhost/QDrains/public/images/profile/747ffa5da3538e66840ebc0548b8fd58.jpeg', '1988-11-10', 0, 'false'),
(100007, 'Nate', 'Cook', 'cook@gmail.com', '2011-11-17', NULL, 'http://localhost/QDrains/public/images/profile/PPrvI.jpg', '1983-02-10', 565, 'true'),
(100008, 'Marc', 'Gravell', 'marc123@ymail.com', '2014-11-02', 'C# programmer and MVP, with a keen interest in all things code', 'http://localhost/QDrains/public/images/profile/NJcqr.png', '1987-02-18', 0, 'false'),
(100009, 'Elliott', 'Frisch', 'frisch4@gmail.com', '2012-03-11', 'Just a software developer raising children in the state of Georgia.', 'http://localhost/QDrains/public/images/profile/HAq55.png', '1979-07-14', 0, 'false'),
(100010, 'Quentin', NULL, 'quentin@gmail.com', '2014-08-17', 'I run a consultancy based near London, UK and develop in Perl, Python, JavaScript (on the client and server) and various other languages mostly for the purposes of putting things (such as BBC iPlayer ', 'http://localhost/QDrains/public/images/profile/1d2d3229ed1961d2bd81853242493247.png', '1990-11-10', 0, 'false'),
(100011, 'Gyapti', 'Jain', 'jaingyapti45@gmail.com', '2009-11-03', 'I am an iPhone / Android / Windows phone application developer with keen interest in Java and Objective C', 'http://localhost/QDrains/public/images/profile/Jj4yA.jpg', '2011-01-22', 0, 'false'),
(100012, 'Gerwin', NULL, 'gerwin@ymail.com', '2007-11-10', NULL, 'http://localhost/QDrains/public/images/profile/ZBCmU.jpg', '1985-05-04', 0, 'false'),
(100013, 'Casey', 'Rule', 'rulecasey3452@yahoo.com', '2010-08-10', 'I am a web developer and programmer for First In Math, an online math education program for grades K-8. I studied computer science and music composition at Lehigh University.', 'http://localhost/QDrains/public/images/profile/z4Dnp.jpg', NULL, 0, 'false'),
(100014, 'Turing85', '', 'turing@gmail.com', '2014-11-11', NULL, 'http://localhost/QDrains/public/images/profile/T218f.jpg', '1991-11-18', 0, 'false'),
(100015, '0x0FFF', NULL, 'jafdoo@gmail.com', '2011-11-10', 'Distributed Systems architect with a broad experience in design of enterprise data warehouses as well as purpose-build systems for distributed processing of big data volumes. Broad knowledge in fields of distributed systems design, RDBMS kernel, transaction processing. Experienced in working with Hadoop ecosystem. LinkedIn Profile', 'http://localhost/QDrains/public/images/profile/161a3dcabfc750cafc2c1ac55520ac07.png', '1992-05-13', 0, 'false'),
(100016, 'Abdullah', NULL, 'abdullah@org.com', '2014-02-16', 'I have a very strong passion towards solving programming problems in general. I''ve spent some time exploring the beauty of different programming languages. But I finally decided to take the path of Android development.', 'http://localhost/QDrains/public/images/profile/THIAJ.jpg', '1986-04-28', 0, 'false'),
(100017, 'jonrsharpe', NULL, 'jonrsharpe@gmail.com', '2014-09-18', 'Transport consultant, software developer, guitarist, cyclist & photographer based in Bracknell, UK. These are my opinions; if you don''t like them, I have others.\r\n\r\nShould you be overwhelmed by gratitude, I have an Amazon Wishlist.', 'http://localhost/QDrains/public/images/profile/feZwC.jpg', NULL, 0, 'false'),
(100018, 'rayryeng', NULL, 'rayryeng@ymail.com', '2013-05-14', 'If you like how I answer questions, check out some of my favourite SO users too. Most of the tricks I know have come from them. In no particular order:\r\n\r\nAmro, chappjc, Divakar, Luis Mendo, natan, Shai, Dan, Rody Oldenhuis, thewaywewalk, Daniel, RTL, Dima.', 'http://localhost/QDrains/public/images/profile/1DO3C.jpg', NULL, 0, 'false'),
(100019, 'bheklilr', NULL, 'bheklilr@gmail.com', '2000-11-09', 'I''m a Pythonista and Haskeller who likes to dabble in various technologies. I have a strong math and engineering background from university, but I am now focusing on programming and computer science more. I enjoy learning the type theory behind Hindley-Milner languages, but I also enjoy the flexibility and rapid prototyping of dynamic languages.', 'http://localhost/QDrains/public/images/profile/lcDd2.jpg', NULL, 0, 'false'),
(100020, 'Reed', 'Copsey', 'reedcopsey@ymail.com', '2001-11-12', 'CTO - C Tech Development Corp.\r\n\r\nMicrosoft MVP - Visual C#, Since 2010\r\n\r\nI''ve been programming professionally for about 14 years, most recently with a focus on C++ and C#.', 'http://localhost/QDrains/public/images/profile/87b3a4c585e6fd2ad5308e15e12bdc36 .jpeg', '1979-07-08', 0, 'false'),
(100021, 'Brian', 'Campbell', 'briancampbell', '2011-02-17', 'At 10, I cut my teeth writing text adventure games in Scheme. In high school, I wrote Mandelbrot set programs on a $10 Commodore 64 from a yard sale, an HP 48 calculator, and a Java 1.0 applet. I''ve entered 68k machine code by hand into RAM on on a computer I built on a breadboard, and I''ve implemented an object oriented layer in Scheme for scripting interactive educational multimedia.', 'http://localhost/QDrains/public/images/profile/6aa87e056bd2ee342fbd56eb2110603c.png', NULL, 0, 'false'),
(100022, 'Meder', NULL, 'meder@gmai.com', '2011-01-19', 'I began learning front end web development around 04-05, started my first program around 06 with PHP, am currently dabbling in node.js, PostgreSQL, Haskell, Python, ECMAScript, and SQL.\r\n\r\nIn the future I hope to dabble more in Smalltalk, Erlang, C, Lua, SQLite, and Ruby and wipe away PHP from my memory.\r\n\r\nMy other interests besides programming include Mixed Martial Arts, Anime, Sci-Fi, Movie watching, Basketball, Reading novels.', 'http://localhost/QDrains/public/images/profile/41869b3e0fbebc5e266376257b83b792.jpeg', NULL, 0, 'false'),
(100023, 'Chris', 'Jester-Young', 'chrisjesteryoung123@gmail.com', '2013-03-10', 'All the code snippets I post on Stack Overflow are licensed under CC0, unless otherwise specified.† In short: Free as in free love. Reuse to your heart''s content! :-D\r\n\r\n(This does not in any way contradict the site policy of licensing everything under CC-Wiki; it simply gives users even more freedom. In particular, you are not obliged to link back to SO when you use my code snippets.)', 'http://localhost/QDrains/public/images/profile/729442eea8d8548842a6e0947e333c7b.jpeg', NULL, 0, 'false'),
(100024, 'alksjd ', 'lkaksjd', 'gamage.krishandhanushka@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100025, 'alksjd ', 'lkaksjd', 'gamage.krishanddhanushka@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100026, 'ishara ', 'telakdj ', 'ishara@gamil.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100027, 'krish', 'sakdjf', 'gkdi@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100028, 'krish', 'sakdjf', 'gddkdi@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100029, 'krish', 'sakdjf', 'gdddkdi@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100030, 'krish', 'sakdjf', 'gddghdkdi@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100031, 'krish', 'sakdjf', 'gddgghdkdi@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100032, 'asdfdlaks', 'assdlkljas', 'dlskjf@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100033, 'asdfasdf', 'asdfas', 'dfasdf@gamai.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100034, 'asdf', 'asdf', 'adkf@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100035, 'fasdf', 'asdf', 'dasf@gamil.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100036, 'asdfafgsf', 'sdghsxdfga', 'dfjk@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100037, 'asdfas', 'asdfasd', 'aaaa@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100038, 'asdf', 'asdfas', 'asdfds@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100039, 'kamal', 'kumara', 'kamal@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100040, 'asdfas', 'asdfas', 'kaamal@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100041, 'asdfas', 'sdlfjkl', 'gamage@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'false'),
(100042, 'asdf', 'asdf', 'asg@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100043, 'sadf', 'asgd', 'hhhhhhhhhhh@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100044, 'fsad', 'sdfa', 'sadf@sadf.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100045, 'asdf', 'sadf', 'kkk@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100046, 'asdf', 'asdf', 'lll@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100047, 'sadf', 'asdf', 'gggg@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100048, 'dsdfas', 'asdfasdf', 'tttt@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100049, 'asdf', 'asdf', 'ooo@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100050, 'asdf', 'asdf', 'uuuu@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100051, 'kasun', 'kadsf', 'kasun@gmail.com', '2014-12-08', NULL, 'unknown.jpg', NULL, 0, 'true'),
(100052, 'suram', 'ksdjf', 'suram@gmail.com', '2014-12-08', NULL, 'http://www.qdrains.com/public/images/unknown.jpg', NULL, 0, 'true'),
(100053, 'aasdfasd', 'asfdasdf', 'hhh@gmail.com', '2014-12-08', NULL, 'http://www.qdrains.com/public/images/unknown.jpg', NULL, 0, 'true'),
(100054, 'lakmal', 'sdf', 'lakmal@gmail.com', '2014-12-08', NULL, 'http://www.qdrains.com/public/images/unknown.jpg', NULL, 0, 'true'),
(100055, 'asdf', 'asfd', 'qqqqq@gmail.com', '2014-12-08', NULL, 'http://www.qdrains.com/public/images/unknown.jpg', NULL, 0, 'true'),
(100056, 'krishan', 'gamage', 'gamge.dk@gmail.com', '2014-12-13', NULL, 'http://www.qdrains.com/public/images/unknown.jpg', NULL, 1, 'false'),
(100057, 'krishan', 'dhanushka', 'gamage.kdhanushka@gmail.com', '2014-12-22', NULL, 'http://www.qdrains.com/public/images/unknown.jpg', NULL, 1, 'false');

-- --------------------------------------------------------

--
-- Table structure for table `voteanswers`
--

CREATE TABLE IF NOT EXISTS `voteanswers` (
  `ans_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `date` date NOT NULL,
  `status` enum('up','down') NOT NULL,
  PRIMARY KEY (`ans_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voteanswers`
--

INSERT INTO `voteanswers` (`ans_Id`, `user_Id`, `date`, `status`) VALUES
(10001, 100003, '2014-12-01', 'up'),
(10001, 100007, '2014-12-10', 'down'),
(10001, 100016, '2014-10-13', 'down'),
(10002, 100007, '2014-12-10', 'up'),
(10002, 100012, '2014-12-01', 'up'),
(10003, 100007, '2014-12-10', 'up'),
(10006, 100007, '2014-12-13', 'down'),
(10009, 100007, '2014-12-14', 'down');

--
-- Triggers `voteanswers`
--
DROP TRIGGER IF EXISTS `after_insert_voteanswers`;
DELIMITER //
CREATE TRIGGER `after_insert_voteanswers` BEFORE INSERT ON `voteanswers`
 FOR EACH ROW BEGIN
set @voteAmount = 0;
set @voteDes = '';
set @event = '';

if new.status = 'up' then
	set @voteAmount = 10;
    set @voteDes = 'voting up a answer';
    set @event = 'vote up';
elseif new.status = 'down' then
	set @voteAmount = -5;
    set @voteDes = 'voting down a answer';
    set @event = 'vote down';
end if; set @ansUserId = (select user_Id from answers where ans_Id = new.ans_Id);

Insert into reputation (`user_Id`,`date`,`event`,`description`,`rep_amount`) values (@ansUserId,now(),@event,@voteDes,@voteAmount);
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `votequestions`
--

CREATE TABLE IF NOT EXISTS `votequestions` (
  `q_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `date` date NOT NULL,
  `status` enum('up','down') NOT NULL,
  PRIMARY KEY (`q_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `votequestions`
--

INSERT INTO `votequestions` (`q_Id`, `user_Id`, `date`, `status`) VALUES
(38, 100002, '2014-12-04', 'up'),
(38, 100006, '2014-11-10', 'down'),
(38, 100007, '2014-12-10', 'up'),
(38, 100009, '2014-12-02', 'up'),
(38, 100010, '2014-10-13', 'down'),
(38, 100019, '2014-12-01', 'down'),
(38, 100020, '2014-10-07', 'down'),
(39, 100007, '2014-12-10', 'down'),
(40, 100007, '2014-12-10', 'down'),
(44, 100007, '2014-12-13', 'down'),
(45, 100007, '2014-12-14', 'down'),
(46, 100007, '2014-12-15', 'up'),
(47, 100007, '2014-12-15', 'up');

--
-- Triggers `votequestions`
--
DROP TRIGGER IF EXISTS `after_insert_votequestions`;
DELIMITER //
CREATE TRIGGER `after_insert_votequestions` AFTER INSERT ON `votequestions`
 FOR EACH ROW BEGIN
    set @voteAmount = 0;
    set @voteDes = '';
    set @event = '';
    
    if new.status = 'up' then
        set @voteAmount = 15;
        set @voteDes = 'voting up a question';
        set @event = 'vote up';
    elseif new.status = 'down' then
        set @voteAmount = -5;
        set @voteDes = 'voting down a questions';
        set @event = 'vote down';
    end if; 
    set @quesUserId = (select user_Id from questions where qId = new.q_Id);
    
    Insert into reputation (`user_Id`,`date`,`event`,`description`,`rep_amount`) values (@quesUserId,now(),@event,@voteDes,@voteAmount);
end
//
DELIMITER ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answerchangestatus`
--
ALTER TABLE `answerchangestatus`
  ADD CONSTRAINT `answerchangestatus_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `answerchangestatus_ibfk_2` FOREIGN KEY (`ans_Id`) REFERENCES `answers` (`ans_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE;

--
-- Constraints for table `ans_comments`
--
ALTER TABLE `ans_comments`
  ADD CONSTRAINT `ans_comments_ibfk_1` FOREIGN KEY (`com_Id`) REFERENCES `comments` (`com_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ans_comments_ibfk_2` FOREIGN KEY (`ans_Id`) REFERENCES `answers` (`ans_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `facebookusers`
--
ALTER TABLE `facebookusers`
  ADD CONSTRAINT `facebookusers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favouritetags`
--
ALTER TABLE `favouritetags`
  ADD CONSTRAINT `favouritetags_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `favouritetags_ibfk_2` FOREIGN KEY (`tag_Id`) REFERENCES `tags` (`tag_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `googleusers`
--
ALTER TABLE `googleusers`
  ADD CONSTRAINT `googleusers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `localusers`
--
ALTER TABLE `localusers`
  ADD CONSTRAINT `localusers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `loggeddetails`
--
ALTER TABLE `loggeddetails`
  ADD CONSTRAINT `loggeddetails_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`);

--
-- Constraints for table `questioncomments`
--
ALTER TABLE `questioncomments`
  ADD CONSTRAINT `questioncomments_ibfk_1` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `questioncomments_ibfk_2` FOREIGN KEY (`com_Id`) REFERENCES `comments` (`com_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `questionscategories`
--
ALTER TABLE `questionscategories`
  ADD CONSTRAINT `questionscategories_ibfk_1` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `questionscategories_ibfk_2` FOREIGN KEY (`tag_Id`) REFERENCES `tags` (`tag_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `questionschangestatus`
--
ALTER TABLE `questionschangestatus`
  ADD CONSTRAINT `questionschangestatus_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `questionschangestatus_ibfk_2` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE;

--
-- Constraints for table `reputation`
--
ALTER TABLE `reputation`
  ADD CONSTRAINT `reputation_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `tagchangestatus`
--
ALTER TABLE `tagchangestatus`
  ADD CONSTRAINT `tagchangestatus_ibfk_1` FOREIGN KEY (`tag_Id`) REFERENCES `tags` (`tag_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tagchangestatus_ibfk_2` FOREIGN KEY (`requester_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`creater_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `voteanswers`
--
ALTER TABLE `voteanswers`
  ADD CONSTRAINT `voteanswers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `voteanswers_ibfk_2` FOREIGN KEY (`ans_Id`) REFERENCES `answers` (`ans_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `votequestions`
--
ALTER TABLE `votequestions`
  ADD CONSTRAINT `votequestions_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `votequestions_ibfk_2` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
