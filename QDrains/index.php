<?php



require 'config.php';
require 'util/auth.php';
require 'public/src/Google_Client.php';
require 'public/src/contrib/Google_Oauth2Service.php';

//use as autoloader!
function __autoload ($class)
{
    require LBS.$class.".php";
}

//Load the boostrap!
$bootstrap = new Bootstrap();

/**
 * Optional path setting
 * $boostrap->setControllerPath('c');
 * $boostrap->setModelPath('m');
 * $bootstrap->setDefaultFile('optional.php');
 * $bootstrap->setErrorFile('error.php);
 */
$bootstrap->init();