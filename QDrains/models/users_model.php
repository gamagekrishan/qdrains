<?php

class users_model extends Model {

    function __construct() {
        parent::__construct();
    }

    //check whether the users details login details correct 
    public function loginRun($email, $pass) {

        $data = $this->db->select('SELECT * FROM users as u INNER JOIN localusers as l ON u.user_Id = l.user_Id WHERE u.email = :email AND l.password = :pass', array(":email" => $email, ":pass" => $pass));

        $count = count($data);
        if ($count > 0) {
            if ($data[0]['active'] == 'false') {
                echo json_encode(array('status' => 0, 'active' => 'false'));
            } else {
                if (isset($_POST['keepmelogincheck'])) {
                    $this->keepmelogin($data[0]['user_Id']);
                }

                $this->sessionGenerate($data);
                echo json_encode(array('status' => 1));
            }
            //echo json_encode(array('status' => 1, 'data' => array('email' => $data[0]['email'], 'pass' => 'd')));
        } else {
            echo json_encode(array('status' => 0, 'text' => 'Your email or password is wrong'));
        }
    }

    public function googlelogin() {

        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to QDrains.com');
        $gClient->setClientId(GOOGLE_CLIENT_ID);
        $gClient->setClientSecret(GOOGLE_CLIENT_SECRET);
        $gClient->setRedirectUri(GOOGLE_REDIRECT_URL);
        $gClient->setDeveloperKey(GOOGLE_DEVELOPER_KEY);

        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_GET['code'])) {
            $gClient->authenticate($_GET['code']);
            Session::set('token', $gClient->getAccessToken());
            header('Location: ' . filter_var(GOOGLE_REDIRECT_URL, FILTER_SANITIZE_URL));
            return;
        }
        if (isset($_GET['error'])) {
            header('Location: ' . URL . 'users/login');
        }

        if (isset($_SESSION['token'])) {
            $gClient->setAccessToken(Session::get('token'));
        }

        if ($gClient->getAccessToken()) {
            
            //For logged in user, get details from google using access token
            $user = $google_oauthV2->userinfo->get();
            $count = $this->checkRegisteredUser('google', $user['id']);
            $lcount = $this->checkRegisteredUser('local', $user['email']);
//            echo '<pre>';
//            print_r($user);
//            echo '<pre>';
//            echo 'googelid ='.$count;
//            echo 'localaccount = '.$lcount;die;
            if ($count > 0) {
                $this->sessionGenerate($this->db->select('select * from users as u inner join googleusers as g on u.user_Id = g.user_Id '
                                . 'where g_Id = :gid', array('gid' => $user['id'])));
                Refresh::redirect();
            }else if($lcount == 1){
                $userDetails = $this->mergeAcc($user);
                $this->sessionGenerate($userDetails);
                Refresh::redirect();
            } else {
                $res = $this->auth();
                if ($res['status'] == '1') {
                    Session::set('guser', $user);
                    Session::set('guserauth', $res['data']);
                    return 'new';
                } else if ($res['status'] == 2) {
                    return 2;
                } else {
                    return 0;
                }
            }
        } else {
            //For Guest user, get google login url
            $authUrl = $gClient->createAuthUrl();
        }
        if (isset($authUrl)) { //user is not logged in, show login button
            header('Location: ' . $authUrl);
        }
    }
   
    private function mergeAcc($user){
        $userDetails = $this->db->select('select *  from users where email = :email',  array(
            ':email' => $user['email']
        ));
        $this->db->insert('googleusers',array(
           'user_Id' =>  $userDetails[0]['user_Id'],
            'g_Id' => $user['id']
        ));
        $this->db->update('users',array(
            'gacc' => '1',
            'emailconfirm' => '1'
        ),"user_Id = '".$userDetails[0]['user_Id']."'");
       $count =  $this->db->select('select COUNT(*) as countt from linkidentify where user_Id = :uId',array(
            ':uId' => $userDetails[0]['user_Id']
        ));
       if($count[0]['countt'] == '1'){
           $this->db->update('linkidentify',array(
               'validity' => 'false'
           ),"user_Id = '".$userDetails[0]['user_Id']."'");
       }
        return $userDetails;
    }

    /**
     * if google user not registered, regiseter user 
     */
    public function registerGoogleUser() {

        $user = Session::get('guser');
        $this->db->insert('users', array('f_name' => $user['given_name'],
            'l_name' => $user['family_name'],
            'email' => $user['email'],
            'profile_image' => $user['picture'],
            'join_date' => date("Y-m-d h:i:sa"),
            'active' => 'true',
            'gacc' => '1'
        ));
        $uid = $this->db->lastInsertId();
        $this->db->insert('googleusers', array(
            'user_Id' => $uid,
            'g_Id' => $user['id']
        ));
        $this->updateAuth(Session::get('guserauth'), $uid, $user['given_name'].' '.$user['family_name']);
        unset($_SESSION['guser']);
        unset($_SESSION['guserauth']);
        $this->sessionGenerate($this->db->select('select * from users as u inner join googleusers as g on u.user_Id = g.user_Id '
                        . 'where g_Id = :gid', array('gid' => $user['id'])));
        header('Location: ' . URL);
        exit();
    }

    /**
     * check if the user registered 
     * @param string $type To fire particular if statement
     * @param string $id get the id or email
     * @return int no of rows
     */
    private function checkRegisteredUser($type, $id) {
        if ($type == 'local') {
            $sth = $this->db->prepare('SELECT email FROM users WHERE email = :email and qacc = :q');
            $sth->execute(array(
                ':email' => $id,
                'q' => '1'
            ));
            return $sth->rowCount();
        }
        if ($type == 'google') {
            $sth = $this->db->prepare('SELECT * FROM googleusers WHERE g_Id = :g_Id');
            $sth->execute(array(
                ':g_Id' => $id
            ));
            return $sth->rowCount();
        }
        if ($type == 'facebook') {
            $sth = $this->db->prepare('SELECT * FROM facebookusers WHERE g_Id = :g_Id');
            $sth->execute(array(
                ':g_Id' => $id
            ));
            return $sth->rowCount();
        }
    }

    /**
     * Register new user with Qdrains user account
     */
    public function signupRun() {

        $form = new Form();
        //validate form data
        $form->post('email', FILTER_SANITIZE_EMAIL)
                ->val('filter', FILTER_VALIDATE_EMAIL)
                ->post('f_name', FILTER_SANITIZE_STRING)
                ->val('minlength', 2)
                ->val('maxlength', 20)
                ->post('l_name', FILTER_SANITIZE_STRING)
                ->val('minlength', 2)
                ->val('maxlength', 20)
                ->post('pass', FILTER_SANITIZE_STRING)
                ->val('minlength', 5)
                ->val('maxlength', 10)
                ->val('match', filter_input(INPUT_POST, 'confirmpass', FILTER_SANITIZE_STRING));

        $data = $form->submit();

        if ($data == false) {
            $filterdData = $form->fetch();
            $uData = $this->db->select('SELECT COUNT(*) as countt,user_Id,email,qacc,gacc,emailconfirm FROM users WHERE email = :email', array(
                ':email' => $filterdData['email']
            ));
            $count = $uData[0]['countt'];
           
            if ($count <= 0) {
                $res = $this->auth();
                if ($res['status'] == '1') {
                    //insert validated user details to user table to register new user
                    $this->db->insert('users', array(
                        'f_name' => $filterdData['f_name'],
                        'l_name' => $filterdData['l_name'],
                        'email' => $filterdData['email'],
                        'join_date' => date("Y-m-d h:i:sa")
                    ));
                    $userid = $this->db->lastInsertId();
                    $this->savePass($userid, $filterdData);
                    $this->updateAuth($res['data'], $userid, $filterdData['f_name'] . ' ' . $filterdData['l_name']);
                    Session::set('requestfrom', 'registersuccess');

                    echo json_encode(array('status' => 1));
                } else if ($res['status'] == '2') {
                    return 2;
                } else {

                    return 0;
                }
            } else if ($uData[0]['gacc'] == '1' && $uData[0]['qacc'] == '0' && $uData[0]['emailconfirm'] == '0') {
                
                $this->savePass($uData[0]['user_Id'], $filterdData);
                Session::set('requestfrom', 'mergesuccess');
                echo json_encode(array('status' => 2));
            } else {
                echo json_encode(array('status' => 0, array('text' => 'This email already exist !!!')));
            }
        } else {
            echo json_encode(array('status' => 0, $data));
        }
    }

    private function updateAuth($data, $userId, $name) {
        $this->db->update('inite', array(
            'name' => $name,
            'u_id' => $userId,
            'accept' => '1'
                ), "inv_id = '" . $data['inv_id'] . "'");
        if($data['rep'] >= 500){
        $this->db->insert('reputation', array(
            'date' => date("Y-m-d h:i:sa"),
            'event' => 'Founder',
            'description' => 'Earlier Join with qdrains',
            'rep_amount' => $data['rep'],
            'user_Id' => $userId
        ));
        }
    }

    private function auth() {
        if (isset($_SESSION['inv'])) {

            $inv = Session::get('inv');
            $data = $this->db->select('select COUNT(*) as countt,inv_id,rep,token,accept from inite where token = :token ', array(
                ':token' => $inv
            ));

            if ($data[0]['countt'] == '1') {

                if ($data[0]['accept'] == '0') {
                    return array(
                        'data' => $data[0],
                        'status' => 1
                    );
                } else {
                    return array(
                        'data' => null,
                        'status' => 2
                    );
                }
            } else {
                return array(
                    'data' => null,
                    'status' => 0
                );
            }
        } else {

            return array(
                'data' => null,
                'status' => 0
            );
        }
    }

    private function savePass($userD, $filterdData) {
        //insert user password to localuser table
        $this->db->insert(
                'localusers', array(
            'user_id' => $userD,
            'password' => Hash::create('sha256', $filterdData['pass'], HASH_PASSWORD_KEY)
        ));
        $this->db->update('users', array(
            'qacc' => '1',
            'emailconfirm' => '2'
                ), "user_Id = '" . $userD . "'");
        //generate new confirm link parameter and insert it to database
        $linkPram = Hash::create('sha256', $filterdData['email'] . date("Y-m-d h:i:sa"), HASH_GENERAL_KEY . 'email');
        $this->db->insert(
                'linkidentify', array(
            'user_id' => $userD,
            'link' => $linkPram,
            'createdate' => date("Y-m-d h:i:sa")
        ));
        $userName = $this->db->select('select f_name,l_name from users where user_Id = :uId',array(
            'uId' => $userD
        ));
        $mData = array(
            'to' => $filterdData['email'],
            'link' => URL . "users/loginconfirm/" . $linkPram,
            'f_name' => $userName[0]['f_name'],
            'l_name' => $userName[0]['l_name']
        );
        $this->mail->confirmMail($mData);
    }

    public function loginconfirm($link) {
        $data = $this->db->select("select user_Id,validity from linkidentify where link  = :link", array(':link' => $link));
        if (count($data) > 0) {
            if($data[0]['validity'] == 'true'){
            $this->db->update('linkidentify', array(
                'validity' => 'false'
                    ), "link = '" . $link . "'");
            $this->db->update('users', array(
                'active' => 'true',
                'emailconfirm' => '1'
                    ), "user_Id = '" . $data[0]['user_Id'] . "'");
            Session::set('uid', $data[0]['user_Id']);
            return 'true';
            }else{
                return 'ok';
            }
        } else {
            return 'false';
        }
    }

    public function loguser() {
        $this->sessionGenerate($this->db->select('select * from users where user_Id = :uid', array('uid' => Session::get('uid'))));
        unset($_SESSION['uid']);
        Refresh::redirect();
    }

    /**
     * If user want to keep login, set the cookies
     * @param int $data user id to generete token
     */
    private function keepmelogin($data) {
        try {
            $salt = rand(99, 999999);
            $token = Hash::create('sha256', $data, HASH_TOKEN_KEY . $salt);
            $this->db->insert('loggeddetails', array(
                'user_Id' => $data,
                'token' => $token,
                'date' => date("Y-m-d")
            ));
            if (isset($_COOKIE['token'])) {
                setcookie("token", "", time() - 3600);
            }
            setcookie('token', $token, time() + (86400 * 30), "/");
        } catch (Exception $ex) {
            echo json_encode(array(
                'status' => 0,
                'text' => 'Something is wrong please login again'
            ));
        }
    }

    /**
     * user dont want to keep login destroy the cookies
     */
    public function getUserDetails($userId) {
        $userData = $this->db->select('select * from users where user_Id = :uId', array(
            ':uId' => $userId
        ));
        if (count($userData) > 0) {
            return $userData;
        } else {
            return false;
        }
    }

    public function getReputaionData($user_Id) {
        $repDataArray = array();
        $curentYear = date("Y") . '00';
        $x = 0;
        do {
            $x += 1;
            $monthData = $this->db->select("select sum(rep_amount) as rep from reputation where EXTRACT(YEAR_MONTH FROM `date`) = :year and user_Id = :user_Id", array(
                ':year' => $curentYear + $x,
                ':user_Id' => $user_Id
            ));
            if ($monthData[0]['rep'] == null) {
                $repDataArray[$x] = 0;
            } else {
                $repDataArray[$x] = $monthData[0]['rep'];
            }
        } while ($x < 12);
        return $repDataArray;
    }

    public function getAnsSummaryTabData($uId) {
        $ans['ans'] = $this->db->select('select ans_Id,content from answers where user_Id = :uId order by ans_Id desc limit 3', array(
            ':uId' => $uId
        ));
        foreach ($ans['ans'] as $key => $value) {
            $ans['ans'][$key]['vote'] = $this->getVotes($value['ans_Id'], 'ans');
        }
        $ans['allans'] = count($this->db->select('select ans_Id from answers where user_Id = :uId', array(
                    ':uId' => $uId
        )));
        return $ans;
    }

    public function getQuesSummaryTabData($uId) {
        $ques['ques'] = $this->db->select('select qId,title from questions where user_Id = :uId order by qId desc limit 3', array(
            ':uId' => $uId
        ));
        foreach ($ques['ques'] as $key => $value) {
            $ques['ques'][$key]['vote'] = $this->getVotes($value['qId'], 'ques');
        }
        $ques['allques'] = count($this->db->select('select qId from questions where user_Id = :qId', array(
                    ':qId' => $uId
        )));
        return $ques;
    }

    public function getAnsTabData($uId) {
        $ansData = $this->db->select('select * from answers where user_Id = :uId limit 5', array(
            ':uId' => $uId
        ));
        foreach ($ansData as $key => $value) {
            $ansData[$key]['vote'] = $this->getVotes($value['ans_Id'], 'ans');
        }
        return $ansData;
    }

    public function getQuesTabData($uId) {
        $quesData['ques'] = $this->db->select('select * from questions where user_Id = :uId limit 5', array(
            ':uId' => $uId
        ));
        foreach ($quesData['ques'] as $key => $value) {
            $quesData['ques'][$key]['vote'] = $this->getVotes($value['qId'], 'ques');
            $quesData['ques'][$key]['answers'] = count($this->db->select('select q_Id from answers where q_Id = :qId', array(
                        ':qId' => $value['qId']
            )));
            $quesData['ques'][$key]['tags'] = $this->db->select('select t.title from tags as t inner join questionscategories as q on t.tag_Id = q.tag_Id where q.q_Id = :qId', array(
                ':qId' => $value['qId']
            ));
        }
        $quesData['allques'] = count($this->db->select('select * from questions where user_Id = :uId', array(
                    ':uId' => $uId
        )));
        return $quesData;
    }

    public function getTagTabData($uId) {
        $tagData['tags'] = $this->db->select('select * from tags where creater_Id = :uId limit 5', array(
            ':uId' => $uId
        ));
        foreach ($tagData['tags'] as $key => $value) {
            $tagData['tags'][$key]['usage'] = count($this->db->select('select tag_Id from questionscategories where tag_Id = :tId', array(
                        ':tId' => $value['tag_Id']
            )));
        }
        $tagData['alltags'] = count($this->db->select('select tag_Id from tags where creater_Id = :uId', array(
                    ':uId' => $uId
        )));
        return $tagData;
    }

    public function getRepTabData($uId) {
        $repData = $this->db->select('select DATE(date) as date,TIME(date) as time,event,description,rep_amount from reputation where user_Id = :uId order by date desc limit 10', array(
            ':uId' => $uId
        ));
        $date = null;
        $repAData = array();
        foreach ($repData as $value) {
            if ($date == $value['date']) {
                array_push($repAData[$value['date']], $value);
            } else {
                $date = $value['date'];
                $repAData[$value['date']][0] = $value;
            }
        }
        return $repAData;
    }

    public function getVotes($id, $type) {
        $table = null;
        $column = null;
        if ($type == 'ques') {
            $table = 'votequestions';
            $column = 'q_Id';
        }
        if ($type == 'ans') {
            $table = 'voteanswers';
            $column = 'ans_Id';
        }
        $up = count($this->db->select("select user_Id from " . $table . " where " . $column . " = :q_Id and status = :val", array(
                    ':q_Id' => $id,
                    ':val' => 'up'
        )));
        $down = count($this->db->select("select user_Id from " . $table . " where " . $column . " = :q_Id and status = :val", array(
                    ':q_Id' => $id,
                    ':val' => 'down'
        )));
        return $up - $down;
    }

    public function getUsersData($type) {

        $rCount = $this->db->select("select COUNT(*) as countt from users where active = 'true' order by " . $type . " desc");

        $pageSetupData = $this->pageSetup($rCount[0]['countt']);
        if ($pageSetupData['pageCond']) {

            $data = $this->db->select("select * from users where active = 'true' order by " . $type . " desc limit " . Session::get('pageSize') . ' OFFSET ' . $pageSetupData['offset'] . '');

            foreach ($data as $key => $val) {
                $data[$key]['tags'] = $this->getTagUsage($val['user_Id']);
            }
            return array(
                'data' => $data,
                'pagesetupdata' => array(
                    'cpage' => $pageSetupData['page'],
                    'allpages' => $pageSetupData['pagecount']
            ));
        } else {
            return false;
        }
    }

    public function getTagUsage($userId) {
        $userTagUsage = $this->db->select('SELECT t.title, COUNT(*) AS count FROM questions as q inner join questionscategories as qc on q.qId = qc.q_Id inner join tags as t on qc.tag_Id = t.tag_Id where q.user_Id = :uId and t.status = :status GROUP BY qc.tag_Id order by count desc limit 3', array(
            ':uId' => $userId,
            ':status' => 'active'
        ));
        return $userTagUsage;
    }

    public function getPageData() {
        $data['alluser'] = $this->getAllUsersCount();
        $data['quescount'] = $this->getAllQuestionsCount();
        $data['unans'] = $this->getUnansweredCount();
        $data['users'] = $this->getTopUsers();
        return $data;
    }

    public function search() {
        if (isset($_POST['uname'])) {
            $uName = filter_input(INPUT_POST, 'uname', FILTER_SANITIZE_STRING);
            $usersData = $this->db->select("select * from users where CONCAT(f_name, ' ', l_name) like '%" . $uName . "%' and active = :active limit 10", array(
                ':active' => 'true'
            ));
            foreach ($usersData as $key => $val) {
                $usersData[$key]['tags'] = $this->getTagUsage($val['user_Id']);
            }
            if (!empty($usersData)) {
                foreach ($usersData as $userData) {
                    ?>
                    <div id="foot_con_main" style="display:inline-block;">
                        <div id="foot_con_pic" style=" background-image:url(<?php echo $userData['profile_image']; ?>);">
                        </div>
                        <div id="foot_con_name">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Name</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php echo $userData['f_name'] . ' ' . $userData['l_name']; ?>
                            </div>
                        </div>
                        <div id="foot_con_address">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Join Date</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php echo $userData['join_date']; ?>
                            </div>
                        </div>
                        <div id="foot_con_reputation">
                            <div class="foot_con_name_tag">
                                <span style="color:#999;">Reputation</span>
                            </div>
                            <div class="foot_con_name_name">
                                <?php echo $userData['reputation']; ?>
                            </div>
                        </div>
                        <div id="foot_con_bestfor">
                            <div id="foot_con_bestfor_name">
                                <span style="color:#999;">Best For</span>
                            </div>
                            <div id="foot_con_bestfor_details">
                                <?php foreach ($userData['tags'] as $tag) { ?>
                                    <a href="" id="foot_con_bestfor_details_tag"><?php echo $tag['title']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo '<h2><span style="margin-left:100px; color:#ff9933">No users matched your search.</span></h2>';
            }
        }
    }

    public function edit() {
        $data = $this->db->select('select f_name,l_name,email,about,profile_image,age,gender,address from users where user_Id = :uId', array(
            ':uId' => Session::get('uId')
        ));
        return $data;
    }

    public function saveprofile() {
//        echo '<pre>';
//        print_r($_POST);
//        print_r($_FILES);
//        echo '<pre>';
        $form = new Form();
        //validate form data
        $form->post('f_name', FILTER_SANITIZE_STRING)
                ->val('minlength', 2)
                ->val('maxlength', 15)
                ->post('l_name', FILTER_SANITIZE_STRING)
                ->val('minlength', 2)
                ->val('maxlength', 15)
                ->post('address', FILTER_SANITIZE_STRING)
                ->val('maxlength', 30)
                ->post('gender', FILTER_SANITIZE_STRING)
                ->post('age', FILTER_SANITIZE_NUMBER_INT)
                ->val('maxlength', 2)
                ->post('profile_image', 'file')
                ->val('file')
                ->post('about')
                ->val('maxlength', 5000);

        $data = $form->submit();
        if ($data == false) {
            $fieldData = $form->fetch();
            $this->saveProfileDetails($fieldData);
            echo json_encode(array(
                'status' => 1,
                'url' => Session::get('uId') . '/' . $fieldData['f_name'] . '+' . $fieldData['l_name']
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'fiedlerror' => $data
            ));
        }
    }

    private function saveProfileDetails($data) {
        $imagePath = '';
        $newData = array(
            'f_name' => $data['f_name'],
            'l_name' => $data['l_name']
        );
        if ($data['age'] != '') {
            $newData['age'] = $data['age'];
        }
        if ($data['address'] != '') {
            $newData['address'] = $data['address'];
        }
        if ($data['gender'] != '') {
            $newData['gender'] = $data['gender'];
        }
        if ($data['about'] != '') {
            $newData['about'] = htmlentities($data['about']);
        }
        $file = $data['profile_image'];
        if (file_exists($file['tmp_name'])) {
            $imageName = Hash::create('sha256', Session::get('uId'), HASH_GENERAL_KEY);
            $imageType = exif_imagetype($file['tmp_name']);
            if ($imageType == 1) {
                $imageType = '.gif';
            }
            if ($imageType == 2) {
                $imageType = '.jpg';
            }
            if ($imageType == 3) {
                $imageType = '.png';
            }
            $imagePath = 'public/images/profile/' . $imageName . $imageType;
            $newData['profile_image'] = URL . $imagePath;
        }
        move_uploaded_file($file['tmp_name'], $imagePath);
        $this->db->update('users', $newData, 'user_Id = ' . Session::get('uId'));
        $this->sessionUpdate();
    }

}
