<?php
require '../lbs/Database.php';
if(isset($_POST['tname'])){
    
    $name = filter_input(INPUT_POST, "tname", FILTER_SANITIZE_STRING);
    $db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
    $tagData = $db->select("select * from tags where status = :status and title like '".$name."%' order by questions_usage desc limit 10",array(
           ':status' => 'active' 
        ));
      foreach($tagData as $tag) { ?>
            <div id="dp_wrapper_maincon_body_tags_con">
                <a href="#" id="dp_wrapper_maincon_body_tags_block_inherit">
                    <div id="dp_wrapper_maincon_body_tags_block">
                        <div id="dp_wrapper_maincon_body_tags_block_tagname">
                            <span id="font"><?php echo $tag['title']; ?></span>
                        </div>
                        <div id="dp_wrapper_maincon_body_tags_block_discription">
                            <span id="font"><?php echo substr($tag['content'], 0, 120).'...'; ?></span>
                        </div>
                        <div id="dp_wrapper_maincon_body_tags_block_useageandrank">
                            <div id="dp_wrapper_maincon_body_tags_block_useage">
                                <div id="dp_wrapper_maincon_body_tags_block_useage_name">
                                    <span id="font">Followers:</span>
                                </div>
                                <div id="dp_wrapper_maincon_body_tags_block_useage_count">
                                    548
                                </div>
                            </div>
                            <div id="dp_wrapper_maincon_body_tags_block_rank">
                                <div id="dp_wrapper_maincon_body_tags_block_rank_name">
                                    <span id="font">Questions:</span>
                                </div>
                                <div id="dp_wrapper_maincon_body_tags_block_rank_count">
                                    <?php echo $tag['questions_usage']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </a>
            <?php } 
}
?>