<?php

class help_model extends Model {

    function __construct() {
        parent::__construct();
        //echo 'this is help_model<br>';
        
    }
    
    public function cuSubmit(){
        $form = new Form();
        //validate form data
        $form->post('email', FILTER_SANITIZE_EMAIL)
                ->val('filter', FILTER_VALIDATE_EMAIL)
                ->post('subject', FILTER_SANITIZE_STRING)
                ->val('minlength', 5)
                ->val('maxlength', 100)
                ->post('content')
                ->val('minlength', 10)
                ->val('maxlength', 1000) ;

        $data = $form->submit();

        if ($data == false) {
            $filterdData = $form->fetch();
            $this->db->insert('clienissuses',array(
               'email' => $filterdData['email'],
                'subject' => $filterdData['subject'],
                'content' => htmlentities($filterdData['content'])
            ));
             echo json_encode(array(
                'status' => 1
            ));
        }else{
            echo json_encode(array(
                'status' => 0,
                'data' => $data
            ));
        }
    }
}