<?php

class questions_model extends Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * get the tags and return to ask page ajax function
     */
    public function getTags() {
        //echo json_encode(array("name","gamage"));
        if (isset($_POST['sval'])) {
            $sVal = filter_input(INPUT_POST, 'sval', FILTER_SANITIZE_STRING);
            $res = $this->db->select("select title from tags where title like '%" . $sVal . "%' LIMIT 5", array(), PDO::FETCH_COLUMN);
            $count = count($res);
            $fdata = array();
            if ($count > 0) {
                foreach ($res as $key => $value) {
                    array_push($fdata, $value);
                }
                echo json_encode($fdata);
            }
        }
    }

    public function getQuestionsData($ques) {
        foreach ($ques as $key => $q) {
            $ques[$key]['votes'] = $this->getVotes($q['qId'], 'ques');
            $ques[$key]['ans'] = count($this->db->select('select q_Id from answers where q_Id = :qId and status = :status', array(
                        ':qId' => $q['qId'],
                        ':status' => 'active'
            )));
            $ques[$key]['tags'] = $this->getTagsById($q['qId']);
            $ques[$key]['user'] = $this->getUserDetais($q['user_Id']);
        }
        return $ques;
    }

    public function getUnanswerdQuestionsData() {
        $rCount = $this->db->select('select COUNT(*) as countt from questions where status = :status and answered = :ans order by ask_date desc ', array(
            ':status' => 'active',
            ':ans' => 'no'
        ));
        $pageSetupData = $this->pageSetup($rCount[0]['countt']);
        if ($pageSetupData['pageCond']) {
            $ques = $this->getQuestionsData($this->db->select('select * from questions where status = :status and answered = :ans order by ask_date desc limit ' . Session::get('pageSize') . ' OFFSET ' . $pageSetupData['offset'] . '', array(
                        ':status' => 'active',
                        ':ans' => 'no'
            )));

            return array(
                'data' => $ques,
                'pagesetupdata' => array(
                    'cpage' => $pageSetupData['page'],
                    'allpages' => $pageSetupData['pagecount']
                )
            );
        } else {
            return false;
        }
    }

    public function getNewstQuestionsData() {
        $rCount = $this->db->select('select COUNT(*) as countt from questions where status = :status order by ask_date desc ', array(
            ':status' => 'active'
        ));
        $pageSetupData = $this->pageSetup($rCount[0]['countt']);
        if ($pageSetupData['pageCond']) {
            $ques = $this->getQuestionsData($this->db->select('select * from questions where status = :status order by ask_date desc limit ' . Session::get('pageSize') . ' OFFSET ' . $pageSetupData['offset'] . '', array(
                        ':status' => 'active'
            )));
            return array(
                'data' => $ques,
                'pagesetupdata' => array(
                    'cpage' => $pageSetupData['page'],
                    'allpages' => $pageSetupData['pagecount']
                )
            );
        } else {
            return false;
        }
    }

    /**
     * Post function - validate question data and check if the question meet QDrains conditions
     */
    public function postQuestions() {
        $form = new Form();

        //validate form data
        $form->post('askqtitle', FILTER_SANITIZE_STRING)
                ->val('minlength', 10)
                ->val('maxlength', 200)
                ->post('asktextarea')
                ->val('minlength', 20)
                ->val('maxlength', 5000);

        $data = $form->submit();

        $filterdData = $form->fetch();

        //check the tags are valied with exist tags
        $currentTags = $this->getFormTags();

        //check if the tags are exist
        $tagIds = $this->tagValidator($currentTags);
        if ($tagIds && $data == false) {
            $res = $this->saveQuestions($filterdData, $tagIds);
            if ($res != false) {
                echo json_encode(array('status' => 1, 'text' => 'Question Post Success ..', 'qid' => $res));
            } else {
                echo json_encode(array('status' => 0, 'text' => 'Sorry we cant reach your request now please try again while'));
            }
        } else {
            if ($data != false && !$tagIds) {
                echo json_encode(array('status' => 0, $data, array('tag' => 'Invalid tag(s)')));
            } elseif (!$tagIds) {
                echo json_encode(array('status' => 0, array('g' => 'k'), array('tag' => 'Invalid tag')));
            } else {
                echo json_encode(array('status' => 0, $data, array('k' => 'd')));
            }
        }
    }

    /**
     * get the tags of form
     * @return array - array content tags
     */
    private function getFormTags() {
        $currentTags = array();
        array_push($currentTags, filter_input(INPUT_POST, 'tag1', FILTER_SANITIZE_STRING));
        if ($_POST['tag2'] != '') {
            array_push($currentTags, filter_input(INPUT_POST, 'tag2', FILTER_SANITIZE_STRING));
        }
        if ($_POST['tag3'] != '') {
            array_push($currentTags, filter_input(INPUT_POST, 'tag3', FILTER_SANITIZE_STRING));
        }
        if ($_POST['tag4'] != '') {
            array_push($currentTags, filter_input(INPUT_POST, 'tag4', FILTER_SANITIZE_STRING));
        }
        return $currentTags;
    }

    private function tagValidator($tags) {
        $flag = true;
        $tagIds = array();

        if (count(array_unique($tags)) < count($tags)) {
            $flag = false;
        }

        foreach ($tags as $value) {
            if ($flag == true) {
                $data = $this->db->select('select tag_Id from tags where title = :title', array(':title' => $value));
                $count = count($data);
                if ($count > 0) {
                    array_push($tagIds, $data[0]['tag_Id']);
                } else {
                    $flag = false;
                }
            } else {
                break;
            }
        }
        if ($flag == TRUE) {
            return $tagIds;
        } else {
            return false;
        }
    }

    /**
     * save quetion
     * @param string $qData - question title and content
     * @param int $tagData - tag ids
     * @return boolean - if the questions save successfuly return true
     */
    private function saveQuestions($qData, $tagData) {
        $user = Session::get('uId');
        try {
            $this->db->insert('questions', array(
                'ask_date' => date("Y-m-d"),
                'title' => $qData['askqtitle'],
                'content' => htmlentities($qData['asktextarea']),
                'user_Id' => $user
            ));
            $qid = $this->db->lastInsertId();
            foreach ($tagData as $value) {
                $this->db->insert('questionscategories', array(
                    'q_Id' => $qid,
                    'tag_Id' => $value
                ));
            }
            return $qid;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function getQuestions($qId) {
        $res = $this->getQuestionsById($qid);
        if ($res == false) {
            return false;
        } else {
            
        }
    }

    public function getQuestionsById($qid) {
        $res = $this->db->select("select * from questions where qId = :qId and status = :val", array(
            ':qId' => $qid,
            ':val' => 'active'
        ));
        $count = count($res);
        if ($count > 0) {
            $this->db->update('questions', array(
                'views' => $res[0]['views'] + 1
                    ), "qId = " . $qid);
            $res[0]['votes'] = $this->getVotes($qid, 'ques');
            $res[0]['tags'] = $this->getTagsById($qid);
            $res[0]['user'] = $this->getUserDetais($res[0]['user_Id']);
            $res[0]['comments'] = $this->getComments($qid, 'ques');
            $res[0]['ans'] = $this->getAns($qid);

            return $res;
        } else {
            return false;
        }
    }

    public function getAns($quesId) {
        $res = $this->db->select('select a.content, a.ans_Id, a.post_date, a.user_Id, a.acceptance, u.l_name, u.profile_image, u.reputation from answers as a inner join users as u on a.user_Id = u.user_Id where a.q_Id = :q_Id and status = :val', array(
            ':q_Id' => $quesId,
            ':val' => 'active'
        ));
        $byVote = array();
        foreach ($res as $key => $ans) {
            $res[$key]['votes'] = $this->getVotes($ans['ans_Id'], 'ans');
            $res[$key]['comments'] = $this->getComments($ans['ans_Id'], 'ans');
        }
        foreach ($res as $key => $ans) {
            $byVote[$key] = $ans['votes'];
        }
        array_multisort($byVote, SORT_DESC, $res);
        return $res;
    }

    public function getComments($id, $type) {
        if ($type == 'ques') {
            return $this->db->select('select c.content, c.post_date, c.user_id, u.l_name from comments as c inner join questioncomments as q on c.com_Id = q.com_Id inner join users as u on c.user_id = u.user_Id where q.q_Id = :q_Id', array(
                        ':q_Id' => $id
            ));
        }
        if ($type == 'ans') {
            return $this->db->select('select c.content, c.post_date, c.user_id, u.l_name from comments as c inner join ans_comments as a on c.com_Id = a.com_Id inner join users as u on c.user_id = u.user_Id where a.ans_Id = :ans', array(
                        ':ans' => $id
            ));
        }
    }

    public function getVotes($id, $type) {
        $table = null;
        $column = null;
        if ($type == 'ques') {
            $table = 'votequestions';
            $column = 'q_Id';
        }
        if ($type == 'ans') {
            $table = 'voteanswers';
            $column = 'ans_Id';
        }
        $up = count($this->db->select("select user_Id from " . $table . " where " . $column . " = :q_Id and status = :val", array(
                    ':q_Id' => $id,
                    ':val' => 'up'
        )));
        $down = count($this->db->select("select user_Id from " . $table . " where " . $column . " = :q_Id and status = :val", array(
                    ':q_Id' => $id,
                    ':val' => 'down'
        )));
        return $up - $down;
    }

    public function getTagsById($qid) {
        return $this->db->select('select t.title from tags as t inner join questionscategories as q on t.tag_Id = q.tag_Id where q.q_Id = :q_Id', array(
                    ':q_Id' => $qid
                        ), PDO::FETCH_COLUMN);
    }

    public function getUserDetais($uId) {
        return $this->db->select('select l_name, profile_image,reputation from users where user_Id = :uid', array(
                    ':uid' => $uId
        ));
    }

    public function postAnsComment() {


        $form = new Form();

        //validate form data
        $form->post('ansid', FILTER_VALIDATE_INT)
                ->post('commentcontent', FILTER_SANITIZE_STRING)
                ->val('minlength', 10)
                ->val('maxlength', 200);

        $data = $form->submit();

        $filterdData = $form->fetch();

        if ($data == false) {

            $this->db->insert('comments', array(
                'post_date' => date('y-m-d'),
                'content' => $filterdData['commentcontent'],
                'user_id' => Session::get('uId')
            ));
            $comid = $this->db->lastInsertId();
            $this->db->insert('ans_comments', array('com_Id' => $comid, 'ans_Id' => $filterdData['ansid']));
            echo json_encode(array('status' => 1));
        } else {
            echo json_encode(array('status' => 0, 'ansId' => $filterdData['ansid'], 'content' => 'Length of post content must be between 10 and 200.'));
        }
    }

    public function postQuesComment() {
        $quesId = filter_input(INPUT_POST, 'quesid', FILTER_VALIDATE_INT);
        $content = filter_input(INPUT_POST, 'quescomcontent', FILTER_SANITIZE_STRING);
        if (strlen($content) < 200 && strlen($content) > 10) {
            $this->db->insert('comments', array(
                'post_date' => date('y-m-d'),
                'content' => $content,
                'user_id' => Session::get('uId')
            ));
            $comid = $this->db->lastInsertId();
            $this->db->insert('questioncomments', array('com_Id' => $comid, 'q_Id' => $quesId));
            echo json_encode(array('status' => 1));
        } else {
            echo json_encode(array('status' => 0, 'content' => 'Length of post content must be between 10 and 200.'));
        }
    }

    public function postAnswer() {
        $quesId = filter_input(INPUT_POST, 'quesid', FILTER_VALIDATE_INT);
        $content = filter_input(INPUT_POST, 'answercontent');
        if (strlen($content) < 10000 && strlen($content) > 20) {
            $this->db->insert('answers', array(
                'post_date' => date('y-m-d'),
                'content' => htmlentities($content),
                'user_id' => Session::get('uId'),
                'q_Id' => $quesId
            ));
            echo json_encode(array('status' => 1));
        } else {
            echo json_encode(array('status' => 0, 'content' => 'Answer not meet our quality standard.'));
        }
    }

    public function quesvote() {
        $qid = filter_input(INPUT_POST, 'qid', FILTER_SANITIZE_NUMBER_INT);
        $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
        $res = $this->validateQVote($qid, Session::get('uId'),$type);
        if($res['status'] == 1) {
        $this->db->insert('votequestions', array(
            'q_Id' => $qid,
            'user_Id' => $_SESSION['uId'],
            'date' => date('y-m-d'),
            'status' => $type
        ));
        echo json_encode(array('status' => 1));
        } else {
            echo json_encode(array('status' => 0, 'msg' => $res['error']));
        }
    }
    
    private function validateQVote($qId,$rId,$type){
        $repAmount = 150;
        if($type == 'up'){
            $repAmount = 50;
        }
        if($type == 'down'){
            $repAmount = 150;
        }
        $this->sessionUpdate();
        if (Session::get('rep') > $repAmount) {
            $qData = $this->db->select('select user_Id from questions where qId = :qid', array(
                ':qid' => $qId
            ));
            if ($qData[0]['user_Id'] == $rId) {
                return array(
                    'status' => 0,
                    'error' => 'can not vote own question'
                );
            } else {
                $voteData = $this->db->select('select COUNT(*) as countt from votequestions where q_Id = :qid and user_Id = :userid', array(
                    ':qid' => $qId,
                    ':userid' => $rId
                ));
                if ($voteData[0]['countt'] > 0) {
                    return array(
                        'status' => 0,
                        'error' => 'already voted for this question'
                    );
                } else {
                    return array(
                        'status' => 1
                    );
                }
            }
        } else {
            return array(
                    'status' => 0,
                    'error' => 'Vote '.$type.' requires '.$repAmount.' reputation'
                );
        }
    }

    public function ansvote() {
        $ansid = filter_input(INPUT_POST, 'ansid', FILTER_SANITIZE_NUMBER_INT);
        $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
        $res = $this->validateAVote($ansid, Session::get('uId'),$type);
        if ($res['status'] == 1) {
            $this->db->insert('voteanswers', array(
                'ans_Id' => $ansid,
                'user_Id' => $_SESSION['uId'],
                'date' => date('y-m-d'),
                'status' => $type
            ));
            echo json_encode(array('status' => 1));
        } else {
            echo json_encode(array('status' => 0, 'msg' => $res['error']));
        }
    }

    private function validateAVote($ansId, $rId,$type) {
        $repAmount = 150;
        if($type == 'up'){
            $repAmount = 50;
        }
        if($type == 'down'){
            $repAmount = 150;
        }
        $this->sessionUpdate();
        if (Session::get('rep') > $repAmount) {
            $ansData = $this->db->select('select user_Id from answers where ans_Id = :ansid', array(
                ':ansid' => $ansId
            ));
            if ($ansData[0]['user_Id'] == $rId) {
                return array(
                    'status' => 0,
                    'error' => 'can not vote own answer'
                );
            } else {
                $voteData = $this->db->select('select COUNT(*) as countt from voteanswers where ans_Id = :ansid and user_Id = :userid', array(
                    ':ansid' => $ansId,
                    ':userid' => $rId
                ));
                if ($voteData[0]['countt'] > 0) {
                    return array(
                        'status' => 0,
                        'error' => 'already voted for this answer'
                    );
                } else {
                    return array(
                        'status' => 1
                    );
                }
            }
        } else {
            return array(
                    'status' => 0,
                    'error' => 'Vote '.$type.' requires '.$repAmount.' reputation'
                );
        }
    }

    /**
     * This for close a question or answer
     * @param string $table table name that data gonna insert
     * @param stirng  $column column name that data gonna enter
     * @param int $des question or answer id
     */
    public function statusChangeRequest($table, $column, $des, $rType) {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $this->db->insert($table, array(
            $column => $id,
            "user_Id" => Session::get('uId'),
            'description' => $des,
            'date' => date('y-m-d'),
            'request_type' => $rType
        ));
        echo json_encode(array('status' => 1));
    }

    public function getPageData() {
        $data['alluser'] = $this->getAllUsersCount();
        $data['quescount'] = $this->getAllQuestionsCount();
        $data['unans'] = $this->getUnansweredCount();
        $data['users'] = $this->getTopUsers();
        return $data;
    }

}
