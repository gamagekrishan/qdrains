<?php

class index_model extends Model {

    function __construct() {
        parent::__construct();
    }

    //get data that need to index page hot tab
    public function index() {
        try {
            $ques = $this->db->select("select q.qId,q.ask_date,q.title,q.content,q.views,q.user_Id, "
                    . "u.f_name,u.l_name,u.profile_image,u.reputation from questions as q inner "
                    . "join users as u on q.user_Id = u.user_Id where q.status = :status order by q.views DESC limit 5",array(
                        ':status' => 'active'
                    ));
            $tags = $this->getTags($ques);
            $ans = $this->getAnsCount($ques);
            $votes = $this->getVotesCount($ques);
            return array('question' => $ques, 'tags' => $tags, 'ans' => $ans, 'votes' => $votes);
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
    }

    //get data that need to index page Week tab
    public function indexWeek() {
        try {
            $ques = $this->db->select("select q.qId,q.ask_date,q.title,q.content,q.views,q.user_Id, "
                    . "u.f_name,u.l_name,u.profile_image,u.reputation from questions as q inner "
                    . "join users as u on q.user_Id = u.user_Id where ask_date > DATE_SUB(NOW(), "
                    . "INTERVAL 7 DAY) and q.status = :status order by q.views DESC limit 5",array(
                        ':status' => 'active'
                    ));
            $tags = $this->getTags($ques);
            $ans = $this->getAnsCount($ques);
            $votes = $this->getVotesCount($ques);
            return array('question' => $ques, 'tags' => $tags, 'ans' => $ans, 'votes' => $votes);
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
    }

    //get data that need to index page month tab
    public function indexMonth() {
        try {
            $ques = $this->db->select("select q.qId,q.ask_date,q.title,q.content,q.views,q.user_Id, "
                    . "u.f_name,u.l_name,u.profile_image,u.reputation from questions as q inner "
                    . "join users as u on q.user_Id = u.user_Id where ask_date > DATE_SUB(NOW(), "
                    . "INTERVAL 30 DAY) and q.status = :status order by q.views DESC limit 5",array(
                        ':status' => 'active'
                    ));
            $tags = $this->getTags($ques);
            $ans = $this->getAnsCount($ques);
            $votes = $this->getVotesCount($ques);
            return array('question' => $ques, 'tags' => $tags, 'ans' => $ans, 'votes' => $votes);
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
    }

    private function getTags($ques) {
        try {
            $tags = array();
            foreach ($ques as $row) {
                $data = $this->db->select('select t.tag_Id,t.title from questionscategories as q inner join tags as t on q.tag_Id = t.tag_Id where q.q_Id = :q_Id ', array(':q_Id' => $row['qId']));
                $tags[$row['qId']] = $data;
            }
            return $tags;
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
    }

    private function getAnsCount($ques) {
        try {
            $count = array();
            foreach ($ques as $row) {
                $sth = $this->db->prepare('select ans_Id from answers where q_Id = :qid ');
                $sth->execute(array(
                    ':qid' => $row['qId']
                ));
                $count[$row['qId']] = $sth->rowCount();
            }
            return $count;
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
    }

    private function getVotesCount($ques) {
        try {
            
            $count = array();
            foreach ($ques as $row) {
                $sth = $this->db->prepare('select q_Id from votequestions where q_Id = :q_Id');
                $sth->execute(array(
                    ':q_Id' => $row['qId']
                ));
                $count[$row['qId']] = $sth->rowCount();
            }
            return $count;
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
    }
    
    public function getPageData(){
        $data['alluser'] = $this->getAllUsersCount();
        $data['quescount'] = $this->getAllQuestionsCount();
        $data['unans'] = $this->getUnansweredCount();
        $data['users'] = $this->getTopUsers();
        return $data;
    }

}
