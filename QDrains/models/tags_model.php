<?php

class tags_model extends Model {

    function __construct() {
        parent::__construct();
    }

    public function getTagsData() {
        $rCount = $this->db->select('select COUNT(*) as countt from tags where status = :status order by questions_usage desc ', array(
            ':status' => 'active'
        ));
        $pageSetupData = $this->pageSetup($rCount[0]['countt']);
        if ($pageSetupData['pageCond']) {
            $tagData = $this->db->select('select * from tags where status = :status order by questions_usage desc limit ' . Session::get('pageSize') . ' OFFSET ' . $pageSetupData['offset'] . '', array(
                ':status' => 'active'
            ));
            return array(
                'data' => $tagData,
                'pagesetupdata' => array(
                    'cpage' => $pageSetupData['page'],
                    'allpages' => $pageSetupData['pagecount']
                )
            );
        } else {
            return false;
        }
    }

    public function getPageData() {
        $data['alluser'] = $this->getAllUsersCount();
        $data['quescount'] = $this->getAllQuestionsCount();
        $data['unans'] = $this->getUnansweredCount();
        $data['users'] = $this->getTopUsers();
        return $data;
    }

    public function search() {
        if (isset($_POST['tname'])) {

            $name = filter_input(INPUT_POST, "tname", FILTER_SANITIZE_STRING);
            $tagData = $this->db->select("select * from tags where status = :status and title like '" . $name . "%' limit 10", array(
                ':status' => 'active'
            ));
            if (!empty($tagData)) {
                foreach ($tagData as $tag) {
                    ?>
                    <div id="dp_wrapper_maincon_body_tags_con">
                        <a href="#" id="dp_wrapper_maincon_body_tags_block_inherit">
                            <div id="dp_wrapper_maincon_body_tags_block">
                                <div id="dp_wrapper_maincon_body_tags_block_tagname">
                                    <span id="font"><?php echo $tag['title']; ?></span>
                                </div>
                                <div id="dp_wrapper_maincon_body_tags_block_discription">
                                    <span id="font"><?php echo substr($tag['content'], 0, 120) . '...'; ?></span>
                                </div>
                                <div id="dp_wrapper_maincon_body_tags_block_useageandrank">
                                    <div id="dp_wrapper_maincon_body_tags_block_useage">
                                        <div id="dp_wrapper_maincon_body_tags_block_useage_name">
                                            <span id="font">Followers:</span>
                                        </div>
                                        <div id="dp_wrapper_maincon_body_tags_block_useage_count">
                                            0
                                        </div>
                                    </div>
                                    <div id="dp_wrapper_maincon_body_tags_block_rank">
                                        <div id="dp_wrapper_maincon_body_tags_block_rank_name">
                                            <span id="font">Questions:</span>
                                        </div>
                                        <div id="dp_wrapper_maincon_body_tags_block_rank_count">
                                            <?php echo $tag['questions_usage']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </a>
                    <?php
                }
            } else {
                echo '<h2><span style="margin-left:100px; color:#ff9933">No tags matched your search.</span></h2>';
            }
        }
    }

    public function create() {

        $form = new Form();

        //validate form data
        $form->post('title', FILTER_SANITIZE_STRING)
                ->val('minlength', 2)
                ->val('maxlength', 20)
                ->post('content', FILTER_SANITIZE_STRING)
                ->val('minlength', 10)
                ->val('maxlength', 500);

        $data = $form->submit();
        $filterdData = $form->fetch();
        if ($data == false) {
            $this->findTag($filterdData['title']);
            if ($this->saveTag($filterdData)) {
                echo json_encode(array('status' => 1));
            } else {
                echo json_encode(array('status' => 0, 'error' => 'Somthing wrong please try again later..'));
            }
        } else {
            echo json_encode(array('status' => 0, $data));
        }
    }

    private function saveTag($tData) {
        try {
            $this->db->insert('tags', array(
                'create_date' => date("Y-m-d"),
                'title' => $tData['title'],
                'content' => $tData['content'],
                'creater_Id' => Session::get('uId')
            ));
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    private function findTag($title){
       $data =  $this->db->select('select COUNT(*) as countt from tags where title = :title',array(
           ':title' => $title 
        ));
       if($data[0]['countt'] <= 0){
           return ;
       }else{
           echo json_encode(array('status' => 0, 'error' => 'This tag already exist'));die;
       }
       
    }

}
