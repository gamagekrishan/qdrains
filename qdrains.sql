-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2015 at 05:59 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `qdrains`
--

-- --------------------------------------------------------

--
-- Table structure for table `answerchangestatus`
--

CREATE TABLE IF NOT EXISTS `answerchangestatus` (
  `ans_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `request_type` enum('close','accept') NOT NULL,
  PRIMARY KEY (`ans_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `answerchangestatus`
--
DROP TRIGGER IF EXISTS `after_insert_answerchangestatus`;
DELIMITER //
CREATE TRIGGER `after_insert_answerchangestatus` AFTER INSERT ON `answerchangestatus`
 FOR EACH ROW if(new.request_type = 'close') then
	update answers set status = 'closed' where ans_Id = NEW.ans_Id;
elseif (new.request_type = 'accept') then
	update answers set acceptance = 'yes' where ans_Id = NEW.ans_Id;
end if
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `ans_Id` int(10) NOT NULL AUTO_INCREMENT,
  `content` varchar(10100) NOT NULL,
  `post_date` date NOT NULL,
  `status` enum('active','closed') NOT NULL DEFAULT 'active',
  `user_Id` int(6) NOT NULL,
  `q_Id` int(10) NOT NULL,
  `acceptance` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`ans_Id`),
  KEY `user_Id` (`user_Id`),
  KEY `q_Id` (`q_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10039 ;

--
-- Triggers `answers`
--
DROP TRIGGER IF EXISTS `after_insert_answers`;
DELIMITER //
CREATE TRIGGER `after_insert_answers` BEFORE INSERT ON `answers`
 FOR EACH ROW begin
set @qStatus = (select answered from questions where qId = new.q_Id);
if @qStatus = 'no' then
	update questions set answered = 'yes' where qId = new.q_Id;
end if;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ans_comments`
--

CREATE TABLE IF NOT EXISTS `ans_comments` (
  `com_Id` int(10) NOT NULL,
  `ans_Id` int(10) NOT NULL,
  PRIMARY KEY (`com_Id`,`ans_Id`),
  KEY `ans_Id` (`ans_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clienissuses`
--

CREATE TABLE IF NOT EXISTS `clienissuses` (
  `cId` int(10) NOT NULL AUTO_INCREMENT,
  `subject` varchar(110) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `content` varchar(1010) NOT NULL,
  PRIMARY KEY (`cId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `com_Id` int(10) NOT NULL AUTO_INCREMENT,
  `post_date` date NOT NULL,
  `content` varchar(500) NOT NULL,
  `user_id` int(6) NOT NULL,
  PRIMARY KEY (`com_Id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10068 ;

-- --------------------------------------------------------

--
-- Table structure for table `facebookusers`
--

CREATE TABLE IF NOT EXISTS `facebookusers` (
  `user_Id` int(6) NOT NULL,
  `f_Id` varchar(21) NOT NULL,
  PRIMARY KEY (`user_Id`,`f_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `favouritetags`
--

CREATE TABLE IF NOT EXISTS `favouritetags` (
  `tag_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `select_date` date NOT NULL,
  PRIMARY KEY (`tag_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `googleusers`
--

CREATE TABLE IF NOT EXISTS `googleusers` (
  `user_Id` int(6) NOT NULL,
  `g_Id` varchar(70) NOT NULL,
  PRIMARY KEY (`user_Id`,`g_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inite`
--

CREATE TABLE IF NOT EXISTS `inite` (
  `inv_id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `u_id` int(6) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `inv_name` varchar(100) DEFAULT NULL,
  `rep` int(4) NOT NULL,
  `token` varchar(100) NOT NULL,
  `accept` enum('0','1') NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `linkidentify`
--

CREATE TABLE IF NOT EXISTS `linkidentify` (
  `user_Id` int(6) NOT NULL,
  `link` varchar(64) NOT NULL,
  `validity` enum('true','false') NOT NULL DEFAULT 'true',
  `createdate` datetime NOT NULL,
  PRIMARY KEY (`link`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `localusers`
--

CREATE TABLE IF NOT EXISTS `localusers` (
  `user_Id` int(6) NOT NULL,
  `password` varchar(64) NOT NULL,
  `verification` enum('true','false') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`user_Id`),
  KEY `user_Id` (`user_Id`,`password`),
  KEY `user_Id_2` (`user_Id`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loggeddetails`
--

CREATE TABLE IF NOT EXISTS `loggeddetails` (
  `user_Id` int(6) NOT NULL,
  `token` varchar(64) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`user_Id`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questioncomments`
--

CREATE TABLE IF NOT EXISTS `questioncomments` (
  `com_Id` int(10) NOT NULL,
  `q_Id` int(10) NOT NULL,
  PRIMARY KEY (`com_Id`,`q_Id`),
  KEY `q_Id` (`q_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `qId` int(10) NOT NULL AUTO_INCREMENT,
  `ask_date` date NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` varchar(5000) NOT NULL,
  `status` enum('active','closed') NOT NULL DEFAULT 'active',
  `views` int(10) NOT NULL DEFAULT '0',
  `user_Id` int(6) NOT NULL,
  `answered` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`qId`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

-- --------------------------------------------------------

--
-- Table structure for table `questionscategories`
--

CREATE TABLE IF NOT EXISTS `questionscategories` (
  `q_Id` int(10) NOT NULL,
  `tag_Id` int(10) NOT NULL,
  PRIMARY KEY (`q_Id`,`tag_Id`),
  KEY `tag_Id` (`tag_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `questionscategories`
--
DROP TRIGGER IF EXISTS `after_questionscategories_insert`;
DELIMITER //
CREATE TRIGGER `after_questionscategories_insert` AFTER INSERT ON `questionscategories`
 FOR EACH ROW update tags set questions_usage = questions_usage + 1 where tag_Id = NEW.tag_Id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `questionschangestatus`
--

CREATE TABLE IF NOT EXISTS `questionschangestatus` (
  `q_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `request_type` enum('close','hold') NOT NULL,
  PRIMARY KEY (`q_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `questionschangestatus`
--
DROP TRIGGER IF EXISTS `after_insert_questionchangestatus`;
DELIMITER //
CREATE TRIGGER `after_insert_questionchangestatus` AFTER INSERT ON `questionschangestatus`
 FOR EACH ROW update questions set `status` = 'closed' where qId = new.q_Id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `reputation`
--

CREATE TABLE IF NOT EXISTS `reputation` (
  `rep_Id` int(10) NOT NULL AUTO_INCREMENT,
  `user_Id` int(6) NOT NULL,
  `date` datetime NOT NULL,
  `event` varchar(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `rep_amount` int(7) NOT NULL,
  PRIMARY KEY (`rep_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Triggers `reputation`
--
DROP TRIGGER IF EXISTS `after_reputation_insert`;
DELIMITER //
CREATE TRIGGER `after_reputation_insert` AFTER INSERT ON `reputation`
 FOR EACH ROW UPDATE users SET reputation = reputation + NEW.rep_amount WHERE user_Id = NEW.user_Id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tagchangestatus`
--

CREATE TABLE IF NOT EXISTS `tagchangestatus` (
  `tag_Id` int(10) NOT NULL,
  `requester_Id` int(6) NOT NULL,
  `req_date` date NOT NULL,
  `description` int(11) NOT NULL,
  PRIMARY KEY (`tag_Id`,`requester_Id`),
  KEY `requester_Id` (`requester_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_Id` int(10) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(500) DEFAULT NULL,
  `questions_usage` int(10) NOT NULL DEFAULT '0',
  `followers` int(10) NOT NULL DEFAULT '0',
  `creater_Id` int(6) NOT NULL,
  `status` enum('active','closed') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`tag_Id`),
  KEY `creater_Id` (`creater_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10031 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_Id` int(6) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `join_date` date NOT NULL,
  `about` varchar(5000) DEFAULT NULL,
  `profile_image` varchar(200) DEFAULT 'http://www.qdrains.com/public/images/unknown.jpg',
  `age` int(11) DEFAULT NULL,
  `reputation` int(10) NOT NULL DEFAULT '1',
  `active` enum('true','false') NOT NULL DEFAULT 'false',
  `gender` varchar(2) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `qacc` enum('0','1') NOT NULL DEFAULT '0',
  `gacc` enum('0','1') NOT NULL DEFAULT '0',
  `emailconfirm` enum('0','1','2') NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_Id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100088 ;

-- --------------------------------------------------------

--
-- Table structure for table `voteanswers`
--

CREATE TABLE IF NOT EXISTS `voteanswers` (
  `ans_Id` int(10) NOT NULL,
  `user_Id` int(6) NOT NULL,
  `date` date NOT NULL,
  `status` enum('up','down') NOT NULL,
  PRIMARY KEY (`ans_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `voteanswers`
--
DROP TRIGGER IF EXISTS `after_insert_voteanswers`;
DELIMITER //
CREATE TRIGGER `after_insert_voteanswers` BEFORE INSERT ON `voteanswers`
 FOR EACH ROW BEGIN
set @voteAmount = 0;
set @voteDes = '';
set @event = '';

if new.status = 'up' then
	set @voteAmount = 10;
    set @voteDes = 'voting up a answer';
    set @event = 'vote up';
elseif new.status = 'down' then
	set @voteAmount = -5;
    set @voteDes = 'voting down a answer';
    set @event = 'vote down';
end if; set @ansUserId = (select user_Id from answers where ans_Id = new.ans_Id);

Insert into reputation (`user_Id`,`date`,`event`,`description`,`rep_amount`) values (@ansUserId,now(),@event,@voteDes,@voteAmount);
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `votequestions`
--

CREATE TABLE IF NOT EXISTS `votequestions` (
  `q_Id` int(10) NOT NULL,
  `user_Id` int(8) NOT NULL,
  `date` date NOT NULL,
  `status` enum('up','down') NOT NULL,
  PRIMARY KEY (`q_Id`,`user_Id`),
  KEY `user_Id` (`user_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `votequestions`
--
DROP TRIGGER IF EXISTS `after_insert_votequestions`;
DELIMITER //
CREATE TRIGGER `after_insert_votequestions` AFTER INSERT ON `votequestions`
 FOR EACH ROW BEGIN
    set @voteAmount = 0;
    set @voteDes = '';
    set @event = '';
    
    if new.status = 'up' then
        set @voteAmount = 15;
        set @voteDes = 'voting up a question';
        set @event = 'vote up';
    elseif new.status = 'down' then
        set @voteAmount = -5;
        set @voteDes = 'voting down a questions';
        set @event = 'vote down';
    end if; 
    set @quesUserId = (select user_Id from questions where qId = new.q_Id);
    
    Insert into reputation (`user_Id`,`date`,`event`,`description`,`rep_amount`) values (@quesUserId,now(),@event,@voteDes,@voteAmount);
end
//
DELIMITER ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answerchangestatus`
--
ALTER TABLE `answerchangestatus`
  ADD CONSTRAINT `answerchangestatus_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `answerchangestatus_ibfk_2` FOREIGN KEY (`ans_Id`) REFERENCES `answers` (`ans_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE;

--
-- Constraints for table `ans_comments`
--
ALTER TABLE `ans_comments`
  ADD CONSTRAINT `ans_comments_ibfk_1` FOREIGN KEY (`com_Id`) REFERENCES `comments` (`com_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ans_comments_ibfk_2` FOREIGN KEY (`ans_Id`) REFERENCES `answers` (`ans_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `facebookusers`
--
ALTER TABLE `facebookusers`
  ADD CONSTRAINT `facebookusers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favouritetags`
--
ALTER TABLE `favouritetags`
  ADD CONSTRAINT `favouritetags_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `favouritetags_ibfk_2` FOREIGN KEY (`tag_Id`) REFERENCES `tags` (`tag_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `googleusers`
--
ALTER TABLE `googleusers`
  ADD CONSTRAINT `googleusers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `localusers`
--
ALTER TABLE `localusers`
  ADD CONSTRAINT `localusers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `loggeddetails`
--
ALTER TABLE `loggeddetails`
  ADD CONSTRAINT `loggeddetails_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`);

--
-- Constraints for table `questioncomments`
--
ALTER TABLE `questioncomments`
  ADD CONSTRAINT `questioncomments_ibfk_1` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `questioncomments_ibfk_2` FOREIGN KEY (`com_Id`) REFERENCES `comments` (`com_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `questionscategories`
--
ALTER TABLE `questionscategories`
  ADD CONSTRAINT `questionscategories_ibfk_1` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `questionscategories_ibfk_2` FOREIGN KEY (`tag_Id`) REFERENCES `tags` (`tag_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `questionschangestatus`
--
ALTER TABLE `questionschangestatus`
  ADD CONSTRAINT `questionschangestatus_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `questionschangestatus_ibfk_2` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE;

--
-- Constraints for table `reputation`
--
ALTER TABLE `reputation`
  ADD CONSTRAINT `reputation_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `tagchangestatus`
--
ALTER TABLE `tagchangestatus`
  ADD CONSTRAINT `tagchangestatus_ibfk_1` FOREIGN KEY (`tag_Id`) REFERENCES `tags` (`tag_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tagchangestatus_ibfk_2` FOREIGN KEY (`requester_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`creater_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `voteanswers`
--
ALTER TABLE `voteanswers`
  ADD CONSTRAINT `voteanswers_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `voteanswers_ibfk_2` FOREIGN KEY (`ans_Id`) REFERENCES `answers` (`ans_Id`) ON UPDATE CASCADE;

--
-- Constraints for table `votequestions`
--
ALTER TABLE `votequestions`
  ADD CONSTRAINT `votequestions_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `votequestions_ibfk_2` FOREIGN KEY (`q_Id`) REFERENCES `questions` (`qId`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
