<?php

class error extends Controller {

    function __construct() {
        parent::__construct();
        $this->authUserlogin();
        $this->view->title = '404 Error';
    }

    function index() {
        //page variables
        $this->view->css = array('error');
        
        //Render pages
        $this->view->render('error/index');
    }

}
