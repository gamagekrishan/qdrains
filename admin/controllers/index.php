<?php

class Index extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $this->view->css = array('index');
        $this->view->data = $this->model->load();
        $this->pages = array('index/index');
        $this->pageRender();
    }
    
    public function sendInvite(){
        $this->model->sendInvite();
    }

}