<div id="dp_wrapper">
  <div id="dp_wrapper_maincon">
    <div id="dp_wrapper_maincon_head">
      <div id="mainconmenu">
        <ul>
          <li class='active'><span style="font-size:16px; font-weight:bold;">Page not Found</span></li>
        </ul>
      </div>
    </div>
    <div id="dp_wrapper_maincon_body">
      <div id="dp_wrapper_maincon_body_div1"> <span id="font"> Sorry, We Couldn't find the page that you requested.</span> </div>
      <br>
      <div id="dp_wrapper_maincon_body_div2" style="background-image:url(<?php echo URL; ?>public/images/sad.png);"> </div>
      <div id="dp_wrapper_maincon_body_div3"> <span id="font"> If you feel something is missing,<br> That should be here.<br> Plese<a href="<?php  echo URL; ?>help/contactus" style="text-decoration: none; color: blue;"> contact us</a>.</span> </div>
    </div>
  </div>
</div>