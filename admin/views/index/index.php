<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<?php
        if (isset($this->css)) {
            foreach ($this->css as $css) {
                echo '<link rel="stylesheet" href="' . URL . 'public/css/' . $css . '.css" />';
            }
        }
        if (isset($this->js)) {
            foreach ($this->js as $js) {
                echo '<script type= "text/javascript" src="' . URL . 'views/' . $js . '.js"></script>';
            }
        }
        ?>
</head>
<body>
    <?php $d = $this->data; ?>
    <input type="hidden" id="phpVar" value="<?php echo URL; ?>">
<div id="wrapper">

<div id="head">
<h1>Invite People</h1>
</div>
    <form action="<?php echo URL; ?>index/sendInvite" method="post" id="invf">
<div id="middle">

<div id="title">
Name
</div>
<div id="text">
    <input type="text" name="name" class="textbox_search" placeholder="Name">
</div>

<div id="title">
E-Mail
</div>
<div id="text">
    <input type="text" name="email" class="textbox_search" placeholder="E-Mail">
</div>

<div id="title">
Reputation
</div>
<div id="text">
    <input type="text" name="rep" class="textbox_search" placeholder="Reputation"/>
</div>

<div id="notification">
</div>

<input type="submit" class="btn"/>

</div>
    </form>

<div id="table" class="myOtherTable">
<table style="width:800px;">
  <tr>
	<th>Name</th>
    <th>ID</th>
    <th>Invitation Name</th>
    <th>Acceptence</th>
    <th>Date</th>
  </tr>
  <?php        foreach ($d as $inv){ ?>
  <tr>
    <td><?php echo $inv['name']; ?></td>
    <td><?php echo $inv['u_id']; ?></td>		
    <td><?php echo $inv['inv_name']; ?></td>
    <td><?php echo $inv['accept']; ?></td>
    <td><?php echo $inv['date']; ?></td>
  </tr>
  <?php } ?>
</table>
</div>

</div>
</body>
</html>
