<?php

class Bootstrap {

    private $_url = null;
    private $_controller = null;
    private $_controllerName = null;
    private $_controllerPath = 'controllers/';
    private $_modelPath = 'models/';
    private $_errorFile = 'error.php';
    private $_defaultFile = 'index.php';

    public function init() {
        //set the protected $_url
        $this->_getUrl();

        //auto load the default controller if the url is empty
        if (empty($this->_url[0])) {
            $this->_loadDefaultController();
            return false;
        }

        //load the request controller
        $this->_loadExistingController();

        //call methods on controller
        $this->_callControllerMethod();
        
    }

    /**
     * (Optional) Set a custom path to controllers
     * @param string $path
     */
    public function setControllerPath($path) {
        $this->_controllerPath = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to model
     * @param string $path
     */
    public function setModelPath($path) {
        $this->_modelPath = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to error file
     * @param string $path Use the file name of your controller, eg: error.php
     */
    public function setErrorFile($path) {
        $this->_errorFile = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to error file
     * @param string $path Use the file name of your controller, eg: error.php
     */
    public function setDefaultFile($path) {
        $this->_defaultFile = trim($path, '/') . '/';
    }

    /**
     * Fetches the $_GET from 'url'
     */
    private function _getUrl() {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
        $this->_controllerName = $this->_url[0];
    }

    /**
     * This loads if there is no GET parameter passed
     */
    private function _loadDefaultController() {

        require $this->_controllerPath . 'index.php';
        $this->_controller = new index();
         $this->_controller->loadModel('index', $this->_modelPath);
        $this->_controller->index();
        
//        $this->_url[0] = 'index';
//        $this->_loadExistingController();
    }

    /**
     * Load an existing controller if IS GET parameter passed
     * @return boolean|string
     */
    private function _loadExistingController() {
        $file = $this->_controllerPath . $this->_controllerName . '.php';
        if (file_exists($file)) {
            require $file;
            $this->_controller = new $this->_controllerName;
            $this->_controller->loadModel($this->_controllerName, $this->_modelPath);
        } else {

            $this->__error('index');
            return false;
        }
    }

    /**
     * If a method is passed the GET url parameter
     */
    private function _callControllerMethod() {
        //http://localhost/controller/method/(pram)/(pram)/(pram)
        //url[0] = Controller
        //url[1] = Method
        //url[2] = Pram
        //url[3] = Pram
        //url[4] = Pram

        $length = count($this->_url);
        switch ($length) {
            //controller/method/(pram)/(pram)/(pram)
            case 5:
                //Check if the method is exist in the controller
                if (method_exists($this->_controller, $this->_url[1])) {
                    $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
                } else {
                    $this->__error('index');
                }
                break;

            //controller/method/(pram)/(pram)
            case 4:
                //Check if the method is exist in the controller
                if (method_exists($this->_controller, $this->_url[1])) {
                    $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
                } else {
                    $this->__error('index');
                }
                break;

            //controller/method/(pram)
            case 3:
                //if controller is qestions or users its call defferent method
                if (($this->_url[0] == "questions" || $this->_url[0] == 'users') && is_numeric($this->_url[1]) == 1) {
                    $this->_controller->single($this->_url[1], isset($this->_url[2]) ? $this->_url[2] : null);
                }elseif (method_exists($this->_controller, $this->_url[1])) {
                    $this->_controller->{$this->_url[1]}($this->_url[2]);
                } else {
                    $this->__error('index');
                }
                break;

            //controller/method
            case 2:
                //if controller is qestions or users its call defferent method
                if (($this->_url[0] == "questions" || $this->_url[0] == "users") && is_numeric($this->_url[1]) == 1) {
                    $this->_controller->single($this->_url[1]);
                } elseif (method_exists($this->_controller, $this->_url[1])) {
                    $this->_controller->{$this->_url[1]}();
                } else {
                    $this->__error('index');
                }
                break;
            case 1:
                $this->_controller->index();
                break;

            default:
                $this->__error('index');
                break;
        }
    }

    /**
     * Display an error page if nothing exists
     * @return boolean
     */
    public function __error($error) {
        require $this->_controllerPath . $this->_errorFile;
        $this->_controller = new error();
        $this->_controller->{$error}();
        exit;
    }

}
