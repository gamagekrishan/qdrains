<?php

/**
 * 
 * - Fill out  a form
 * - POST to PHP
 * Sanitize
 * validate
 * Return Data
 * Write to Database
 *
 */
require 'form/Validate.php';

class Form {
    /* var array $_currentItem The imediately posted item */

    private $_currentItem = null;

    /* var array $_postedData sotored the posted Data */
    private $_postData = array();

    /* var object $_postedData Validater object */
    private $_val = array();

    /* var array $error Hold the current forms errors */
    private $_error = array();

    public function __construct() {
        $this->_val = new val();
    }

    /**
     * post This is to run $_POST
     */
    public function post($field, $method = false) {
        if ($method != false && $method != 'file') {
            $this->_postData[$field] = preg_replace('/\s\s+/', '',filter_input(INPUT_POST, $field, $method));
            $this->_currentItem = $field;
            return $this;
        }
        if ($method == false) {
            $this->_postData[$field] = filter_input(INPUT_POST, $field);
            $this->_currentItem = $field;
            return $this;
        }
        if($method == 'file'){
            $this->_postData[$field] = $_FILES['profile_image'];
            $this->_currentItem = $field;
            return $this;
            
        }
    }

    /**
     * fetch - Return the posted data 
     * @param mixed $fieldName
     * @return mixed or array
     */
    public function fetch($fieldName = false) {
        if ($fieldName) {
            if (isset($this->_postData[$fieldName])) {
                return $this->_postData[$fieldName];
            } else {
                return false;
            }
        } else {
            return $this->_postData;
        }
    }

    /**
     * val - This to validate
     * @param string $typeOfValidation & method from the Form/val class
     * @param string $arg & property to validate 
     */
    public function val($typeOfValidation, $arg = null) {

        if ($arg == null) {
            $error = $this->_val->{$typeOfValidation}($this->_postData[$this->_currentItem]);
        } else {
            $error = $this->_val->{$typeOfValidation}($this->_postData[$this->_currentItem], $arg);
        }
        if ($error) {
            $this->_error[$this->_currentItem] = $error;
        }
        return $this;
    }

    /**
     * submit - Handles the form and return errors
     */
    public function submit() {
        if (empty($this->_error)) {
            return false;
        } else {
            return $this->_error;
        }
    }

}
