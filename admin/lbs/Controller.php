<?php

class Controller {

    function __construct() {
        
        $this->view = new View();
        Session::init();
        Session::paging();
        
    }
    
    //perpos of page render 
    protected $pages = null;
    protected $flag = true;
    
    /**
     * 
     * @param string $name Name of the medel
     * @param string $path Location of the model
     */
    public function loadModel($name, $modelPath)
    {
        $path = $modelPath . $name.'_model.php';
        if(file_exists($path))
        {
            require $modelPath . $name.'_model.php';
            
            $modelName = $name.'_model';
            $this->model = new $modelName();
        }
    }
    
    /**
     * if user browser has cookies check the token and generate sessions
     */
    public function authUserlogin(){
        if(isset($_COOKIE['token'])){
            $authlog = new Model();
            $authlog->authUserlogin();
            
        }
    }
    
    /**
     * render pages from array values for particular controller method
     */
    protected function pageRender() {
        if ($this->flag == false) {
            $this->pages = null;
            $this->view->css = array('error');
            $this->view->title = 'Page not found';
            $this->view->render('header');
            $this->view->render('contentstart');
            $this->view->render('error/index');
            $this->view->render('footer');
        } else {
            foreach ($this->pages as $value) {
                $this->view->render($value);
            }
        }
    }
    

}
