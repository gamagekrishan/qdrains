<?php

class Model {

    function __construct() {
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $this->val = new Form();
        $this->mail = new Mailer();
    }

    public function authUserlogin() {
        $token = filter_input(INPUT_COOKIE, 'token', FILTER_SANITIZE_STRING);
        $data1 = $this->db->select("SELECT * FROM loggeddetails WHERE token = :token", array(":token" => $token));

        $count1 = count($data1);
        if ($count1 > 0) {
            $data = $this->db->select('SELECT * FROM users as u INNER JOIN localusers as l ON u.user_Id = l.user_Id WHERE u.user_Id = :Id', array(":Id" => $data1[0]['user_Id']));
            $count = count($data);
            if ($count > 0) {
                $this->sessionGenerate($data);
            }
        } else {
            $this->destroyCookie();
        }
    }

    protected function sessionGenerate($data) {
        Session::set('loggedIn', true);
        Session::set('uId', $data[0]['user_Id']);
        Session::set('uImage', $data[0]['profile_image']);
        Session::set('uName', $data[0]['f_name'] . " " . $data[0]['l_name']);
        Session::set('rep', $data[0]['reputation']);
    }

    public function destroyCookie() {
        if (isset($_COOKIE['token'])) {

            $token = filter_input(INPUT_COOKIE, 'token', FILTER_SANITIZE_STRING);
            $sth = $this->db->prepare("SELECT * FROM loggeddetails WHERE token = :token");
            $sth->execute(array(
                ':token' => $token
            ));
            $count = $sth->rowCount();
            if ($count > 0) {
                $this->db->delete('DELETE FROM loggeddetails WHERE token = :token', array(':token' => $token));
            }
            setcookie('token', $token, time() + (1), "/", SESS_DOMAIN);
        }
    }

    protected function getAllUsersCount() {
        return $this->db->select('select count(*) as count from users where active = :status', array(
                    ':status' => 'true'
        ));
    }

    protected function getAllQuestionsCount() {
        return $this->db->select('select count(*) as count from questions where status = :status', array(
                    ':status' => 'active'
        ));
    }

    protected function getUnansweredCount() {
        return $this->db->select('select count(*) as count from questions where answered = :status and status = :s', array(
                    ':status' => 'no',
                    's' => 'active'
        ));
    }

    protected function getTopUsers() {
        return $this->db->select('select l_name,profile_image,user_Id from users order by reputation desc limit 6');
    }

    protected function pageSetup($rCount) {
        
        $pageCond = false;
       
        $reminder =  $rCount % Session::get('pageSize');
        $pageCount = $reminder <= 0 ? (int) ($rCount / Session::get('pageSize')) : (int) ($rCount / Session::get('pageSize')) + 1;

        $page = isset($_GET['page']) ? filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT) : 1;
        
        

        if ($page >= 1 && $page <= $pageCount) {
            $pageCond = true;
        }
        
        return array(
            'page' => $page,
            'pageCond' => $pageCond,
            'offset' => ($page - 1) * Session::get('pageSize'),
            'pagecount' => $pageCount
        );
    }
    
    protected function sessionUpdate(){
        $this->sessionGenerate($this->db->select('select user_Id,profile_image,f_name,l_name,reputation from users where user_Id = :id',array(
            ':id' => Session::get('uId')
        )));
    }

}
