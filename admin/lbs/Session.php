<?php

class Session
{
    
    public static function init ()
    {
        @session_start();
    }
    public static function  set ($key,$value)
                {
        
        $_SESSION[$key] = $value;
    }
    
    public static function  get ($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
    }
    public  static function destroy()
    {
        session_destroy();
    }
    
    public  static function paging()
    {
        if(!isset($_SESSION['pageSize'])){
            $_SESSION['pageSize'] = 5;
        }
        if(isset($_GET['pagesize'])){
         $pageSize = filter_input(INPUT_GET, 'pagesize',FILTER_SANITIZE_NUMBER_INT);
            if($pageSize > 0 && $pageSize <= 20 ){
                $_SESSION['pageSize'] = $pageSize;
            }
        }
    }
}