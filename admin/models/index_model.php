<?php

class index_model extends Model {

    function __construct() {
        parent::__construct();
    }

    //get data that need to index page hot tab
    public function load() {
        try {
            $data = $this->db->select('select *  from inite order by date desc');
            if ($data['s'] == '1') {
                return $data['data'];
            } else {
                echo $data['data'];
            }
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
    }

    public function sendInvite() {
        $form = new Form();
        //validate form data
        $form->post('email', FILTER_SANITIZE_EMAIL)
                ->val('filter', FILTER_VALIDATE_EMAIL)
                ->post('name', FILTER_SANITIZE_STRING)
                ->val('minlength', 2)
                ->val('maxlength', 20)
                ->post('rep', FILTER_SANITIZE_NUMBER_INT)
                ->val('minlength', 1)
                ->val('maxlength', 5);

        $data = $form->submit();

        if ($data == false) {
            $filterdData = $form->fetch();

            $token = Hash::create("sha256", $filterdData['email'], HASH_GENERAL_KEY);

            $d = $this->db->insert('inite', array(
                'email' => $filterdData['email'],
                'inv_name' => $filterdData['name'],
                'rep' => $filterdData['rep'],
                'date' => date("Y-m-d"),
                'token' => $token
            ));
            if ($d['s'] == 1) {
                $mailData = array(
                    'to' => $filterdData['email'],
                    'name' => $filterdData['name'],
                    'link' => 'http://www.qdrains.com/users/signup?invauthtoken='.$token
                );
                $mres = $this->mail->sendInvite($mailData);
                if($mres['status'] == 1){
                 header('Location: '.URL);
                }else{
                   echo "mail not send";
                }
            } else {
               echo $d['data'];
            }
        } else {
            print_r($data);
        }
    }

}
